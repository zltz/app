# 万能工具箱微信小程序

#### Description
微信小程序主要功能介绍:
【稳定】最新新闻资讯
【稳定】每日图文
【稳定】短视频图集壁纸解析
【稳定】短视频平台视频解析
【增加】头像制作器
【增加】好看的头像大全
【增加】手机壁纸大全
【增加】朋友圈文案
【增加】视觉二维码
【增加】星座运势
【增加】二维码生成
【增加】古代情诗
【增加】天气预报
【增加】名人名言
…
五十多种功能、涵盖各种工具使用，工具持续更新中！（免费提供，绝对稳定）！


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
