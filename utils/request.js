
function requestPostApi(url, params, sourceObj, successFun, failFun) {
    requestApi(url, params, 'POST', sourceObj, successFun, failFun)
}
function requestGetApi(url, params, sourceObj, successFun, failFun) {
    requestApi(url, params, 'GET', sourceObj, successFun, failFun)
}
 function requestApi(url, params, method, sourceObj, successFun, failFun) {
    wx.showLoading({
        mask: true,
    })
    if (wx.getStorageSync('openId')) {
        params["openId"] = wx.getStorageSync('openId');
        
        wx.request({
            url: url,
            method: method,
            data: params,
            header: {
                'Content-Type': "application/x-www-form-urlencoded",
            },
            success: function (res) {
                typeof successFun == 'function' && successFun(res.data, sourceObj);
            },
            fail: function (res) {
                typeof failFun == 'function' && failFun(res.data, sourceObj);
            },
            complete: function(res){
                wx.hideLoading();
            }
        })
    } else {
        console.log("无openId")
        wx.login({
            success: res => {
                console.log(res.code)
                wx.request({
                    url: "https://www.tlzcf.vip/api/wx/auth",//更改为你自己的服务端地址
                    method: "POST",
                    data: {
                        js_code: res.code
                    },
                    header: {
                        'Content-Type': "application/x-www-form-urlencoded",
                    },
                    success: function (res) {
                        wx.setStorageSync('openId', res.data.data);
                        params["openId"] = wx.getStorageSync('openId');
                        requestApi(url, params, method, sourceObj, successFun, failFun);
                    },
                    fail: function (res) {
                        console.error(res);
                    }
                })
            }
        })
    }
}

function uploadImgApi(t, n, e, a, s, i, l, u) {
  !function(t, n, e, a, s, i, l, u, c) {
      s && wx.showToast({
          title: s,
          icon: i,
          mask: !0,
          duration: 3e4
      });
      var r = "?";
      for (var f in n) r += f + "=" + n[f] + "&";
      r = r.substr(0, r.length - 1).replace(/#/g, "%23"), console.log(r), wx.uploadFile({
          url: t + r,
          filePath: a,
          name: e,
          header: {
              "Content-Type": "multipart/form-data"
          },
          formData: n,
          success: function(o) {
              s && wx.hideToast(), o && (console.log(o), u(o));
          }
      });
  }(t, n, e, a, s, i, 0, l, u);
}
module.exports = {
    requestPostApi,
    requestGetApi,
    uploadImgApi
}