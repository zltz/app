module.exports = function e(r) {
    r = r.trim();
    for (var a = new Array(), s = "+", p = "", o = r.length, c = 0; c < o; ++c) {
        if ("." === r[c] || !isNaN(Number(r[c])) && " " !== r[c]) p += r[c]; else if ("(" === r[c]) {
            for (var i = 1, t = c; i > 0; ) "(" === r[t += 1] && (i += 1), ")" === r[t] && (i -= 1);
            p = "".concat(e(r.slice(c + 1, t))), c = t;
        }
        if (isNaN(Number(r[c])) && "." !== r[c] || c === o - 1) {
            var u = parseFloat(p);
            switch (s) {
              case "+":
                a.push(u);
                break;

              case "-":
                a.push(-u);
                break;

              case "*":
                a.push(a.pop() * u);
                break;

              case "/":
                a.push(a.pop() / u);
            }
            s = r[c], p = "";
        }
    }
    for (var f = 0; a.length; ) f += a.pop();
    return f;
};