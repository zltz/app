!function() {
    var e = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js");
    Object.defineProperty(exports, "__esModule", {
        value: !0
    }), exports.default = void 0;
    var t = e(require("CBDBE653C6098CCFADBD8E5437B9C3F4.js"));
    function n(e) {
        if ("undefined" == typeof Symbol || null == e[Symbol.iterator]) {
            if (Array.isArray(e) || (e = function(e, t) {
                if (e) {
                    if ("string" == typeof e) return r(e, t);
                    var n = Object.prototype.toString.call(e).slice(8, -1);
                    return "Object" === n && e.constructor && (n = e.constructor.name), "Map" === n || "Set" === n ? Array.from(n) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? r(e, t) : void 0;
                }
            }(e))) {
                var t = 0, n = function() {};
                return {
                    s: n,
                    n: function() {
                        return t >= e.length ? {
                            done: !0
                        } : {
                            done: !1,
                            value: e[t++]
                        };
                    },
                    e: function(e) {
                        throw e;
                    },
                    f: n
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var o, a, l = !0, i = !1;
        return {
            s: function() {
                o = e[Symbol.iterator]();
            },
            n: function() {
                var e = o.next();
                return l = e.done, e;
            },
            e: function(e) {
                i = !0, a = e;
            },
            f: function() {
                try {
                    l || null == o.return || o.return();
                } finally {
                    if (i) throw a;
                }
            }
        };
    }
    function r(e, t) {
        (null == t || t > e.length) && (t = e.length);
        for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
        return r;
    }
    var o = function e() {
        var n = this;
        (0, t.default)(this, e), this.__makeHashCode = function() {
            for (var e = "", t = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" ], r = t.length, o = 0; o < 8; o++) e += t[Math.round(Math.random() * (r - 1))];
            return n.__hashCodeSet.has(e) ? n.__makeHashCode() : (n.__hashCodeSet.add(e), e);
        }, this.__observerMap = new Map(), this.__hashCodeSet = new Set(), this.__segmentKey = "_#$@_";
    };
    exports.default = o, o.__default = new o(), o.post = function(e) {
        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;
        null != e ? o.__default.__observerMap.forEach(function(n, r, a) {
            var l = r.split(o.__default.__segmentKey);
            if (2 === l.length && l[0] === e) {
                var i = n.block, u = void 0 === i ? null : i;
                u && u(t);
            }
        }) : console.warn("[Notification] post name not null");
    }, o.addObserver = function(e, t) {
        var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null;
        if (null != e && null != t) {
            var r = e.__notification_hashCode;
            0 === (void 0 === r ? "" : r).length && (e.__notification_hashCode = o.__default.__makeHashCode());
            var a = t + o.__default.__segmentKey + e.__notification_hashCode;
            o.__default.__observerMap.set(a, {
                observer: e,
                block: n
            });
        } else console.warn("[Notification] observer not null or name not null");
    }, o.removeObserver = function(e) {
        var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;
        if (null != e) {
            var r = e.__notification_hashCode;
            if (null != r) if (t) {
                var a = t + o.__default.__segmentKey + r;
                o.__default.__observerMap.delete(a);
            } else {
                var l, i = n(o.__default.__observerMap.keys());
                try {
                    for (i.s(); !(l = i.n()).done; ) {
                        var u = l.value, s = u.split(o.__default.__segmentKey);
                        2 === s.length && s[1] === r && o.__default.__observerMap.delete(u);
                    }
                } catch (e) {
                    i.e(e);
                } finally {
                    i.f();
                }
            } else console.log("[Notification] observer not add");
        } else console.warn("[Notification] observer not null");
    };
}();