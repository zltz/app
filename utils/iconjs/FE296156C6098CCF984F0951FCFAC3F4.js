!function() {
    var t = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js")(require("9D53CC23C6098CCFFB35A4246909C3F4.js"));
    getApp();
    let e = null;
    Page({
        data: {
            loadingDH: !0,
            keywords: [],
            list: [],
            kindex: 0
        },
        onLoad: function(t) {
            this.getIndex(), wx.createInterstitialAd && (e = wx.createInterstitialAd({
                adUnitId: "adunit-b9abf64b40dddf8d"
            }), e.onLoad(() => {}), e.onError(t => {}), e.onClose(() => {}));
        },
        detail: function(t) {
            var e = t.currentTarget.dataset.id, a = t.currentTarget.dataset.item;
            1 == a && wx.navigateTo({
                url: "/pages/index/index?id=" + e
            }), 2 == a && wx.navigateTo({
                url: "/pages/gua/gua?id=" + e
            });
        },
        selKey: function(e) {
            var a = this, n = e.currentTarget.dataset.index;
            a.setData({
                kindex: n
            }), t.default.postRequest("/api/Image/frameKeywords", {
                keywords: a.data.keywords[n]
            }, "加载中...", "loading", function(t) {
                200 == t.data.status ? a.setData({
                    list: t.data.data
                }) : wx.showToast({
                    title: t.data.info,
                    icon: "none",
                    duration: 3e3
                });
            }, function(t) {
                wx.showToast({
                    title: "请求失败",
                    icon: "none",
                    duration: 3e3
                });
            });
        },
        getIndex: function() {
            var e = this;
            t.default.postRequest("/api/Image/frame", {}, "", "", function(t) {
                var a = t.data.data.keywords;
                a.unshift("全部", "热门", "最新", "名称"), e.setData({
                    keywords: a,
                    list: t.data.data.list,
                    loadingDH: !1
                });
            }, function(t) {
                wx.showToast({
                    title: "请求失败",
                    icon: "none",
                    duration: 3e3
                });
            });
        },
        onReady: function() {},
        onShow: function() {
            e && e.show().catch(t => {
                console.error(t);
            });
        },
        onHide: function() {},
        onUnload: function() {},
        onPullDownRefresh: function() {},
        onReachBottom: function() {},
        onShareAppMessage: function() {
            return {
                title: "➜在线制作头像挂件边框小工具，点击开始制作",
                imageUrl: "https://www.daijingjuan.com/attachment/images/2.png",
                path: "/pages/index/index",
                success: function(t) {
                    console.log(t);
                }
            };
        }
    });
}();