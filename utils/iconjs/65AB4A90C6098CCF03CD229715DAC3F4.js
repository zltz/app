!function() {
    function t(t) {
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
            if (Array.isArray(t) || (t = function(t, n) {
                if (t) {
                    if ("string" == typeof t) return r(t, n);
                    var a = Object.prototype.toString.call(t).slice(8, -1);
                    return "Object" === a && t.constructor && (a = t.constructor.name), "Map" === a || "Set" === a ? Array.from(a) : "Arguments" === a || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(a) ? r(t, n) : void 0;
                }
            }(t))) {
                var n = 0, a = function() {};
                return {
                    s: a,
                    n: function() {
                        return n >= t.length ? {
                            done: !0
                        } : {
                            done: !1,
                            value: t[n++]
                        };
                    },
                    e: function(t) {
                        throw t;
                    },
                    f: a
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var e, o, i = !0, l = !1;
        return {
            s: function() {
                e = t[Symbol.iterator]();
            },
            n: function() {
                var t = e.next();
                return i = t.done, t;
            },
            e: function(t) {
                l = !0, o = t;
            },
            f: function() {
                try {
                    i || null == e.return || e.return();
                } finally {
                    if (l) throw o;
                }
            }
        };
    }
    function r(t, r) {
        (null == r || r > t.length) && (r = t.length);
        for (var n = 0, a = new Array(r); n < r; n++) a[n] = t[n];
        return a;
    }
    !function() {
        var r = {
            isGradient: function(t) {
                return !(!t || !t.startsWith("linear") && !t.startsWith("radial"));
            },
            doGradient: function(t, r, a, e) {
                t.startsWith("linear") ? function(t, r, a, e) {
                    for (var o = function(t, r, n) {
                        var a, e = t.match(/([-]?\d{1,3})deg/);
                        switch (e && e[1] ? parseFloat(e[1]) : 0) {
                          case 0:
                            a = [ 0, -n / 2, 0, n / 2 ];
                            break;

                          case 90:
                            a = [ r / 2, 0, -r / 2, 0 ];
                            break;

                          case -90:
                            a = [ -r / 2, 0, r / 2, 0 ];
                            break;

                          case 180:
                            a = [ 0, n / 2, 0, -n / 2 ];
                            break;

                          case -180:
                            a = [ 0, -n / 2, 0, n / 2 ];
                            break;

                          default:
                            var o = 0, i = 0, l = 0, s = 0;
                            e[1] > 0 && e[1] < 90 ? (l = -(o = r / 2 - (r / 2 * Math.tan((90 - e[1]) * Math.PI * 2 / 360) - n / 2) * Math.sin(2 * (90 - e[1]) * Math.PI * 2 / 360) / 2), 
                            i = -(s = Math.tan((90 - e[1]) * Math.PI * 2 / 360) * o)) : e[1] > -180 && e[1] < -90 ? (l = -(o = -r / 2 + (r / 2 * Math.tan((90 - e[1]) * Math.PI * 2 / 360) - n / 2) * Math.sin(2 * (90 - e[1]) * Math.PI * 2 / 360) / 2), 
                            i = -(s = Math.tan((90 - e[1]) * Math.PI * 2 / 360) * o)) : e[1] > 90 && e[1] < 180 ? (l = -(o = r / 2 + (-r / 2 * Math.tan((90 - e[1]) * Math.PI * 2 / 360) - n / 2) * Math.sin(2 * (90 - e[1]) * Math.PI * 2 / 360) / 2), 
                            i = -(s = Math.tan((90 - e[1]) * Math.PI * 2 / 360) * o)) : (l = -(o = -r / 2 - (-r / 2 * Math.tan((90 - e[1]) * Math.PI * 2 / 360) - n / 2) * Math.sin(2 * (90 - e[1]) * Math.PI * 2 / 360) / 2), 
                            i = -(s = Math.tan((90 - e[1]) * Math.PI * 2 / 360) * o)), a = [ o, i, l, s ];
                        }
                        return a;
                    }(a, t, r), i = e.createLinearGradient(o[0], o[1], o[2], o[3]), l = a.match(/linear-gradient\((.+)\)/)[1], s = n(l.substring(l.indexOf(",") + 1)), c = 0; c < s.colors.length; c++) i.addColorStop(s.percents[c], s.colors[c]);
                    e.fillStyle = i;
                }(r, a, t, e) : t.startsWith("radial") && function(t, r, a, e) {
                    for (var o = n(a.match(/radial-gradient\((.+)\)/)[1]), i = e.createRadialGradient(0, 0, 0, 0, 0, t < r ? r / 2 : t / 2), l = 0; l < o.colors.length; l++) i.addColorStop(o.percents[l], o.colors[l]);
                    e.fillStyle = i;
                }(r, a, t, e);
            }
        };
        function n(r) {
            var n, a = [], e = [], o = t(r.substring(0, r.length - 1).split("%,"));
            try {
                for (o.s(); !(n = o.n()).done; ) {
                    var i = n.value;
                    a.push(i.substring(0, i.lastIndexOf(" ")).trim()), e.push(i.substring(i.lastIndexOf(" "), i.length) / 100);
                }
            } catch (t) {
                o.e(t);
            } finally {
                o.f();
            }
            return {
                colors: a,
                percents: e
            };
        }
        module.exports = {
            api: r
        };
    }();
}();