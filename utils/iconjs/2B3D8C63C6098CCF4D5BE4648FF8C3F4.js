var e, r, t, n;

e = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js"), r = e(require("546F4625C6098CCF32092E22607AC3F4.js")), 
t = e(require("1A41E661C6098CCF7C278E663FA9C3F4.js")), (n = require("cloudfunctions/checkImg/wx-server-sdk.js")).init(), 
exports.main = function() {
    var e = (0, t.default)(r.default.mark(function e(t, a) {
        var u;
        return r.default.wrap(function(e) {
            for (;;) switch (e.prev = e.next) {
              case 0:
                if (e.prev = 0, u = !1, !t.img) {
                    e.next = 6;
                    break;
                }
                return e.next = 5, n.openapi.security.imgSecCheck({
                    media: {
                        header: {
                            "Content-Type": "application/octet-stream"
                        },
                        contentType: "image/jpg",
                        value: Buffer.from(t.img)
                    }
                });

              case 5:
                u = e.sent;

              case 6:
                return e.abrupt("return", {
                    msgR: msgR,
                    imageR: u
                });

              case 10:
                return e.prev = 10, e.t0 = e.catch(0), e.abrupt("return", e.t0);

              case 13:
              case "end":
                return e.stop();
            }
        }, e, null, [ [ 0, 10 ] ]);
    }));
    return function(r, t) {
        return e.apply(this, arguments);
    };
}();