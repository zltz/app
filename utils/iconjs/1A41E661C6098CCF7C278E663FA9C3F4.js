!function() {
    function n(n, t, o, r, e, i, u) {
        try {
            var c = n[i](u), f = c.value;
        } catch (n) {
            return void o(n);
        }
        c.done ? t(f) : Promise.resolve(f).then(r, e);
    }
    module.exports = function(t) {
        return function() {
            var o = this, r = arguments;
            return new Promise(function(e, i) {
                var u = t.apply(o, r);
                function c(t) {
                    n(u, e, i, c, f, "next", t);
                }
                function f(t) {
                    n(u, e, i, c, f, "throw", t);
                }
                c(void 0);
            });
        };
    };
}();