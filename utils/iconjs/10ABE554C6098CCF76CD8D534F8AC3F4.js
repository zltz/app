var t, i, h, s, e, c;

t = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js"), i = t(require("CBDBE653C6098CCFADBD8E5437B9C3F4.js")), 
h = t(require("2A825192C6098CCF4CE4399578C9C3F4.js")), s = 17, e = function() {
    function t(h, s) {
        (0, i.default)(this, t), this.id = h, this.list = [], this.ctx = wx.createCanvasContext(h), 
        this.selectedItem = null, this.touchItem = !1, this.bg = s, this.initbg(), this.ctx.draw();
    }
    return (0, h.default)(t, [ {
        key: "initbg",
        value: function() {
            var t = this.bg, i = t.url, h = t.width, s = t.height, e = t.x, c = t.y;
            this.ctx.beginPath(), this.ctx.drawImage(i, e, c, h, s), this.ctx.closePath();
        }
    }, {
        key: "cancelSelected",
        value: function() {
            var t = this;
            this.initbg(), this.list.forEach(function(i, h) {
                i.draw(t.ctx, !1);
            }), this.ctx.draw(!1);
        }
    }, {
        key: "saveCanvas",
        value: function() {
            var t = this;
            return this.cancelSelected(), new Promise(function(i, h) {
                wx.canvasToTempFilePath({
                    canvasId: t.id,
                    success: function(t) {
                        i(t);
                    },
                    fail: function() {
                        h(res);
                    }
                });
            });
        }
    }, {
        key: "addList",
        value: function(t) {
            this.list.push(t);
        }
    }, {
        key: "isTouchItem",
        value: function(t, i, h) {
            var s = 13.5, e = (Math.sqrt(Math.pow(t.touch.countWid, 2) + Math.pow(t.touch.countWid, 2)) - t.width) / 2, c = t.x - s - e, a = t.y - s - e, n = t.x + t.width + s + e, u = t.y + t.width + s + e;
            return i > c && i < n && h > a && h < u;
        }
    }, {
        key: "hasClickItem",
        value: function(t, i) {
            var h = this, s = [];
            return this.list.forEach(function(e, c) {
                h.isTouchItem(e, t, i) && s.unshift({
                    item: e,
                    idx: c
                });
            }), s.length > 0 && s[0];
        }
    }, {
        key: "isClickClose",
        value: function(t, i, h) {
            var e = t.touch.clsx, c = t.touch.clsy, a = e - s / 3, n = e + s + s / 3, u = c - s / 3, o = c + s + s / 3;
            return console.log(a, u, n, o, s), i > a && i < n && h > u && h < o;
        }
    }, {
        key: "draw",
        value: function() {
            var t = this;
            this.initbg(), this.list.forEach(function(i, h) {
                var s = !1;
                h == t.list.length - 1 && (t.selectedItem = i, s = !0), i.draw(t.ctx, s);
            }), this.ctx.draw(!1);
        }
    }, {
        key: "touchStart",
        value: function(t, i) {
            var h = this;
            if (0 != this.list.length) {
                var s = this.hasClickItem(t, i), e = "boolean" != typeof s;
                if (this.touchItem = e, console.log(e, "是否点中了元素"), e) {
                    var c = null;
                    this.initbg(), this.list.forEach(function(e, a) {
                        var n = !1;
                        h.isClickClose(e, t, i) ? c = a : (a == s.idx && (n = !0, e.touchStart(t, i), h.selectedItem = e), 
                        e.draw(h.ctx, n));
                    }), this.ctx.draw(!1), null != c && (this.list.splice(c, 1), this.draw());
                } else this.cancelSelected(), this.selectedItem;
            }
        }
    }, {
        key: "touchScale",
        value: function(t, i) {
            var h = this;
            null != this.selectedItem && this.touchItem && 0 != this.list.length && (this.initbg(), 
            this.selectedItem.change(t, i), this.list.forEach(function(t, i) {
                var s = !1;
                h.selectedItem == t && (s = !0), t.draw(h.ctx, s);
            }), this.ctx.draw(!1));
        }
    }, {
        key: "touchEnd",
        value: function() {
            null != this.selectedItem && 0 != this.list.length && this.selectedItem.scaleEnd();
        }
    } ]), t;
}(), c = function() {
    function t(h) {
        var s = h.url, e = h.width, c = h.height, a = h.x, n = h.y;
        (0, i.default)(this, t), this.url = s, this.width = e || 0, this.height = c || 0, 
        this.x = a, this.y = n, this.scalenum = 0, this.cenx = a + e / 2, this.ceny = n + c / 2, 
        this.rotate = 0, this.touchx = a + e, this.touchy = n + c, this.cache = {
            route: 0,
            scalenum: 0,
            x: a,
            y: n
        }, this.touch = {
            countWid: e,
            countHei: c,
            btnx: a + e - 8.5,
            btny: n + c - 8.5,
            clsx: a - 8.5,
            clsy: n - 8.5
        }, this.routeline = 0, this.isTouchBtn = !1;
    }
    return (0, h.default)(t, [ {
        key: "draw",
        value: function(t) {
            var i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], h = this.width + 2 * this.scalenum, s = this.height + 2 * this.scalenum, e = this.x - this.scalenum, c = this.y - this.scalenum;
            this.touch.countWid = h, this.touch.countHei = s, t.beginPath(), t.save(), t.translate(this.cenx, this.ceny), 
            t.rotate(-(this.rotate + this.cache.route) * Math.PI / 180), t.translate(-this.cenx, -this.ceny), 
            i && this.selected(t, e, c, h, s), t.drawImage(this.url, e, c, h, s), t.restore(), 
            t.closePath();
        }
    }, {
        key: "selected",
        value: function(t, i, h, e, c) {
            t.setStrokeStyle("#ffd3d6"), t.setLineWidth(2), t.setLineDash([ 10, 10 ], 12), t.strokeRect(i, h, e, c), 
            t.drawImage("/images/change.png", i + e - 8.5, h + c - 8.5, s, s), t.drawImage("/images/cls.png", i - 8.5, h - 8.5, s, s), 
            t.fill();
        }
    }, {
        key: "touchStart",
        value: function(t, i) {
            this.touchx = t, this.touchy = i, this.isTouchBtn = this.isBtn(t, i), console.log(this.isTouchBtn, "是否点中了按钮");
        }
    }, {
        key: "change",
        value: function(t, i) {
            this.touch.lastMovex = t, this.touch.lastMovey = i, this.touch.distx = this.touch.lastMovex - this.touchx, 
            this.touch.disty = this.touch.lastMovey - this.touchy, this.isTouchBtn ? this.rotateScale(t, i) : this.move(t, i);
        }
    }, {
        key: "isBtn",
        value: function(t, i) {
            var h = this.touch.btnx, s = this.touch.btny;
            return t > h - 8.5 - 0 && t < h + 25.5 + 0 && i > s - 8.5 - 0 && i < s + 25.5 + 0;
        }
    }, {
        key: "move",
        value: function(t, i) {
            this.x = this.cache.x + this.touch.distx, this.y = this.cache.y + this.touch.disty, 
            this.cenx = this.x + this.width / 2, this.ceny = this.y + this.height / 2;
        }
    }, {
        key: "rotateScale",
        value: function(t, i) {
            var h = Math.atan2(this.touchx - this.cenx, this.touchy - this.ceny) / Math.PI * 180, s = Math.atan2(t - this.cenx, i - this.ceny) / Math.PI * 180, e = Math.sqrt(Math.pow(t - this.cenx, 2) + Math.pow(i - this.ceny, 2)), c = Math.sqrt(Math.pow(e, 2) + Math.pow(e, 2));
            this.cenx, this.width, this.ceny, this.height, this.cache.route = s - h, c >= 60 && (this.scalenum = (c - this.width) / 2, 
            this.cache.scalenum = (c - this.width) / 2);
        }
    }, {
        key: "scaleEnd",
        value: function() {
            this.isTouchBtn ? (this.touch.clsx -= this.touch.distx || 0, this.touch.clsy -= this.touch.disty || 0) : (this.cache.x = this.x, 
            this.cache.y = this.y, this.touch.clsx += this.touch.distx || 0, this.touch.clsy += this.touch.disty || 0), 
            this.touch.btnx += this.touch.distx || 0, this.touch.btny += this.touch.disty || 0, 
            this.touch.distx = 0, this.touch.disty = 0, this.rotate = this.rotate + this.cache.route, 
            this.cache.route = 0;
        }
    } ]), t;
}(), module.exports = {
    Canvas: e,
    CanvasItem: c
};