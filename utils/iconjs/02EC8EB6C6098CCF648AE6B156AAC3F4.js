!function() {
    var t = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js");
    Object.defineProperty(exports, "__esModule", {
        value: !0
    }), exports.default = void 0;
    var e = t(require("546F4625C6098CCF32092E22607AC3F4.js")), n = t(require("1A41E661C6098CCF7C278E663FA9C3F4.js")), a = t(require("52A6C852C6098CCF34C0A0558E4AC3F4.js")), s = t(require("CBDBE653C6098CCFADBD8E5437B9C3F4.js")), i = t(require("2A825192C6098CCF4CE4399578C9C3F4.js"));
    function r(t) {
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
            if (Array.isArray(t) || (t = function(t, e) {
                if (t) {
                    if ("string" == typeof t) return o(t, e);
                    var n = Object.prototype.toString.call(t).slice(8, -1);
                    return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(n) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? o(t, e) : void 0;
                }
            }(t))) {
                var e = 0, n = function() {};
                return {
                    s: n,
                    n: function() {
                        return e >= t.length ? {
                            done: !0
                        } : {
                            done: !1,
                            value: t[e++]
                        };
                    },
                    e: function(t) {
                        throw t;
                    },
                    f: n
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var a, s, i = !0, r = !1;
        return {
            s: function() {
                a = t[Symbol.iterator]();
            },
            n: function() {
                var t = a.next();
                return i = t.done, t;
            },
            e: function(t) {
                r = !0, s = t;
            },
            f: function() {
                try {
                    i || null == a.return || a.return();
                } finally {
                    if (r) throw s;
                }
            }
        };
    }
    function o(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var n = 0, a = new Array(e); n < e; n++) a[n] = t[n];
        return a;
    }
    var c = function() {
        function t(e, n, a, i, r) {
            (0, s.default)(this, t), this.ctx = void 0, this.type = void 0, this.canvasId = void 0, 
            this.canvasNode = void 0, this.stepList = [], this.canvasPrototype = {}, this.ctx = n, 
            this.canvasId = a, this.type = e, i && (this.canvasNode = r || {});
        }
        return (0, i.default)(t, [ {
            key: "save",
            value: function() {
                this.stepList.push({
                    action: "save",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "restore",
            value: function() {
                this.stepList.push({
                    action: "restore",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "setLineDash",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.canvasPrototype.lineDash = e, this.stepList.push({
                    action: "setLineDash",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "moveTo",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "moveTo",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "closePath",
            value: function() {
                this.stepList.push({
                    action: "closePath",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "lineTo",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "lineTo",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "quadraticCurveTo",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "quadraticCurveTo",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "bezierCurveTo",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "bezierCurveTo",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "arcTo",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "arcTo",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "arc",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "arc",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "rect",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "rect",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "scale",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "scale",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "rotate",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "rotate",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "translate",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "translate",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "transform",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "transform",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "setTransform",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "setTransform",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "clearRect",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "clearRect",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "fillRect",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "fillRect",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "strokeRect",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "strokeRect",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "fillText",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "fillText",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "strokeText",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "strokeText",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "beginPath",
            value: function() {
                this.stepList.push({
                    action: "beginPath",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "fill",
            value: function() {
                this.stepList.push({
                    action: "fill",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "stroke",
            value: function() {
                this.stepList.push({
                    action: "stroke",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "drawFocusIfNeeded",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "drawFocusIfNeeded",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "clip",
            value: function() {
                this.stepList.push({
                    action: "clip",
                    args: null,
                    actionType: "func"
                });
            }
        }, {
            key: "isPointInPath",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "isPointInPath",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "drawImage",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "drawImage",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "addHitRegion",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "addHitRegion",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "removeHitRegion",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "removeHitRegion",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "clearHitRegions",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "clearHitRegions",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "putImageData",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                this.stepList.push({
                    action: "putImageData",
                    args: e,
                    actionType: "func"
                });
            }
        }, {
            key: "getLineDash",
            value: function() {
                return this.canvasPrototype.lineDash;
            }
        }, {
            key: "createLinearGradient",
            value: function() {
                var t;
                return (t = this.ctx).createLinearGradient.apply(t, arguments);
            }
        }, {
            key: "createRadialGradient",
            value: function() {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                var s, i;
                return "2d" === this.type ? (s = this.ctx).createRadialGradient.apply(s, e) : (i = this.ctx).createCircularGradient.apply(i, (0, 
                a.default)(e.slice(3, 6)));
            }
        }, {
            key: "createPattern",
            value: function() {
                var t;
                return (t = this.ctx).createPattern.apply(t, arguments);
            }
        }, {
            key: "measureText",
            value: function() {
                var t;
                return (t = this.ctx).measureText.apply(t, arguments);
            }
        }, {
            key: "createImageData",
            value: function() {
                var t;
                return (t = this.ctx).createImageData.apply(t, arguments);
            }
        }, {
            key: "getImageData",
            value: function() {
                var t;
                return (t = this.ctx).getImageData.apply(t, arguments);
            }
        }, {
            key: "draw",
            value: function() {
                var t = (0, n.default)(e.default.mark(function t(n, a) {
                    var s, i, o, c, u, h, l;
                    return e.default.wrap(function(t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            if (s = this.stepList.slice(), this.stepList.length = 0, "mina" !== this.type) {
                                t.next = 7;
                                break;
                            }
                            if (s.length > 0) {
                                i = r(s);
                                try {
                                    for (i.s(); !(o = i.n()).done; ) c = o.value, this.implementMinaStep(c);
                                } catch (t) {
                                    i.e(t);
                                } finally {
                                    i.f();
                                }
                                s.length = 0;
                            }
                            this.ctx.draw(n, a), t.next = 29;
                            break;

                          case 7:
                            if ("2d" !== this.type) {
                                t.next = 29;
                                break;
                            }
                            if (n || this.ctx.clearRect(0, 0, this.canvasNode.width, this.canvasNode.height), 
                            !(s.length > 0)) {
                                t.next = 28;
                                break;
                            }
                            u = r(s), t.prev = 11, u.s();

                          case 13:
                            if ((h = u.n()).done) {
                                t.next = 19;
                                break;
                            }
                            return l = h.value, t.next = 17, this.implement2DStep(l);

                          case 17:
                            t.next = 13;
                            break;

                          case 19:
                            t.next = 24;
                            break;

                          case 21:
                            t.prev = 21, t.t0 = t.catch(11), u.e(t.t0);

                          case 24:
                            return t.prev = 24, u.f(), t.finish(24);

                          case 27:
                            s.length = 0;

                          case 28:
                            a && a();

                          case 29:
                            s.length = 0;

                          case 30:
                          case "end":
                            return t.stop();
                        }
                    }, t, this, [ [ 11, 21, 24, 27 ] ]);
                }));
                return function(e, n) {
                    return t.apply(this, arguments);
                };
            }()
        }, {
            key: "implementMinaStep",
            value: function(t) {
                switch (t.action) {
                  case "textAlign":
                    this.ctx.setTextAlign(t.args);
                    break;

                  case "textBaseline":
                    this.ctx.setTextBaseline(t.args);
                    break;

                  default:
                    if ("set" === t.actionType) this.ctx[t.action] = t.args; else if ("func" === t.actionType) {
                        var e;
                        t.args ? (e = this.ctx)[t.action].apply(e, (0, a.default)(t.args)) : this.ctx[t.action]();
                    }
                }
            }
        }, {
            key: "implement2DStep",
            value: function(t) {
                var e = this;
                return new Promise(function(n) {
                    if ("drawImage" === t.action) {
                        var s = e.canvasNode.createImage();
                        s.src = t.args[0], s.onload = function() {
                            var i;
                            (i = e.ctx).drawImage.apply(i, [ s ].concat((0, a.default)(t.args.slice(1)))), n();
                        };
                    } else {
                        if ("set" === t.actionType) e.ctx[t.action] = t.args; else if ("func" === t.actionType) {
                            var i;
                            t.args ? (i = e.ctx)[t.action].apply(i, (0, a.default)(t.args)) : e.ctx[t.action]();
                        }
                        n();
                    }
                });
            }
        }, {
            key: "width",
            set: function(t) {
                this.canvasNode && (this.canvasNode.width = t);
            },
            get: function() {
                return this.canvasNode ? this.canvasNode.width : 0;
            }
        }, {
            key: "height",
            set: function(t) {
                this.canvasNode && (this.canvasNode.height = t);
            },
            get: function() {
                return this.canvasNode ? this.canvasNode.height : 0;
            }
        }, {
            key: "lineWidth",
            set: function(t) {
                this.canvasPrototype.lineWidth = t, this.stepList.push({
                    action: "lineWidth",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.lineWidth;
            }
        }, {
            key: "lineCap",
            set: function(t) {
                this.canvasPrototype.lineCap = t, this.stepList.push({
                    action: "lineCap",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.lineCap;
            }
        }, {
            key: "lineJoin",
            set: function(t) {
                this.canvasPrototype.lineJoin = t, this.stepList.push({
                    action: "lineJoin",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.lineJoin;
            }
        }, {
            key: "miterLimit",
            set: function(t) {
                this.canvasPrototype.miterLimit = t, this.stepList.push({
                    action: "miterLimit",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.miterLimit;
            }
        }, {
            key: "lineDashOffset",
            set: function(t) {
                this.canvasPrototype.lineDashOffset = t, this.stepList.push({
                    action: "lineDashOffset",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.lineDashOffset;
            }
        }, {
            key: "font",
            set: function(t) {
                this.canvasPrototype.font = t, this.ctx.font = t, this.stepList.push({
                    action: "font",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.font;
            }
        }, {
            key: "textAlign",
            set: function(t) {
                this.canvasPrototype.textAlign = t, this.stepList.push({
                    action: "textAlign",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.textAlign;
            }
        }, {
            key: "textBaseline",
            set: function(t) {
                this.canvasPrototype.textBaseline = t, this.stepList.push({
                    action: "textBaseline",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.textBaseline;
            }
        }, {
            key: "fillStyle",
            set: function(t) {
                this.canvasPrototype.fillStyle = t, this.stepList.push({
                    action: "fillStyle",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.fillStyle;
            }
        }, {
            key: "strokeStyle",
            set: function(t) {
                this.canvasPrototype.strokeStyle = t, this.stepList.push({
                    action: "strokeStyle",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.strokeStyle;
            }
        }, {
            key: "globalAlpha",
            set: function(t) {
                this.canvasPrototype.globalAlpha = t, this.stepList.push({
                    action: "globalAlpha",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.globalAlpha;
            }
        }, {
            key: "globalCompositeOperation",
            set: function(t) {
                this.canvasPrototype.globalCompositeOperation = t, this.stepList.push({
                    action: "globalCompositeOperation",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.globalCompositeOperation;
            }
        }, {
            key: "shadowColor",
            set: function(t) {
                this.canvasPrototype.shadowColor = t, this.stepList.push({
                    action: "shadowColor",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.shadowColor;
            }
        }, {
            key: "shadowOffsetX",
            set: function(t) {
                this.canvasPrototype.shadowOffsetX = t, this.stepList.push({
                    action: "shadowOffsetX",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.shadowOffsetX;
            }
        }, {
            key: "shadowOffsetY",
            set: function(t) {
                this.canvasPrototype.shadowOffsetY = t, this.stepList.push({
                    action: "shadowOffsetY",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.shadowOffsetY;
            }
        }, {
            key: "shadowBlur",
            set: function(t) {
                this.canvasPrototype.shadowBlur = t, this.stepList.push({
                    action: "shadowBlur",
                    args: t,
                    actionType: "set"
                });
            },
            get: function() {
                return this.canvasPrototype.shadowBlur;
            }
        } ]), t;
    }();
    exports.default = c;
}();