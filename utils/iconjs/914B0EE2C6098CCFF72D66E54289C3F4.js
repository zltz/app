!function() {
    var e = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js");
    Object.defineProperty(exports, "__esModule", {
        value: !0
    }), exports.default = void 0;
    var n = e(require("035EDC83C6098CCF6538B484E06AC3F4.js")), t = e(require("210D2926C6098CCF476B4121DF3AC3F4.js")), i = e(require("CBDBE653C6098CCFADBD8E5437B9C3F4.js")), r = e(require("2A825192C6098CCF4CE4399578C9C3F4.js"));
    function o(e) {
        if ("undefined" == typeof Symbol || null == e[Symbol.iterator]) {
            if (Array.isArray(e) || (e = function(e, n) {
                if (e) {
                    if ("string" == typeof e) return a(e, n);
                    var t = Object.prototype.toString.call(e).slice(8, -1);
                    return "Object" === t && e.constructor && (t = e.constructor.name), "Map" === t || "Set" === t ? Array.from(t) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? a(e, n) : void 0;
                }
            }(e))) {
                var n = 0, t = function() {};
                return {
                    s: t,
                    n: function() {
                        return n >= e.length ? {
                            done: !0
                        } : {
                            done: !1,
                            value: e[n++]
                        };
                    },
                    e: function(e) {
                        throw e;
                    },
                    f: t
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var i, r, o = !0, c = !1;
        return {
            s: function() {
                i = e[Symbol.iterator]();
            },
            n: function() {
                var e = i.next();
                return o = e.done, e;
            },
            e: function(e) {
                c = !0, r = e;
            },
            f: function() {
                try {
                    o || null == i.return || i.return();
                } finally {
                    if (c) throw r;
                }
            }
        };
    }
    function a(e, n) {
        (null == n || n > e.length) && (n = e.length);
        for (var t = 0, i = new Array(n); t < n; t++) i[t] = e[t];
        return i;
    }
    var c = require("A36B88C2C6098CCFC50DE0C5FB39C3F4.js"), f = require("A5D1CB15C6098CCFC3B7A3121169C3F4.js"), l = "savedFiles", s = "totalSize", u = "path", d = "time", v = "size", h = 6291456, g = {}, y = function() {
        function e() {
            (0, i.default)(this, e), getApp().PAINTER_MAX_LRU_SPACE && (h = getApp().PAINTER_MAX_LRU_SPACE), 
            wx.getStorage({
                key: l,
                success: function(e) {
                    e.data && (g = e.data);
                }
            });
        }
        return (0, r.default)(e, [ {
            key: "download",
            value: function(e, n) {
                return new Promise(function(i, r) {
                    if (e && c.isValidUrl(e)) {
                        var o = function(e) {
                            if (c.isDataUrl(e)) {
                                var n = /data:image\/(\w+);base64,(.*)/.exec(e) || [], i = (0, t.default)(n, 3), r = i[1], o = i[2];
                                return "".concat(f.hex_sha1(o), ".").concat(r);
                            }
                            return e;
                        }(e);
                        if (n) {
                            var a = function(e) {
                                if (g[e]) return g[e].time = new Date().getTime(), wx.setStorage({
                                    key: l,
                                    data: g
                                }), g[e];
                            }(o);
                            a ? -1 !== a[u].indexOf("//usr/") ? wx.getFileInfo({
                                filePath: a[u],
                                success: function() {
                                    i(a[u]);
                                },
                                fail: function(t) {
                                    console.error("base64 file broken, ".concat(JSON.stringify(t))), w(e, n).then(function(e) {
                                        i(e);
                                    }, function() {
                                        r();
                                    });
                                }
                            }) : wx.getSavedFileInfo({
                                filePath: a[u],
                                success: function(e) {
                                    i(a[u]);
                                },
                                fail: function(t) {
                                    console.error("the file is broken, redownload it, ".concat(JSON.stringify(t))), 
                                    S(e, n).then(function(e) {
                                        i(e);
                                    }, function() {
                                        r();
                                    });
                                }
                            }) : c.isOnlineUrl(e) ? S(e, n).then(function(e) {
                                i(e);
                            }, function() {
                                r();
                            }) : c.isDataUrl(e) && w(e, n).then(function(e) {
                                i(e);
                            }, function() {
                                r();
                            });
                        } else wx.getFileInfo({
                            filePath: o,
                            success: function() {
                                i(e);
                            },
                            fail: function() {
                                c.isOnlineUrl(e) ? S(e, n).then(function(e) {
                                    i(e);
                                }, function() {
                                    r();
                                }) : c.isDataUrl(e) && w(e, n).then(function(e) {
                                    i(e);
                                }, function() {
                                    r();
                                });
                            }
                        });
                    } else i(e);
                });
            }
        } ]), e;
    }();
    function w(e, n) {
        return new Promise(function(i, r) {
            var o = /data:image\/(\w+);base64,(.*)/.exec(e) || [], a = (0, t.default)(o, 3), c = a[1], l = a[2];
            if (!c) return console.error("base parse failed"), void r();
            var s = "".concat(f.hex_sha1(l), ".").concat(c), u = "".concat(wx.env.USER_DATA_PATH, "/").concat(s), d = wx.base64ToArrayBuffer(l.replace(/[\r\n]/g, ""));
            wx.getFileSystemManager().writeFile({
                filePath: u,
                data: d,
                encoding: "binary",
                success: function() {
                    wx.getFileInfo({
                        filePath: u,
                        success: function(e) {
                            var t = e.size;
                            n ? b(t).then(function() {
                                m(s, t, u, !0).then(function(e) {
                                    i(e);
                                });
                            }, function() {
                                i(u);
                            }) : i(u);
                        },
                        fail: function(e) {
                            console.error("getFileInfo ".concat(u, " failed, ").concat(JSON.stringify(e))), 
                            i(u);
                        }
                    });
                },
                fail: function(e) {
                    console.log(e);
                }
            });
        });
    }
    function S(e, n) {
        return new Promise(function(t, i) {
            wx.downloadFile({
                url: e,
                success: function(r) {
                    if (200 !== r.statusCode) return console.error("downloadFile ".concat(e, " failed res.statusCode is not 200")), 
                    void i();
                    var o = r.tempFilePath;
                    wx.getFileInfo({
                        filePath: o,
                        success: function(i) {
                            var r = i.size;
                            n ? b(r).then(function() {
                                m(e, r, o).then(function(e) {
                                    t(e);
                                });
                            }, function() {
                                t(o);
                            }) : t(o);
                        },
                        fail: function(e) {
                            console.error("getFileInfo ".concat(r.tempFilePath, " failed, ").concat(JSON.stringify(e))), 
                            t(r.tempFilePath);
                        }
                    });
                },
                fail: function(e) {
                    console.error("downloadFile failed, ".concat(JSON.stringify(e), " ")), i();
                }
            });
        });
    }
    function m(e, n, t) {
        var i = arguments.length > 3 && void 0 !== arguments[3] && arguments[3];
        return new Promise(function(r, o) {
            if (i) {
                var a = g[s] ? g[s] : 0;
                return g[e] = {}, g[e][u] = t, g[e][d] = new Date().getTime(), g[e][v] = n, g.totalSize = n + a, 
                wx.setStorage({
                    key: l,
                    data: g
                }), void r(t);
            }
            wx.saveFile({
                tempFilePath: t,
                success: function(t) {
                    var i = g[s] ? g[s] : 0;
                    g[e] = {}, g[e][u] = t.savedFilePath, g[e][d] = new Date().getTime(), g[e][v] = n, 
                    g.totalSize = n + i, wx.setStorage({
                        key: l,
                        data: g
                    }), r(t.savedFilePath);
                },
                fail: function(n) {
                    console.error("saveFile ".concat(e, " failed, then we delete all files, ").concat(JSON.stringify(n))), 
                    r(t), wx.removeStorage({
                        key: l,
                        success: function() {
                            wx.getSavedFileList({
                                success: function(e) {
                                    p(e.fileList);
                                },
                                fail: function(e) {
                                    console.error("getSavedFileList failed, ".concat(JSON.stringify(e)));
                                }
                            });
                        }
                    });
                }
            });
        });
    }
    function b(e) {
        return e > h ? Promise.reject() : new Promise(function(n, t) {
            var i = g[s] ? g[s] : 0;
            if (e + i <= h) n(); else {
                var r = [], a = JSON.parse(JSON.stringify(g));
                delete a[s];
                var c, f = o(Object.keys(a).sort(function(e, n) {
                    return a[e][d] - a[n][d];
                }));
                try {
                    for (f.s(); !(c = f.n()).done; ) {
                        var v = c.value;
                        if (i -= g[v].size, r.push(g[v][u]), delete g[v], i + e < h) break;
                    }
                } catch (e) {
                    f.e(e);
                } finally {
                    f.f();
                }
                g.totalSize = i, wx.setStorage({
                    key: l,
                    data: g,
                    success: function() {
                        r.length > 0 && p(r), n();
                    },
                    fail: function(e) {
                        console.error("doLru setStorage failed, ".concat(JSON.stringify(e))), t();
                    }
                });
            }
        });
    }
    function p(e) {
        var t, i = o(e);
        try {
            var r = function() {
                var e = t.value, i = e;
                "object" === (0, n.default)(e) && (i = e.filePath), -1 !== i.indexOf("//usr/") ? wx.getFileSystemManager().unlink({
                    filePath: i,
                    fail: function(n) {
                        console.error("removeSavedFile ".concat(e, " failed, ").concat(JSON.stringify(n)));
                    }
                }) : wx.removeSavedFile({
                    filePath: i,
                    fail: function(n) {
                        console.error("removeSavedFile ".concat(e, " failed, ").concat(JSON.stringify(n)));
                    }
                });
            };
            for (i.s(); !(t = i.n()).done; ) r();
        } catch (e) {
            i.e(e);
        } finally {
            i.f();
        }
    }
    exports.default = y;
}();