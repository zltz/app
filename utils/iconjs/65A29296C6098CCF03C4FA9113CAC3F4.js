!function() {
    var t = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js");
    Object.defineProperty(exports, "__esModule", {
        value: !0
    }), exports.default = exports.clearPenCache = exports.penCache = void 0;
    var e = t(require("52A6C852C6098CCF34C0A0558E4AC3F4.js")), s = t(require("CBDBE653C6098CCFADBD8E5437B9C3F4.js")), i = t(require("2A825192C6098CCF4CE4399578C9C3F4.js"));
    function r(t) {
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
            if (Array.isArray(t) || (t = function(t, e) {
                if (t) {
                    if ("string" == typeof t) return o(t, e);
                    var s = Object.prototype.toString.call(t).slice(8, -1);
                    return "Object" === s && t.constructor && (s = t.constructor.name), "Map" === s || "Set" === s ? Array.from(s) : "Arguments" === s || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(s) ? o(t, e) : void 0;
                }
            }(t))) {
                var e = 0, s = function() {};
                return {
                    s: s,
                    n: function() {
                        return e >= t.length ? {
                            done: !0
                        } : {
                            done: !1,
                            value: t[e++]
                        };
                    },
                    e: function(t) {
                        throw t;
                    },
                    f: s
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var i, r, h = !0, c = !1;
        return {
            s: function() {
                i = t[Symbol.iterator]();
            },
            n: function() {
                var t = i.next();
                return h = t.done, t;
            },
            e: function(t) {
                c = !0, r = t;
            },
            f: function() {
                try {
                    h || null == i.return || i.return();
                } finally {
                    if (c) throw r;
                }
            }
        };
    }
    function o(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var s = 0, i = new Array(e); s < e; s++) i[s] = t[s];
        return i;
    }
    var h = require("2F4C87F7C6098CCF492AEFF057BAC3F4.js"), c = require("65AB4A90C6098CCF03CD229715DAC3F4.js"), a = {
        viewRect: {},
        textLines: {}
    };
    exports.penCache = a, exports.clearPenCache = function(t) {
        t ? (a.viewRect[t] = null, a.textLines[t] = null) : (a.viewRect = {}, a.textLines = {});
    };
    var n = function() {
        function t(e, i) {
            (0, s.default)(this, t), this.ctx = e, this.data = i;
        }
        return (0, i.default)(t, [ {
            key: "paint",
            value: function(t) {
                this.style = {
                    width: this.data.width.toPx(),
                    height: this.data.height.toPx()
                }, this._background();
                var e, s = r(this.data.views);
                try {
                    for (s.s(); !(e = s.n()).done; ) {
                        var i = e.value;
                        this._drawAbsolute(i);
                    }
                } catch (t) {
                    s.e(t);
                } finally {
                    s.f();
                }
                this.ctx.draw(!1, function() {
                    t && t();
                });
            }
        }, {
            key: "_background",
            value: function() {
                this.ctx.save();
                var t = this.style, e = t.width, s = t.height, i = this.data.background;
                this.ctx.translate(e / 2, s / 2), this._doClip(this.data.borderRadius, e, s), i ? i.startsWith("#") || i.startsWith("rgba") || "transparent" === i.toLowerCase() ? (this.ctx.fillStyle = i, 
                this.ctx.fillRect(-e / 2, -s / 2, e, s)) : c.api.isGradient(i) ? (c.api.doGradient(i, e, s, this.ctx), 
                this.ctx.fillRect(-e / 2, -s / 2, e, s)) : this.ctx.drawImage(i, -e / 2, -s / 2, e, s) : (this.ctx.fillStyle = "transparent", 
                this.ctx.fillRect(-e / 2, -s / 2, e, s)), this.ctx.restore();
            }
        }, {
            key: "_drawAbsolute",
            value: function(t) {
                if (t && t.type) switch (t.css && t.css.length && (t.css = Object.assign.apply(Object, (0, 
                e.default)(t.css))), t.type) {
                  case "image":
                    this._drawAbsImage(t);
                    break;

                  case "text":
                    this._fillAbsText(t);
                    break;

                  case "rect":
                    this._drawAbsRect(t);
                    break;

                  case "qrcode":
                    this._drawQRCode(t);
                }
            }
        }, {
            key: "_border",
            value: function(t) {
                var e = t.borderRadius, s = void 0 === e ? 0 : e, i = t.width, r = t.height, o = t.borderWidth, h = void 0 === o ? 0 : o, c = t.borderStyle, a = void 0 === c ? "solid" : c, n = 0, l = 0, d = 0, x = 0, u = Math.min(i, r);
                if (s) {
                    var f = s.split(/\s+/);
                    4 === f.length ? (n = Math.min(f[0].toPx(!1, u), i / 2, r / 2), l = Math.min(f[1].toPx(!1, u), i / 2, r / 2), 
                    d = Math.min(f[2].toPx(!1, u), i / 2, r / 2), x = Math.min(f[3].toPx(!1, u), i / 2, r / 2)) : n = l = d = x = Math.min(s && s.toPx(!1, u), i / 2, r / 2);
                }
                var g = h && h.toPx(!1, u);
                this.ctx.lineWidth = g, "dashed" === a ? this.ctx.setLineDash([ 4 * g / 3, 4 * g / 3 ]) : "dotted" === a && this.ctx.setLineDash([ g, g ]);
                var v = "solid" !== a;
                this.ctx.beginPath(), v && 0 === n && this.ctx.moveTo(-i / 2 - g, -r / 2 - g / 2), 
                0 !== n && this.ctx.arc(-i / 2 + n, -r / 2 + n, n + g / 2, 1 * Math.PI, 1.5 * Math.PI), 
                this.ctx.lineTo(0 === l ? v ? i / 2 : i / 2 + g / 2 : i / 2 - l, -r / 2 - g / 2), 
                v && 0 === l && this.ctx.moveTo(i / 2 + g / 2, -r / 2 - g), 0 !== l && this.ctx.arc(i / 2 - l, -r / 2 + l, l + g / 2, 1.5 * Math.PI, 2 * Math.PI), 
                this.ctx.lineTo(i / 2 + g / 2, 0 === d ? v ? r / 2 : r / 2 + g / 2 : r / 2 - d), 
                v && 0 === d && this.ctx.moveTo(i / 2 + g, r / 2 + g / 2), 0 !== d && this.ctx.arc(i / 2 - d, r / 2 - d, d + g / 2, 0, .5 * Math.PI), 
                this.ctx.lineTo(0 === x ? v ? -i / 2 : -i / 2 - g / 2 : -i / 2 + x, r / 2 + g / 2), 
                v && 0 === x && this.ctx.moveTo(-i / 2 - g / 2, r / 2 + g), 0 !== x && this.ctx.arc(-i / 2 + x, r / 2 - x, x + g / 2, .5 * Math.PI, 1 * Math.PI), 
                this.ctx.lineTo(-i / 2 - g / 2, 0 === n ? v ? -r / 2 : -r / 2 - g / 2 : -r / 2 + n), 
                v && 0 === n && this.ctx.moveTo(-i / 2 - g, -r / 2 - g / 2), v || this.ctx.closePath();
            }
        }, {
            key: "_doClip",
            value: function(t, e, s, i) {
                t && e && s && (this.ctx.globalAlpha = 0, this.ctx.fillStyle = "white", this._border({
                    borderRadius: t,
                    width: e,
                    height: s,
                    borderStyle: i
                }), this.ctx.fill(), getApp().systemInfo && getApp().systemInfo.version <= "6.6.6" && "ios" === getApp().systemInfo.platform || this.ctx.clip(), 
                this.ctx.globalAlpha = 1);
            }
        }, {
            key: "_doBorder",
            value: function(t, e, s) {
                if (t.css) {
                    var i = t.css, r = i.borderRadius, o = i.borderWidth, h = i.borderColor, c = i.borderStyle;
                    o && (this.ctx.save(), this._preProcess(t, !0), this.ctx.strokeStyle = h || "black", 
                    this._border({
                        borderRadius: r,
                        width: e,
                        height: s,
                        borderWidth: o,
                        borderStyle: c
                    }), this.ctx.stroke(), this.ctx.restore());
                }
            }
        }, {
            key: "_preProcess",
            value: function(t, e) {
                var s, i, r, o, h = 0, c = this._doPaddings(t);
                switch (t.type) {
                  case "text":
                    for (var n = String(t.text).split("\n"), l = 0; l < n.length; ++l) "" === n[l] && (n[l] = " ");
                    var d = t.css.fontWeight || "400", x = t.css.textStyle || "normal";
                    t.css.fontSize || (t.css.fontSize = "20rpx"), this.ctx.font = "".concat(x, " ").concat(d, " ").concat(t.css.fontSize.toPx(), 'px "').concat(t.css.fontFamily || "sans-serif", '"');
                    for (var u = 0, f = [], g = 0; g < n.length; ++g) {
                        var v = this.ctx.measureText(n[g]).width, b = t.css.fontSize.toPx() + c[1] + c[3], y = t.css.width ? t.css.width.toPx(!1, this.style.width) - c[1] - c[3] : v;
                        y < b && (y = b);
                        var p = Math.ceil(v / y);
                        h = y > h ? y : h, u += p, f[g] = p;
                    }
                    u = t.css.maxLines < u ? t.css.maxLines : u;
                    var w = t.css.lineHeight ? t.css.lineHeight.toPx() : t.css.fontSize.toPx();
                    s = w * u, i = {
                        lines: u,
                        lineHeight: w,
                        textArray: n,
                        linesArray: f
                    };
                    break;

                  case "image":
                    var m = getApp().systemInfo.pixelRatio ? getApp().systemInfo.pixelRatio : 2;
                    t.css && (t.css.width || (t.css.width = "auto"), t.css.height || (t.css.height = "auto")), 
                    !t.css || "auto" === t.css.width && "auto" === t.css.height ? (h = Math.round(t.sWidth / m), 
                    s = Math.round(t.sHeight / m)) : "auto" === t.css.width ? (s = t.css.height.toPx(!1, this.style.height), 
                    h = t.sWidth / t.sHeight * s) : "auto" === t.css.height ? (h = t.css.width.toPx(!1, this.style.width), 
                    s = t.sHeight / t.sWidth * h) : (h = t.css.width.toPx(!1, this.style.width), s = t.css.height.toPx(!1, this.style.height));
                    break;

                  default:
                    if (!t.css.width || !t.css.height) return void console.error("You should set width and height");
                    h = t.css.width.toPx(!1, this.style.width), s = t.css.height.toPx(!1, this.style.height);
                }
                if (t.css && t.css.right) if ("string" == typeof t.css.right) r = this.style.width - t.css.right.toPx(!0, this.style.width); else {
                    var P = t.css.right;
                    r = this.style.width - P[0].toPx(!0, this.style.width) - a.viewRect[P[1]].width * (P[2] || 1);
                } else if (t.css && t.css.left) if ("string" == typeof t.css.left) r = t.css.left.toPx(!0, this.style.width); else {
                    var k = t.css.left;
                    r = k[0].toPx(!0, this.style.width) + a.viewRect[k[1]].width * (k[2] || 1);
                } else r = 0;
                if (t.css && t.css.bottom) o = this.style.height - s - t.css.bottom.toPx(!0, this.style.height); else if (t.css && t.css.top) if ("string" == typeof t.css.top) o = t.css.top.toPx(!0, this.style.height); else {
                    var _ = t.css.top;
                    o = _[0].toPx(!0, this.style.height) + a.viewRect[_[1]].height * (_[2] || 1);
                } else o = 0;
                var S = t.css && t.css.rotate ? this._getAngle(t.css.rotate) : 0, A = t.css && t.css.align ? t.css.align : t.css && t.css.right ? "right" : "left", T = t.css && t.css.verticalAlign ? t.css.verticalAlign : "top", R = 0;
                switch (A) {
                  case "center":
                    R = r;
                    break;

                  case "right":
                    R = r - h / 2;
                    break;

                  default:
                    R = r + h / 2;
                }
                var C = 0;
                switch (T) {
                  case "center":
                    C = o;
                    break;

                  case "bottom":
                    C = o - s / 2;
                    break;

                  default:
                    C = o + s / 2;
                }
                this.ctx.translate(R, C);
                var I = r;
                "center" === A ? I = r - h / 2 : "right" === A && (I = r - h);
                var W = o;
                return "center" === T ? W = o - s / 2 : "bottom" === T && (W = o - s), t.rect ? (t.rect.left = I, 
                t.rect.top = W, t.rect.right = I + h, t.rect.bottom = W + s, t.rect.x = t.css && t.css.right ? r - h : r, 
                t.rect.y = o) : t.rect = {
                    left: I,
                    top: W,
                    right: I + h,
                    bottom: W + s,
                    x: t.css && t.css.right ? r - h : r,
                    y: o
                }, t.rect.left = t.rect.left - c[3], t.rect.top = t.rect.top - c[0], t.rect.right = t.rect.right + c[1], 
                t.rect.bottom = t.rect.bottom + c[2], "text" === t.type && (t.rect.minWidth = t.css.fontSize.toPx() + c[1] + c[3]), 
                this.ctx.rotate(S), !e && t.css && t.css.borderRadius && "rect" !== t.type && this._doClip(t.css.borderRadius, h, s, t.css.borderStyle), 
                this._doShadow(t), t.id && (a.viewRect[t.id] = {
                    width: h,
                    height: s,
                    left: t.rect.left,
                    top: t.rect.top,
                    right: t.rect.right,
                    bottom: t.rect.bottom
                }), {
                    width: h,
                    height: s,
                    x: r,
                    y: o,
                    extra: i
                };
            }
        }, {
            key: "_doPaddings",
            value: function(t) {
                var e = (t.css ? t.css : {}).padding, s = [ 0, 0, 0, 0 ];
                if (e) {
                    var i = e.split(/\s+/);
                    if (1 === i.length) {
                        var r = i[0].toPx();
                        s = [ r, r, r, r ];
                    }
                    if (2 === i.length) {
                        var o = i[0].toPx(), h = i[1].toPx();
                        s = [ o, h, o, h ];
                    }
                    if (3 === i.length) {
                        var c = i[0].toPx(), a = i[1].toPx();
                        s = [ c, a, i[2].toPx(), a ];
                    }
                    4 === i.length && (s = [ i[0].toPx(), i[1].toPx(), i[2].toPx(), i[3].toPx() ]);
                }
                return s;
            }
        }, {
            key: "_doBackground",
            value: function(t) {
                this.ctx.save();
                var e = this._preProcess(t, !0), s = e.width, i = e.height, r = t.css.background, o = this._doPaddings(t), h = s + o[1] + o[3], a = i + o[0] + o[2];
                this._doClip(t.css.borderRadius, h, a, t.css.borderStyle), c.api.isGradient(r) ? c.api.doGradient(r, h, a, this.ctx) : this.ctx.fillStyle = r, 
                this.ctx.fillRect(-h / 2, -a / 2, h, a), this.ctx.restore();
            }
        }, {
            key: "_drawQRCode",
            value: function(t) {
                this.ctx.save();
                var e = this._preProcess(t), s = e.width, i = e.height;
                h.api.draw(t.content, this.ctx, -s / 2, -i / 2, s, i, t.css.background, t.css.color), 
                this.ctx.restore(), this._doBorder(t, s, i);
            }
        }, {
            key: "_drawAbsImage",
            value: function(t) {
                if (t.url) {
                    this.ctx.save();
                    var e = this._preProcess(t), s = e.width, i = e.height, r = t.sWidth, o = t.sHeight, h = 0, c = 0, a = s / i;
                    a >= t.sWidth / t.sHeight ? (o = r / a, c = Math.round((t.sHeight - o) / 2)) : (r = o * a, 
                    h = Math.round((t.sWidth - r) / 2)), t.css && "scaleToFill" === t.css.mode ? this.ctx.drawImage(t.url, -s / 2, -i / 2, s, i) : (this.ctx.drawImage(t.url, h, c, r, o, -s / 2, -i / 2, s, i), 
                    t.rect.startX = h / t.sWidth, t.rect.startY = c / t.sHeight, t.rect.endX = (h + r) / t.sWidth, 
                    t.rect.endY = (c + o) / t.sHeight), this.ctx.restore(), this._doBorder(t, s, i);
                }
            }
        }, {
            key: "_fillAbsText",
            value: function(t) {
                if (t.text) {
                    t.css.background && this._doBackground(t), this.ctx.save();
                    var s = this._preProcess(t, t.css.background && t.css.borderRadius), i = s.width, o = s.height, h = s.extra;
                    if (this.ctx.fillStyle = t.css.color || "black", t.id && a.textLines[t.id]) {
                        this.ctx.textAlign = t.css.textAlign ? t.css.textAlign : "left";
                        var c, n = r(a.textLines[t.id]);
                        try {
                            for (n.s(); !(c = n.n()).done; ) {
                                var l = c.value, d = l.measuredWith, x = l.text, u = l.x, f = l.y, g = l.textDecoration;
                                if ("stroke" === t.css.textStyle ? this.ctx.strokeText(x, u, f, d) : this.ctx.fillText(x, u, f, d), 
                                g) {
                                    var v, b, y = t.css.fontSize.toPx();
                                    this.ctx.lineWidth = y / 13, this.ctx.beginPath(), (v = this.ctx).moveTo.apply(v, (0, 
                                    e.default)(g.moveTo)), (b = this.ctx).lineTo.apply(b, (0, e.default)(g.lineTo)), 
                                    this.ctx.closePath(), this.ctx.strokeStyle = t.css.color, this.ctx.stroke();
                                }
                            }
                        } catch (t) {
                            n.e(t);
                        } finally {
                            n.f();
                        }
                    } else {
                        var p = h.lines, w = h.lineHeight, m = h.textArray, P = h.linesArray;
                        if (t.id) {
                            for (var k = 0, _ = 0; _ < m.length; ++_) {
                                var S = this.ctx.measureText(m[_]).width;
                                k = S > k ? S : k;
                            }
                            a.viewRect[t.id].width = i ? k < i ? k : i : k;
                        }
                        for (var A = 0, T = 0; T < m.length; ++T) for (var R = Math.ceil(m[T].length / P[T]), C = 0, I = 0, W = 0; W < P[T] && !(A >= p); ++W) {
                            I = R;
                            for (var M = m[T].substr(C, I), L = this.ctx.measureText(M).width; C + I <= m[T].length && (i - L > t.css.fontSize.toPx() || L - i > t.css.fontSize.toPx()); ) {
                                if (L < i) M = m[T].substr(C, ++I); else {
                                    if (M.length <= 1) break;
                                    M = m[T].substr(C, --I);
                                }
                                L = this.ctx.measureText(M).width;
                            }
                            if (C += M.length, A === p - 1 && (T < m.length - 1 || C < m[T].length)) {
                                for (;this.ctx.measureText("".concat(M, "...")).width > i && !(M.length <= 1); ) M = M.substring(0, M.length - 1);
                                M += "...", L = this.ctx.measureText(M).width;
                            }
                            this.ctx.textAlign = t.css.textAlign ? t.css.textAlign : "left";
                            var j = void 0, z = void 0;
                            switch (t.css.textAlign) {
                              case "center":
                                z = (j = 0) - L / 2;
                                break;

                              case "right":
                                z = (j = i / 2) - L;
                                break;

                              default:
                                z = j = -i / 2;
                            }
                            var B = -o / 2 + (0 === A ? t.css.fontSize.toPx() : t.css.fontSize.toPx() + A * w);
                            A++, "stroke" === t.css.textStyle ? this.ctx.strokeText(M, j, B, L) : this.ctx.fillText(M, j, B, L);
                            var D = t.css.fontSize.toPx(), H = void 0;
                            t.css.textDecoration && (this.ctx.lineWidth = D / 13, this.ctx.beginPath(), /\bunderline\b/.test(t.css.textDecoration) && (this.ctx.moveTo(z, B), 
                            this.ctx.lineTo(z + L, B), H = {
                                moveTo: [ z, B ],
                                lineTo: [ z + L, B ]
                            }), /\boverline\b/.test(t.css.textDecoration) && (this.ctx.moveTo(z, B - D), this.ctx.lineTo(z + L, B - D), 
                            H = {
                                moveTo: [ z, B - D ],
                                lineTo: [ z + L, B - D ]
                            }), /\bline-through\b/.test(t.css.textDecoration) && (this.ctx.moveTo(z, B - D / 3), 
                            this.ctx.lineTo(z + L, B - D / 3), H = {
                                moveTo: [ z, B - D / 3 ],
                                lineTo: [ z + L, B - D / 3 ]
                            }), this.ctx.closePath(), this.ctx.strokeStyle = t.css.color, this.ctx.stroke()), 
                            t.id && (a.textLines[t.id] ? a.textLines[t.id].push({
                                text: M,
                                x: j,
                                y: B,
                                measuredWith: L,
                                textDecoration: H
                            }) : a.textLines[t.id] = [ {
                                text: M,
                                x: j,
                                y: B,
                                measuredWith: L,
                                textDecoration: H
                            } ]);
                        }
                    }
                    this.ctx.restore(), this._doBorder(t, i, o);
                }
            }
        }, {
            key: "_drawAbsRect",
            value: function(t) {
                this.ctx.save();
                var e = this._preProcess(t), s = e.width, i = e.height;
                c.api.isGradient(t.css.color) ? c.api.doGradient(t.css.color, s, i, this.ctx) : this.ctx.fillStyle = t.css.color;
                var r = t.css, o = r.borderRadius, h = r.borderStyle, a = r.borderWidth;
                this._border({
                    borderRadius: o,
                    width: s,
                    height: i,
                    borderWidth: a,
                    borderStyle: h
                }), this.ctx.fill(), this.ctx.restore(), this._doBorder(t, s, i);
            }
        }, {
            key: "_doShadow",
            value: function(t) {
                if (t.css && t.css.shadow) {
                    var e = t.css.shadow.replace(/,\s+/g, ",").split(/\s+/);
                    e.length > 4 ? console.error("shadow don't spread option") : (this.ctx.shadowOffsetX = parseInt(e[0], 10), 
                    this.ctx.shadowOffsetY = parseInt(e[1], 10), this.ctx.shadowBlur = parseInt(e[2], 10), 
                    this.ctx.shadowColor = e[3]);
                }
            }
        }, {
            key: "_getAngle",
            value: function(t) {
                return Number(t) * Math.PI / 180;
            }
        } ]), t;
    }();
    exports.default = n;
}();