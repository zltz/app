!function() {
    function r(r, n, t, u) {
        return r < 20 ? n & t | ~n & u : r < 40 ? n ^ t ^ u : r < 60 ? n & t | n & u | t & u : n ^ t ^ u;
    }
    function n(r) {
        return r < 20 ? 1518500249 : r < 40 ? 1859775393 : r < 60 ? -1894007588 : -899497514;
    }
    function t(r, n) {
        var t = (65535 & r) + (65535 & n);
        return (r >> 16) + (n >> 16) + (t >> 16) << 16 | 65535 & t;
    }
    function u(r, n) {
        return r << n | r >>> 32 - n;
    }
    module.exports = {
        hex_sha1: function(e) {
            return function(r) {
                for (var n = "0123456789abcdef", t = "", u = 0; u < 4 * r.length; u++) t += n.charAt(r[u >> 2] >> 8 * (3 - u % 4) + 4 & 15) + n.charAt(r[u >> 2] >> 8 * (3 - u % 4) & 15);
                return t;
            }(function(e, o) {
                e[o >> 5] |= 128 << 24 - o % 32, e[15 + (o + 64 >> 9 << 4)] = o;
                for (var a = Array(80), f = 1732584193, c = -271733879, h = -1732584194, i = 271733878, v = -1009589776, A = 0; A < e.length; A += 16) {
                    for (var l = f, g = c, d = h, y = i, s = v, x = 0; x < 80; x++) {
                        a[x] = x < 16 ? e[A + x] : u(a[x - 3] ^ a[x - 8] ^ a[x - 14] ^ a[x - 16], 1);
                        var b = t(t(u(f, 5), r(x, c, h, i)), t(t(v, a[x]), n(x)));
                        v = i, i = h, h = u(c, 30), c = f, f = b;
                    }
                    f = t(f, l), c = t(c, g), h = t(h, d), i = t(i, y), v = t(v, s);
                }
                return Array(f, c, h, i, v);
            }(function(r) {
                for (var n = Array(), t = 0; t < 8 * r.length; t += 8) n[t >> 5] |= (255 & r.charCodeAt(t / 8)) << 24 - t % 32;
                return n;
            }(e), 8 * e.length));
        }
    };
}();