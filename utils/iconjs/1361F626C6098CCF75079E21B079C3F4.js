!function() {
    var t = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js");
    Object.defineProperty(exports, "__esModule", {
        value: !0
    }), exports.default = void 0;
    var e = t(require("CBDBE653C6098CCFADBD8E5437B9C3F4.js"));
    exports.default = function t(i, a) {
        var n = this, o = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 700, l = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : 700;
        (0, e.default)(this, t), this.switchRect = function(t) {
            var e = t.x, i = t.y, a = t.w, o = t.h, l = t.sw, s = void 0 === l ? 320 : l, r = t.sh, d = void 0 === r ? 320 : r, h = n._size;
            return {
                x: (e - (wx.getSystemInfoSync().screenWidth - 320) / 2) * (h.w / s),
                y: i * (h.h / d),
                w: a * (h.w / s),
                h: o * (h.h / d)
            };
        }, this.addImage = function(t) {
            var e = t.path, i = t.x, a = void 0 === i ? 0 : i, o = t.y, l = void 0 === o ? 0 : o, s = t.w, r = void 0 === s ? 0 : s, d = t.h, h = void 0 === d ? 0 : d, c = t.deg, v = void 0 === c ? 0 : c, u = t.radius, f = void 0 === u ? 0 : u, x = n._ctx, w = void 0 === x ? null : x;
            if (null != w) {
                if (w.save(), w.beginPath(), f > 0) {
                    var T = Math.min(r / 2, h / 2);
                    f = Math.min(f, T), w.setLineWidth(1), w.setStrokeStyle("transparent"), w.arc(a + f, l + f, f, Math.PI, 1.5 * Math.PI), 
                    w.moveTo(a + f, l), w.lineTo(a + r - f, l), w.lineTo(a + r, l + f), w.arc(a + r - f, l + f, f, 1.5 * Math.PI, 2 * Math.PI), 
                    w.lineTo(a + r, l + h - f), w.lineTo(a + r - f, l + h), w.arc(a + r - f, l + h - f, f, 0, .5 * Math.PI), 
                    w.lineTo(a + f, l + h), w.lineTo(a, l + h - f), w.arc(a + f, l + h - f, f, .5 * Math.PI, Math.PI), 
                    w.lineTo(a, l + f), w.lineTo(a + f, l), w.stroke(), w.clip();
                }
                0 != v ? (w.translate(a + r / 2, l + h / 2), w.rotate(v * Math.PI / 180), w.drawImage(e, -r / 2, -h / 2, r, h)) : w.drawImage(e, a, l, r, h), 
                w.closePath(), w.restore();
            }
            return n;
        }, this.addText = function(t) {
            var e = t.text, i = t.x, a = void 0 === i ? 0 : i, o = t.y, l = void 0 === o ? 0 : o, s = t.fontSize, r = void 0 === s ? 14 : s, d = t.color, h = void 0 === d ? "#444" : d, c = t.maxWidth, v = void 0 === c ? 0 : c, u = t.align, f = void 0 === u ? "left" : u, x = t.font, w = void 0 === x ? "" : x, T = t.textBaseline, g = void 0 === T ? "top" : T, m = n._ctx, P = void 0 === m ? null : m, p = n._size;
            return null != P && null != e && (P.save(), P.beginPath(), r > 0 && P.setFontSize(r), 
            "" != w && (P.font = w), 0 == v && (v = P.measureText(e).width), "center" == f && (a = (p.w - v) / 2), 
            P.setTextBaseline(g), P.setFillStyle(h), P.fillText(e, a, l, v), P.setTextAlign(f), 
            P.closePath(), P.restore()), n;
        }, this.startCompound = function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null, e = n._ctx, i = n._canvasid;
            e.draw(), setTimeout(function() {
                wx.canvasToTempFilePath({
                    canvasId: i,
                    success: function(e) {
                        t && t(e.tempFilePath);
                    },
                    fail: function(e) {
                        console.log(e), t && t(null);
                    }
                });
            }, 100);
        }, this.startCut = function(t) {
            var e = t.x, i = void 0 === e ? 0 : e, a = t.y, o = void 0 === a ? 0 : a, l = t.w, s = void 0 === l ? 0 : l, r = t.h, d = void 0 === r ? 0 : r, h = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null, c = n._ctx, v = n._canvasid;
            n._size, c.draw(), setTimeout(function() {
                wx.canvasToTempFilePath({
                    canvasId: v,
                    x: i,
                    y: o,
                    width: s,
                    height: d,
                    destWidth: s,
                    destHeight: d,
                    success: function(t) {
                        h && h(t.tempFilePath);
                    },
                    fail: function(t) {
                        console.log(t), h && h(null);
                    }
                });
            }, 100);
        }, this._canvasid = a, this._size = {
            w: o,
            h: l
        };
        var s = wx.createCanvasContext(a, i);
        s.clearRect(0, 0, o, l), s.save(), s.beginPath(), s.rect(0, 0, o, l), s.setFillStyle("#fff"), 
        s.fill(), s.restore(), this._ctx = s;
    };
}();