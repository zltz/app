module.exports = function(r, t) {
    var n = [], e = !0, l = !1, o = void 0;
    try {
        for (var u, a = r[Symbol.iterator](); !(e = (u = a.next()).done) && (n.push(u.value), 
        !t || n.length !== t); e = !0) ;
    } catch (r) {
        l = !0, o = r;
    } finally {
        try {
            e || null == a.return || a.return();
        } finally {
            if (l) throw o;
        }
    }
    return n;
};