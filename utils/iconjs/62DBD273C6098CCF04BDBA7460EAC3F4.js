!function() {
    var e;
    global.__wcc_version__ = "v0.5vv_20200413_syb_scopedata", global.__wcc_version_info__ = {
        customComponents: !0,
        fixZeroRpx: !0,
        propValueDeepCopy: !1
    };
    $gwx0 = function(t, o) {
        function n(e, t) {
            void 0 !== t && e.children.push(t);
        }
        function a(e) {
            return void 0 !== e ? {
                tag: "virtual",
                wxKey: e,
                children: []
            } : {
                tag: "virtual",
                children: []
            };
        }
        function r(t) {
            if (++e >= 16e3) throw "Dom limit exceeded, please check if there's any mistake you've made.";
            return {
                tag: "wx-" + t,
                attr: {},
                children: [],
                n: [],
                raw: {},
                generics: {}
            };
        }
        function i(e) {
            for (var t = e.split("\n    "), o = 0; o < t.length; ++o) 0 != o && (")" === t[o][t[o].length - 1] ? t[o] = t[o].replace(/\s\(.*\)$/, "") : t[o] = "at anonymous function");
            return t.join("\n    ");
        }
        function s(e) {
            function t(o, n, a, r, s, c) {
                var u = o[0];
                if (void 0 !== c && (s.ap = c), "object" == typeof u) {
                    var l, d, h, g, _, f, w, m, v;
                    switch (u[0]) {
                      case 2:
                        return function(o, n, a, r, i) {
                            var s, p, c, u, l = o[0][1];
                            switch (l) {
                              case "?:":
                                return s = t(o[1], n, a, r, i, !1), c = e && "h" === wh.hn(s), u = wh.rv(s) ? t(o[2], n, a, r, i, !1) : t(o[3], n, a, r, i, !1), 
                                u = c && "n" === wh.hn(u) ? wh.nh(u, "c") : u;

                              case "&&":
                                return s = t(o[1], n, a, r, i, !1), c = e && "h" === wh.hn(s), u = wh.rv(s) ? t(o[2], n, a, r, i, !1) : wh.rv(s), 
                                u = c && "n" === wh.hn(u) ? wh.nh(u, "c") : u;

                              case "||":
                                return s = t(o[1], n, a, r, i, !1), c = e && "h" === wh.hn(s), u = wh.rv(s) ? wh.rv(s) : t(o[2], n, a, r, i, !1), 
                                u = c && "n" === wh.hn(u) ? wh.nh(u, "c") : u;

                              case "+":
                              case "*":
                              case "/":
                              case "%":
                              case "|":
                              case "^":
                              case "&":
                              case "===":
                              case "==":
                              case "!=":
                              case "!==":
                              case ">=":
                              case "<=":
                              case ">":
                              case "<":
                              case "<<":
                              case ">>":
                                switch (s = t(o[1], n, a, r, i, !1), p = t(o[2], n, a, r, i, !1), c = e && ("h" === wh.hn(s) || "h" === wh.hn(p)), 
                                l) {
                                  case "+":
                                    u = wh.rv(s) + wh.rv(p);
                                    break;

                                  case "*":
                                    u = wh.rv(s) * wh.rv(p);
                                    break;

                                  case "/":
                                    u = wh.rv(s) / wh.rv(p);
                                    break;

                                  case "%":
                                    u = wh.rv(s) % wh.rv(p);
                                    break;

                                  case "|":
                                    u = wh.rv(s) | wh.rv(p);
                                    break;

                                  case "^":
                                    u = wh.rv(s) ^ wh.rv(p);
                                    break;

                                  case "&":
                                    u = wh.rv(s) & wh.rv(p);
                                    break;

                                  case "===":
                                    u = wh.rv(s) === wh.rv(p);
                                    break;

                                  case "==":
                                    u = wh.rv(s) == wh.rv(p);
                                    break;

                                  case "!=":
                                    u = wh.rv(s) != wh.rv(p);
                                    break;

                                  case "!==":
                                    u = wh.rv(s) !== wh.rv(p);
                                    break;

                                  case ">=":
                                    u = wh.rv(s) >= wh.rv(p);
                                    break;

                                  case "<=":
                                    u = wh.rv(s) <= wh.rv(p);
                                    break;

                                  case ">":
                                    u = wh.rv(s) > wh.rv(p);
                                    break;

                                  case "<":
                                    u = wh.rv(s) < wh.rv(p);
                                    break;

                                  case "<<":
                                    u = wh.rv(s) << wh.rv(p);
                                    break;

                                  case ">>":
                                    u = wh.rv(s) >> wh.rv(p);
                                }
                                return c ? wh.nh(u, "c") : u;

                              case "-":
                                return s = 3 === o.length ? t(o[1], n, a, r, i, !1) : 0, p = 3 === o.length ? t(o[2], n, a, r, i, !1) : t(o[1], n, a, r, i, !1), 
                                u = (c = e && ("h" === wh.hn(s) || "h" === wh.hn(p))) ? wh.rv(s) - wh.rv(p) : s - p, 
                                c ? wh.nh(u, "c") : u;

                              case "!":
                                return s = t(o[1], n, a, r, i, !1), c = e && "h" == wh.hn(s), u = !wh.rv(s), c ? wh.nh(u, "c") : u;

                              case "~":
                                return s = t(o[1], n, a, r, i, !1), c = e && "h" == wh.hn(s), u = ~wh.rv(s), c ? wh.nh(u, "c") : u;

                              default:
                                $gwn("unrecognized op" + l);
                            }
                        }(o, n, a, r, s);

                      case 4:
                        return t(o[1], n, a, r, s, !1);

                      case 5:
                        switch (o.length) {
                          case 2:
                            return k = t(o[1], n, a, r, s, !1), e ? [ k ] : [ wh.rv(k) ];

                          case 1:
                            return [];

                          default:
                            return k = t(o[1], n, a, r, s, !1), d = t(o[2], n, a, r, s, !1), k.push(e ? d : wh.rv(d)), 
                            k;
                        }
                        break;

                      case 6:
                        k = t(o[1], n, a, r, s);
                        var x = s.ap;
                        if (w = "h" === wh.hn(k), l = w ? wh.rv(k) : k, s.is_affected |= w, e) return null == l ? w ? wh.nh(void 0, "e") : void 0 : (d = t(o[2], n, a, r, s, !1), 
                        m = "h" === wh.hn(d), h = m ? wh.rv(d) : d, s.ap = x, s.is_affected |= m, null == h || "__proto__" === h || "prototype" === h || "caller" === h ? w || m ? wh.nh(void 0, "e") : void 0 : ("function" != typeof (g = l[h]) || x || (g = void 0), 
                        v = "h" === wh.hn(g), s.is_affected |= v, w || m ? v ? g : wh.nh(g, "e") : g));
                        if (null == l) return;
                        if (d = t(o[2], n, a, r, s, !1), m = "h" === wh.hn(d), h = m ? wh.rv(d) : d, s.ap = x, 
                        s.is_affected |= m, null == h || "__proto__" === h || "prototype" === h || "caller" === h) return;
                        return "function" != typeof (g = l[h]) || x || (g = void 0), v = "h" === wh.hn(g), 
                        s.is_affected |= v, v ? wh.rv(g) : g;

                      case 7:
                        switch (o[1][0]) {
                          case 11:
                            return s.is_affected |= "h" === wh.hn(r), r;

                          case 3:
                            if (_ = wh.rv(a), f = wh.rv(n), d = o[1][1], r && r.f && r.f.hasOwnProperty(d) ? (k = r.f, 
                            s.ap = !0) : k = _ && _.hasOwnProperty(d) ? a : f && f.hasOwnProperty(d) ? n : void 0, 
                            e) {
                                if (k) return w = "h" === wh.hn(k), g = (l = w ? wh.rv(k) : k)[d], v = "h" === wh.hn(g), 
                                s.is_affected |= w || v, g = w && !v ? wh.nh(g, "e") : g;
                            } else if (k) return w = "h" === wh.hn(k), g = (l = w ? wh.rv(k) : k)[d], v = "h" === wh.hn(g), 
                            s.is_affected |= w || v, wh.rv(g);
                            return;
                        }
                        break;

                      case 8:
                        return (k = {})[o[1]] = t(o[2], n, a, r, s, !1), k;

                      case 9:
                        function y(t, o, n) {
                            for (var a in w = "h" === wh.hn(t), m = "h" === wh.hn(o), l = wh.rv(t), h = wh.rv(o)) !n && l.hasOwnProperty(a) || (l[a] = e ? m ? wh.nh(h[a], "e") : h[a] : wh.rv(h[a]));
                            return t;
                        }
                        k = t(o[1], n, a, r, s, !1), d = t(o[2], n, a, r, s, !1);
                        var A = k, O = !0;
                        return "object" == typeof o[1][0] && 10 === o[1][0][0] && (k = d, d = A, O = !1), 
                        "object" == typeof o[1][0] && 10 === o[1][0][0] ? y(y(P = {}, k, O), d, O) : y(k, d, O);

                      case 10:
                        return k = t(o[1], n, a, r, s, !1), k = e ? k : wh.rv(k);

                      case 12:
                        var P;
                        if (k = t(o[1], n, a, r, s), !s.ap) return e && "h" === wh.hn(k) ? wh.nh(P, "f") : P;
                        x = s.ap;
                        d = t(o[2], n, a, r, s, !1), s.ap = x, w = "h" === wh.hn(k), m = p(d), l = wh.rv(k), 
                        h = wh.rv(d), snap_bb = b(h, "nv_");
                        try {
                            P = "function" == typeof l ? b(l.apply(null, snap_bb)) : void 0;
                        } catch (n) {
                            n.message = n.message.replace(/nv_/g, ""), n.stack = n.stack.substring(0, n.stack.indexOf("\n", n.stack.lastIndexOf("at nv_"))), 
                            n.stack = n.stack.replace(/\snv_/g, " "), n.stack = i(n.stack), r.debugInfo && (n.stack += "\n    at " + r.debugInfo[0] + ":" + r.debugInfo[1] + ":" + r.debugInfo[2], 
                            console.error(n)), P = void 0;
                        }
                        return e && (m || w) ? wh.nh(P, "f") : P;
                    }
                } else {
                    if (3 === u || 1 === u) return o[1];
                    if (11 === u) {
                        for (var k = "", L = 1; L < o.length; L++) {
                            var j = wh.rv(t(o[L], n, a, r, s, !1));
                            k += void 0 === j ? "" : j;
                        }
                        return k;
                    }
                }
            }
            return function(e, o, n, a, r, i) {
                return "11182016" == e[0] ? (a.debugInfo = e[2], t(e[1], o, n, a, r, i)) : (a.debugInfo = null, 
                t(e, o, n, a, r, i));
            };
        }
        function p(e) {
            if ("h" == wh.hn(e)) return !0;
            if ("object" != typeof e) return !1;
            for (var t in e) if (e.hasOwnProperty(t) && p(e[t])) return !0;
            return !1;
        }
        function c(e, t, o, n, a) {
            var r = b(n, "", 2);
            a.ap && r && r.constructor === Function && (t = "$wxs:" + t, e.attr.$gdc = b), (a.is_affected || p(n)) && (e.n.push(t), 
            e.raw[t] = n), e.attr[t] = r;
        }
        function u(e, t, o, n, a, r, i) {
            i.opindex = n;
            var s = {};
            c(t, o, 0, grb(e[n], a, r, i, s), s);
        }
        function l(e, t, o, n, a) {
            a.opindex = t;
            var r = grb(e[t], o, n, a, {});
            return r && r.constructor === Function ? void 0 : r;
        }
        void 0 === o && (o = {}), "undefined" == typeof __WXML_GLOBAL__ && (__WXML_GLOBAL__ = {}), 
        __WXML_GLOBAL__.modules = __WXML_GLOBAL__.modules || {}, $gwx("init", o), $gwn = console.warn, 
        $gwl = console.log, wh = function() {
            function e() {}
            return e.prototype = {
                hn: function(e, t) {
                    if ("object" == typeof e) {
                        var o = 0, n = !1, a = !1;
                        for (var r in e) if (n |= "__value__" === r, a |= "__wxspec__" === r, ++o > 2) break;
                        return 2 == o && n && a && (t || "m" !== e.__wxspec__ || "h" === this.hn(e.__value__)) ? "h" : "n";
                    }
                    return "n";
                },
                nh: function(e, t) {
                    return {
                        __value__: e,
                        __wxspec__: t || !0
                    };
                },
                rv: function(e) {
                    return "n" === this.hn(e, !0) ? e : this.rv(e.__value__);
                },
                hm: function(e) {
                    if ("object" == typeof e) {
                        var t = 0, o = !1, n = !1;
                        for (var a in e) if (o |= "__value__" === a, n |= "__wxspec__" === a, ++t > 2) break;
                        return 2 == t && o && n && ("m" === e.__wxspec__ || this.hm(e.__value__));
                    }
                    return !1;
                }
            }, new e();
        }(), gra = s(!0), grb = s(!1);
        var d = function() {
            Object.defineProperty(Object.prototype, "nv_constructor", {
                writable: !0,
                value: "Object"
            }), Object.defineProperty(Object.prototype, "nv_toString", {
                writable: !0,
                value: function() {
                    return "[object Object]";
                }
            });
        }, h = function() {
            Object.defineProperty(Function.prototype, "nv_constructor", {
                writable: !0,
                value: "Function"
            }), Object.defineProperty(Function.prototype, "nv_length", {
                get: function() {
                    return this.length;
                },
                set: function() {}
            }), Object.defineProperty(Function.prototype, "nv_toString", {
                writable: !0,
                value: function() {
                    return "[function Function]";
                }
            });
        }, g = function() {
            Object.defineProperty(Array.prototype, "nv_toString", {
                writable: !0,
                value: function() {
                    return this.nv_join();
                }
            }), Object.defineProperty(Array.prototype, "nv_join", {
                writable: !0,
                value: function(e) {
                    e = null == e ? "," : e;
                    for (var t = "", o = 0; o < this.length; ++o) 0 != o && (t += e), null == this[o] || null == this[o] ? t += "" : "function" == typeof this[o] ? t += this[o].nv_toString() : "object" == typeof this[o] && "Array" === this[o].nv_constructor ? t += this[o].nv_join() : t += this[o].toString();
                    return t;
                }
            }), Object.defineProperty(Array.prototype, "nv_constructor", {
                writable: !0,
                value: "Array"
            }), Object.defineProperty(Array.prototype, "nv_concat", {
                writable: !0,
                value: Array.prototype.concat
            }), Object.defineProperty(Array.prototype, "nv_pop", {
                writable: !0,
                value: Array.prototype.pop
            }), Object.defineProperty(Array.prototype, "nv_push", {
                writable: !0,
                value: Array.prototype.push
            }), Object.defineProperty(Array.prototype, "nv_reverse", {
                writable: !0,
                value: Array.prototype.reverse
            }), Object.defineProperty(Array.prototype, "nv_shift", {
                writable: !0,
                value: Array.prototype.shift
            }), Object.defineProperty(Array.prototype, "nv_slice", {
                writable: !0,
                value: Array.prototype.slice
            }), Object.defineProperty(Array.prototype, "nv_sort", {
                writable: !0,
                value: Array.prototype.sort
            }), Object.defineProperty(Array.prototype, "nv_splice", {
                writable: !0,
                value: Array.prototype.splice
            }), Object.defineProperty(Array.prototype, "nv_unshift", {
                writable: !0,
                value: Array.prototype.unshift
            }), Object.defineProperty(Array.prototype, "nv_indexOf", {
                writable: !0,
                value: Array.prototype.indexOf
            }), Object.defineProperty(Array.prototype, "nv_lastIndexOf", {
                writable: !0,
                value: Array.prototype.lastIndexOf
            }), Object.defineProperty(Array.prototype, "nv_every", {
                writable: !0,
                value: Array.prototype.every
            }), Object.defineProperty(Array.prototype, "nv_some", {
                writable: !0,
                value: Array.prototype.some
            }), Object.defineProperty(Array.prototype, "nv_forEach", {
                writable: !0,
                value: Array.prototype.forEach
            }), Object.defineProperty(Array.prototype, "nv_map", {
                writable: !0,
                value: Array.prototype.map
            }), Object.defineProperty(Array.prototype, "nv_filter", {
                writable: !0,
                value: Array.prototype.filter
            }), Object.defineProperty(Array.prototype, "nv_reduce", {
                writable: !0,
                value: Array.prototype.reduce
            }), Object.defineProperty(Array.prototype, "nv_reduceRight", {
                writable: !0,
                value: Array.prototype.reduceRight
            }), Object.defineProperty(Array.prototype, "nv_length", {
                get: function() {
                    return this.length;
                },
                set: function(e) {
                    this.length = e;
                }
            });
        }, _ = function() {
            Object.defineProperty(String.prototype, "nv_constructor", {
                writable: !0,
                value: "String"
            }), Object.defineProperty(String.prototype, "nv_toString", {
                writable: !0,
                value: String.prototype.toString
            }), Object.defineProperty(String.prototype, "nv_valueOf", {
                writable: !0,
                value: String.prototype.valueOf
            }), Object.defineProperty(String.prototype, "nv_charAt", {
                writable: !0,
                value: String.prototype.charAt
            }), Object.defineProperty(String.prototype, "nv_charCodeAt", {
                writable: !0,
                value: String.prototype.charCodeAt
            }), Object.defineProperty(String.prototype, "nv_concat", {
                writable: !0,
                value: String.prototype.concat
            }), Object.defineProperty(String.prototype, "nv_indexOf", {
                writable: !0,
                value: String.prototype.indexOf
            }), Object.defineProperty(String.prototype, "nv_lastIndexOf", {
                writable: !0,
                value: String.prototype.lastIndexOf
            }), Object.defineProperty(String.prototype, "nv_localeCompare", {
                writable: !0,
                value: String.prototype.localeCompare
            }), Object.defineProperty(String.prototype, "nv_match", {
                writable: !0,
                value: String.prototype.match
            }), Object.defineProperty(String.prototype, "nv_replace", {
                writable: !0,
                value: String.prototype.replace
            }), Object.defineProperty(String.prototype, "nv_search", {
                writable: !0,
                value: String.prototype.search
            }), Object.defineProperty(String.prototype, "nv_slice", {
                writable: !0,
                value: String.prototype.slice
            }), Object.defineProperty(String.prototype, "nv_split", {
                writable: !0,
                value: String.prototype.split
            }), Object.defineProperty(String.prototype, "nv_substring", {
                writable: !0,
                value: String.prototype.substring
            }), Object.defineProperty(String.prototype, "nv_toLowerCase", {
                writable: !0,
                value: String.prototype.toLowerCase
            }), Object.defineProperty(String.prototype, "nv_toLocaleLowerCase", {
                writable: !0,
                value: String.prototype.toLocaleLowerCase
            }), Object.defineProperty(String.prototype, "nv_toUpperCase", {
                writable: !0,
                value: String.prototype.toUpperCase
            }), Object.defineProperty(String.prototype, "nv_toLocaleUpperCase", {
                writable: !0,
                value: String.prototype.toLocaleUpperCase
            }), Object.defineProperty(String.prototype, "nv_trim", {
                writable: !0,
                value: String.prototype.trim
            }), Object.defineProperty(String.prototype, "nv_length", {
                get: function() {
                    return this.length;
                },
                set: function(e) {
                    this.length = e;
                }
            });
        }, f = function() {
            Object.defineProperty(Boolean.prototype, "nv_constructor", {
                writable: !0,
                value: "Boolean"
            }), Object.defineProperty(Boolean.prototype, "nv_toString", {
                writable: !0,
                value: Boolean.prototype.toString
            }), Object.defineProperty(Boolean.prototype, "nv_valueOf", {
                writable: !0,
                value: Boolean.prototype.valueOf
            });
        }, w = function() {
            Object.defineProperty(Number, "nv_MAX_VALUE", {
                writable: !1,
                value: Number.MAX_VALUE
            }), Object.defineProperty(Number, "nv_MIN_VALUE", {
                writable: !1,
                value: Number.MIN_VALUE
            }), Object.defineProperty(Number, "nv_NEGATIVE_INFINITY", {
                writable: !1,
                value: Number.NEGATIVE_INFINITY
            }), Object.defineProperty(Number, "nv_POSITIVE_INFINITY", {
                writable: !1,
                value: Number.POSITIVE_INFINITY
            }), Object.defineProperty(Number.prototype, "nv_constructor", {
                writable: !0,
                value: "Number"
            }), Object.defineProperty(Number.prototype, "nv_toString", {
                writable: !0,
                value: Number.prototype.toString
            }), Object.defineProperty(Number.prototype, "nv_toLocaleString", {
                writable: !0,
                value: Number.prototype.toLocaleString
            }), Object.defineProperty(Number.prototype, "nv_valueOf", {
                writable: !0,
                value: Number.prototype.valueOf
            }), Object.defineProperty(Number.prototype, "nv_toFixed", {
                writable: !0,
                value: Number.prototype.toFixed
            }), Object.defineProperty(Number.prototype, "nv_toExponential", {
                writable: !0,
                value: Number.prototype.toExponential
            }), Object.defineProperty(Number.prototype, "nv_toPrecision", {
                writable: !0,
                value: Number.prototype.toPrecision
            });
        }, m = function() {
            Object.defineProperty(Math, "nv_E", {
                writable: !1,
                value: Math.E
            }), Object.defineProperty(Math, "nv_LN10", {
                writable: !1,
                value: Math.LN10
            }), Object.defineProperty(Math, "nv_LN2", {
                writable: !1,
                value: Math.LN2
            }), Object.defineProperty(Math, "nv_LOG2E", {
                writable: !1,
                value: Math.LOG2E
            }), Object.defineProperty(Math, "nv_LOG10E", {
                writable: !1,
                value: Math.LOG10E
            }), Object.defineProperty(Math, "nv_PI", {
                writable: !1,
                value: Math.PI
            }), Object.defineProperty(Math, "nv_SQRT1_2", {
                writable: !1,
                value: Math.SQRT1_2
            }), Object.defineProperty(Math, "nv_SQRT2", {
                writable: !1,
                value: Math.SQRT2
            }), Object.defineProperty(Math, "nv_abs", {
                writable: !1,
                value: Math.abs
            }), Object.defineProperty(Math, "nv_acos", {
                writable: !1,
                value: Math.acos
            }), Object.defineProperty(Math, "nv_asin", {
                writable: !1,
                value: Math.asin
            }), Object.defineProperty(Math, "nv_atan", {
                writable: !1,
                value: Math.atan
            }), Object.defineProperty(Math, "nv_atan2", {
                writable: !1,
                value: Math.atan2
            }), Object.defineProperty(Math, "nv_ceil", {
                writable: !1,
                value: Math.ceil
            }), Object.defineProperty(Math, "nv_cos", {
                writable: !1,
                value: Math.cos
            }), Object.defineProperty(Math, "nv_exp", {
                writable: !1,
                value: Math.exp
            }), Object.defineProperty(Math, "nv_floor", {
                writable: !1,
                value: Math.floor
            }), Object.defineProperty(Math, "nv_log", {
                writable: !1,
                value: Math.log
            }), Object.defineProperty(Math, "nv_max", {
                writable: !1,
                value: Math.max
            }), Object.defineProperty(Math, "nv_min", {
                writable: !1,
                value: Math.min
            }), Object.defineProperty(Math, "nv_pow", {
                writable: !1,
                value: Math.pow
            }), Object.defineProperty(Math, "nv_random", {
                writable: !1,
                value: Math.random
            }), Object.defineProperty(Math, "nv_round", {
                writable: !1,
                value: Math.round
            }), Object.defineProperty(Math, "nv_sin", {
                writable: !1,
                value: Math.sin
            }), Object.defineProperty(Math, "nv_sqrt", {
                writable: !1,
                value: Math.sqrt
            }), Object.defineProperty(Math, "nv_tan", {
                writable: !1,
                value: Math.tan
            });
        }, v = function() {
            Object.defineProperty(Date.prototype, "nv_constructor", {
                writable: !0,
                value: "Date"
            }), Object.defineProperty(Date, "nv_parse", {
                writable: !0,
                value: Date.parse
            }), Object.defineProperty(Date, "nv_UTC", {
                writable: !0,
                value: Date.UTC
            }), Object.defineProperty(Date, "nv_now", {
                writable: !0,
                value: Date.now
            }), Object.defineProperty(Date.prototype, "nv_toString", {
                writable: !0,
                value: Date.prototype.toString
            }), Object.defineProperty(Date.prototype, "nv_toDateString", {
                writable: !0,
                value: Date.prototype.toDateString
            }), Object.defineProperty(Date.prototype, "nv_toTimeString", {
                writable: !0,
                value: Date.prototype.toTimeString
            }), Object.defineProperty(Date.prototype, "nv_toLocaleString", {
                writable: !0,
                value: Date.prototype.toLocaleString
            }), Object.defineProperty(Date.prototype, "nv_toLocaleDateString", {
                writable: !0,
                value: Date.prototype.toLocaleDateString
            }), Object.defineProperty(Date.prototype, "nv_toLocaleTimeString", {
                writable: !0,
                value: Date.prototype.toLocaleTimeString
            }), Object.defineProperty(Date.prototype, "nv_valueOf", {
                writable: !0,
                value: Date.prototype.valueOf
            }), Object.defineProperty(Date.prototype, "nv_getTime", {
                writable: !0,
                value: Date.prototype.getTime
            }), Object.defineProperty(Date.prototype, "nv_getFullYear", {
                writable: !0,
                value: Date.prototype.getFullYear
            }), Object.defineProperty(Date.prototype, "nv_getUTCFullYear", {
                writable: !0,
                value: Date.prototype.getUTCFullYear
            }), Object.defineProperty(Date.prototype, "nv_getMonth", {
                writable: !0,
                value: Date.prototype.getMonth
            }), Object.defineProperty(Date.prototype, "nv_getUTCMonth", {
                writable: !0,
                value: Date.prototype.getUTCMonth
            }), Object.defineProperty(Date.prototype, "nv_getDate", {
                writable: !0,
                value: Date.prototype.getDate
            }), Object.defineProperty(Date.prototype, "nv_getUTCDate", {
                writable: !0,
                value: Date.prototype.getUTCDate
            }), Object.defineProperty(Date.prototype, "nv_getDay", {
                writable: !0,
                value: Date.prototype.getDay
            }), Object.defineProperty(Date.prototype, "nv_getUTCDay", {
                writable: !0,
                value: Date.prototype.getUTCDay
            }), Object.defineProperty(Date.prototype, "nv_getHours", {
                writable: !0,
                value: Date.prototype.getHours
            }), Object.defineProperty(Date.prototype, "nv_getUTCHours", {
                writable: !0,
                value: Date.prototype.getUTCHours
            }), Object.defineProperty(Date.prototype, "nv_getMinutes", {
                writable: !0,
                value: Date.prototype.getMinutes
            }), Object.defineProperty(Date.prototype, "nv_getUTCMinutes", {
                writable: !0,
                value: Date.prototype.getUTCMinutes
            }), Object.defineProperty(Date.prototype, "nv_getSeconds", {
                writable: !0,
                value: Date.prototype.getSeconds
            }), Object.defineProperty(Date.prototype, "nv_getUTCSeconds", {
                writable: !0,
                value: Date.prototype.getUTCSeconds
            }), Object.defineProperty(Date.prototype, "nv_getMilliseconds", {
                writable: !0,
                value: Date.prototype.getMilliseconds
            }), Object.defineProperty(Date.prototype, "nv_getUTCMilliseconds", {
                writable: !0,
                value: Date.prototype.getUTCMilliseconds
            }), Object.defineProperty(Date.prototype, "nv_getTimezoneOffset", {
                writable: !0,
                value: Date.prototype.getTimezoneOffset
            }), Object.defineProperty(Date.prototype, "nv_setTime", {
                writable: !0,
                value: Date.prototype.setTime
            }), Object.defineProperty(Date.prototype, "nv_setMilliseconds", {
                writable: !0,
                value: Date.prototype.setMilliseconds
            }), Object.defineProperty(Date.prototype, "nv_setUTCMilliseconds", {
                writable: !0,
                value: Date.prototype.setUTCMilliseconds
            }), Object.defineProperty(Date.prototype, "nv_setSeconds", {
                writable: !0,
                value: Date.prototype.setSeconds
            }), Object.defineProperty(Date.prototype, "nv_setUTCSeconds", {
                writable: !0,
                value: Date.prototype.setUTCSeconds
            }), Object.defineProperty(Date.prototype, "nv_setMinutes", {
                writable: !0,
                value: Date.prototype.setMinutes
            }), Object.defineProperty(Date.prototype, "nv_setUTCMinutes", {
                writable: !0,
                value: Date.prototype.setUTCMinutes
            }), Object.defineProperty(Date.prototype, "nv_setHours", {
                writable: !0,
                value: Date.prototype.setHours
            }), Object.defineProperty(Date.prototype, "nv_setUTCHours", {
                writable: !0,
                value: Date.prototype.setUTCHours
            }), Object.defineProperty(Date.prototype, "nv_setDate", {
                writable: !0,
                value: Date.prototype.setDate
            }), Object.defineProperty(Date.prototype, "nv_setUTCDate", {
                writable: !0,
                value: Date.prototype.setUTCDate
            }), Object.defineProperty(Date.prototype, "nv_setMonth", {
                writable: !0,
                value: Date.prototype.setMonth
            }), Object.defineProperty(Date.prototype, "nv_setUTCMonth", {
                writable: !0,
                value: Date.prototype.setUTCMonth
            }), Object.defineProperty(Date.prototype, "nv_setFullYear", {
                writable: !0,
                value: Date.prototype.setFullYear
            }), Object.defineProperty(Date.prototype, "nv_setUTCFullYear", {
                writable: !0,
                value: Date.prototype.setUTCFullYear
            }), Object.defineProperty(Date.prototype, "nv_toUTCString", {
                writable: !0,
                value: Date.prototype.toUTCString
            }), Object.defineProperty(Date.prototype, "nv_toISOString", {
                writable: !0,
                value: Date.prototype.toISOString
            }), Object.defineProperty(Date.prototype, "nv_toJSON", {
                writable: !0,
                value: Date.prototype.toJSON
            });
        }, x = function() {
            Object.defineProperty(RegExp.prototype, "nv_constructor", {
                writable: !0,
                value: "RegExp"
            }), Object.defineProperty(RegExp.prototype, "nv_exec", {
                writable: !0,
                value: RegExp.prototype.exec
            }), Object.defineProperty(RegExp.prototype, "nv_test", {
                writable: !0,
                value: RegExp.prototype.test
            }), Object.defineProperty(RegExp.prototype, "nv_toString", {
                writable: !0,
                value: RegExp.prototype.toString
            }), Object.defineProperty(RegExp.prototype, "nv_source", {
                get: function() {
                    return this.source;
                },
                set: function() {}
            }), Object.defineProperty(RegExp.prototype, "nv_global", {
                get: function() {
                    return this.global;
                },
                set: function() {}
            }), Object.defineProperty(RegExp.prototype, "nv_ignoreCase", {
                get: function() {
                    return this.ignoreCase;
                },
                set: function() {}
            }), Object.defineProperty(RegExp.prototype, "nv_multiline", {
                get: function() {
                    return this.multiline;
                },
                set: function() {}
            }), Object.defineProperty(RegExp.prototype, "nv_lastIndex", {
                get: function() {
                    return this.lastIndex;
                },
                set: function(e) {
                    this.lastIndex = e;
                }
            });
        };
        "undefined" != typeof __WXML_GLOBAL__ && void 0 !== __WXML_GLOBAL__.wxs_nf_init || (d(), 
        h(), g(), _(), f(), w(), m(), v(), x()), "undefined" != typeof __WXML_GLOBAL__ && (__WXML_GLOBAL__.wxs_nf_init = !0);
        parseInt, parseFloat, isNaN, isFinite, decodeURI, decodeURIComponent, encodeURI, 
        encodeURIComponent;
        function b(e, t, o) {
            if (null == (e = wh.rv(e))) return e;
            if (e.constructor === String || e.constructor === Boolean || e.constructor === Number) return e;
            if (e.constructor === Object) {
                var n = {};
                for (var a in e) e.hasOwnProperty(a) && (void 0 === t ? n[a.substring(3)] = b(e[a], t, o) : n[t + a] = b(e[a], t, o));
                return n;
            }
            if (e.constructor === Array) {
                n = [];
                for (var r = 0; r < e.length; r++) n.push(b(e[r], t, o));
                return n;
            }
            if (e.constructor === Date) return (n = new Date()).setTime(e.getTime()), n;
            if (e.constructor === RegExp) {
                var i = "";
                return e.global && (i += "g"), e.ignoreCase && (i += "i"), e.multiline && (i += "m"), 
                new RegExp(e.source, i);
            }
            if (o && e.constructor === Function) {
                if (1 == o) return b(e(), void 0, 2);
                if (2 == o) return e;
            }
            return null;
        }
        var y = {};
        y.nv_stringify = function(e) {
            return JSON.stringify(e), JSON.stringify(b(e));
        }, y.nv_parse = function(e) {
            if (void 0 !== e) return b(JSON.parse(e), "nv_");
        };
        var A = {};
        void 0 === o.entrys && (o.entrys = {}), A = o.entrys;
        var O = {};
        void 0 === o.defines && (o.defines = {}), O = o.defines;
        var P;
        void 0 === o.modules && (o.modules = {}), P = o.modules || {}, __WXML_GLOBAL__.ops_cached = __WXML_GLOBAL__.ops_cached || {}, 
        __WXML_GLOBAL__.ops_set = __WXML_GLOBAL__.ops_set || {}, __WXML_GLOBAL__.ops_init = __WXML_GLOBAL__.ops_init || {};
        var k = __WXML_GLOBAL__.ops_set.$gwx0 || [];
        __WXML_GLOBAL__.ops_set.$gwx0 = k, __WXML_GLOBAL__.ops_init.$gwx0 = !0;
        L = {}, j = {};
        var L, j, D = [ "./pagesA/album/index.wxml", "./pagesA/dache/index.wxml", "./pagesA/gongzh/gongzh.wxml", "./pagesA/imgparse/index.wxml", "./pagesA/index/index.wxml", "./pagesA/pages/photomarkresult/photomarkresult.wxml", "./pagesA/phone/phone.wxml", "./pagesA/photomark/photomark.wxml", "./pagesA/photomarkresult/photomarkresult.wxml", "./pagesA/success/index.wxml", "./pagesA/waimai/index.wxml" ];
        O[D[0]] = {};
        A[D[0]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_1 || (__WXML_GLOBAL__.ops_cached.$gwx0_1 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_1), __WXML_GLOBAL__.ops_cached.$gwx0_1;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[1]] = {};
        A[D[1]] = {
            f: function(e, t, o, i) {
                var s = (__WXML_GLOBAL__.ops_cached.$gwx0_2 || (__WXML_GLOBAL__.ops_cached.$gwx0_2 = [], 
                function(e) {
                    function t(t) {
                        e.push(t);
                    }
                    t([ 3, "mid_content" ]), t([ 3, "part1" ]), t([ [ 2, "&&" ], [ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 1 ] ], [ 3, "hidden" ] ] ], [ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 2 ] ], [ 3, "hidden" ] ] ] ]), 
                    t([ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 2 ] ], [ 3, "hidden" ] ] ]), 
                    t([ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 3 ] ], [ 3, "hidden" ] ] ]), 
                    t(e[4]), t(e[4]), t(e[4]);
                }(__WXML_GLOBAL__.ops_cached.$gwx0_2)), __WXML_GLOBAL__.ops_cached.$gwx0_2), p = r("view");
                u(s, p, "class", 0, e, t, i);
                var c = r("view");
                u(s, c, "class", 1, e, t, i);
                var d = a();
                n(c, d), l(s, 2, e, t, i) && (d.wxVkey = 1);
                var h = a();
                n(c, h), l(s, 3, e, t, i) && (h.wxVkey = 1), d.wxXCkey = 1, h.wxXCkey = 1, n(p, c);
                var g = a();
                n(p, g), l(s, 4, e, t, i) && (g.wxVkey = 1);
                var _ = a();
                n(p, _), l(s, 5, e, t, i) && (_.wxVkey = 1);
                var f = a();
                n(p, f), l(s, 6, e, t, i) && (f.wxVkey = 1);
                var w = a();
                return n(p, w), l(s, 7, e, t, i) && (w.wxVkey = 1), g.wxXCkey = 1, _.wxXCkey = 1, 
                f.wxXCkey = 1, w.wxXCkey = 1, n(o, p), o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[2]] = {};
        A[D[2]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_3 || (__WXML_GLOBAL__.ops_cached.$gwx0_3 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_3), __WXML_GLOBAL__.ops_cached.$gwx0_3;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[3]] = {};
        A[D[3]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_4 || (__WXML_GLOBAL__.ops_cached.$gwx0_4 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_4), __WXML_GLOBAL__.ops_cached.$gwx0_4;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[4]] = {};
        A[D[4]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_5 || (__WXML_GLOBAL__.ops_cached.$gwx0_5 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_5), __WXML_GLOBAL__.ops_cached.$gwx0_5;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[5]] = {};
        A[D[5]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_6 || (__WXML_GLOBAL__.ops_cached.$gwx0_6 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_6), __WXML_GLOBAL__.ops_cached.$gwx0_6;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[6]] = {};
        A[D[6]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_7 || (__WXML_GLOBAL__.ops_cached.$gwx0_7 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_7), __WXML_GLOBAL__.ops_cached.$gwx0_7;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[7]] = {};
        A[D[7]] = {
            f: function(e, t, o, i) {
                var s = (__WXML_GLOBAL__.ops_cached.$gwx0_8 || (__WXML_GLOBAL__.ops_cached.$gwx0_8 = [], 
                function(e) {
                    function t(t) {
                        e.push(t);
                    }
                    t([ [ 7 ], [ 3, "photoPath" ] ]), t([ [ 6 ], [ [ 7 ], [ 3, "u8ad" ] ], [ 3, "adData" ] ]), 
                    t([ 3, "u8adLoad" ]), t([ 3, "u8adClick" ]), t([ 3, "u8adClose" ]), t([ 3, "u8_component" ]), 
                    t([ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "u8ad" ] ], [ 3, "ad" ] ], [ 3, "fixed" ] ]);
                }(__WXML_GLOBAL__.ops_cached.$gwx0_8)), __WXML_GLOBAL__.ops_cached.$gwx0_8), p = a();
                return n(o, p), l(s, 0, e, t, i) && (p.wxVkey = 1), n(o, function(e, t, o, n, a, i, s) {
                    for (var p = r(t), c = 0, l = 0; l < o.length; l += 2) c + o[l + 1] < 0 ? p.attr[o[l]] = !0 : (u(e, p, o[l], c + o[l + 1], a, i, s), 
                    0 === c && (c = o[l + 1]));
                    for (l = 0; l < n.length; l += 2) if (c + n[l + 1] < 0) p.generics[n[l]] = ""; else {
                        var d = grb(e[c + n[l + 1]], a, i, s);
                        "" != d && (d = "wx-" + d), p.generics[n[l]] = d, 0 === c && (c = n[l + 1]);
                    }
                    return p;
                }(s, "u8-ad", [ "adData", 1, "bindadload", 1, "bindclick", 2, "bindclose", 3, "class", 4, "data-id", 5 ], [], e, t, i)), 
                p.wxXCkey = 1, o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[8]] = {};
        A[D[8]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_9 || (__WXML_GLOBAL__.ops_cached.$gwx0_9 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_9), __WXML_GLOBAL__.ops_cached.$gwx0_9;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[9]] = {};
        A[D[9]] = {
            f: function(e, t, o, n) {
                __WXML_GLOBAL__.ops_cached.$gwx0_10 || (__WXML_GLOBAL__.ops_cached.$gwx0_10 = [], 
                __WXML_GLOBAL__.ops_cached.$gwx0_10), __WXML_GLOBAL__.ops_cached.$gwx0_10;
                return o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, O[D[10]] = {};
        if (A[D[10]] = {
            f: function(e, t, o, i) {
                var s = (__WXML_GLOBAL__.ops_cached.$gwx0_11 || (__WXML_GLOBAL__.ops_cached.$gwx0_11 = [], 
                function(e) {
                    function t(t) {
                        e.push(t);
                    }
                    t([ 3, "mid_content" ]), t([ 3, "part1" ]), t([ [ 2, "&&" ], [ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 1 ] ], [ 3, "hidden" ] ] ], [ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 2 ] ], [ 3, "hidden" ] ] ] ]), 
                    t([ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 2 ] ], [ 3, "hidden" ] ] ]), 
                    t([ [ 2, "!" ], [ [ 6 ], [ [ 6 ], [ [ 7 ], [ 3, "apps" ] ], [ 1, 3 ] ], [ 3, "hidden" ] ] ]), 
                    t(e[4]), t(e[4]);
                }(__WXML_GLOBAL__.ops_cached.$gwx0_11)), __WXML_GLOBAL__.ops_cached.$gwx0_11), p = r("view");
                u(s, p, "class", 0, e, t, i);
                var c = r("view");
                u(s, c, "class", 1, e, t, i);
                var d = a();
                n(c, d), l(s, 2, e, t, i) && (d.wxVkey = 1);
                var h = a();
                n(c, h), l(s, 3, e, t, i) && (h.wxVkey = 1), d.wxXCkey = 1, h.wxXCkey = 1, n(p, c);
                var g = a();
                n(p, g), l(s, 4, e, t, i) && (g.wxVkey = 1);
                var _ = a();
                n(p, _), l(s, 5, e, t, i) && (_.wxVkey = 1);
                var f = a();
                return n(p, f), l(s, 6, e, t, i) && (f.wxVkey = 1), g.wxXCkey = 1, _.wxXCkey = 1, 
                f.wxXCkey = 1, n(o, p), o;
            },
            j: [],
            i: [],
            ti: [],
            ic: []
        }, t && A[t]) return function(o, n, a) {
            e = 0;
            var r = {
                tag: "wx-page",
                children: []
            }, i = A[t].f;
            void 0 === a && (a = {}), a.f = b(P[t], "", 1);
            try {
                i(o, {}, r, a), function e(t) {
                    "wx-wx-scope" == t.tag && (t.tag = "virtual", t.wxCkey = "11", t.wxScopeData = t.attr["wx:scope-data"], 
                    delete t.n, delete t.raw, delete t.generics, delete t.attr);
                    for (var o = 0; t.children && o < t.children.length; o++) e(t.children[o]);
                    return t;
                }(r);
            } catch (e) {
                console.log(e);
            }
            return r;
        };
    }, __wxAppCode__["pagesA/album/index.json"] = {
        navigationBarTitleText: "截图带壳",
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/album/index.wxml"] = [ $gwx0, "./pagesA/album/index.wxml" ] : __wxAppCode__["pagesA/album/index.wxml"] = $gwx0("./pagesA/album/index.wxml"), 
    __wxAppCode__["pagesA/dache/index.json"] = {
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/dache/index.wxml"] = [ $gwx0, "./pagesA/dache/index.wxml" ] : __wxAppCode__["pagesA/dache/index.wxml"] = $gwx0("./pagesA/dache/index.wxml"), 
    __wxAppCode__["pagesA/gongzh/gongzh.json"] = {
        navigationBarTitleText: "重要通知",
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/gongzh/gongzh.wxml"] = [ $gwx0, "./pagesA/gongzh/gongzh.wxml" ] : __wxAppCode__["pagesA/gongzh/gongzh.wxml"] = $gwx0("./pagesA/gongzh/gongzh.wxml"), 
    __wxAppCode__["pagesA/imgparse/index.json"] = {
        navigationBarTitleText: "图集去水印",
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/imgparse/index.wxml"] = [ $gwx0, "./pagesA/imgparse/index.wxml" ] : __wxAppCode__["pagesA/imgparse/index.wxml"] = $gwx0("./pagesA/imgparse/index.wxml"), 
    __wxAppCode__["pagesA/index/index.json"] = {
        navigationBarTitleText: "截图带壳 Pro",
        disableScroll: !0,
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/index/index.wxml"] = [ $gwx0, "./pagesA/index/index.wxml" ] : __wxAppCode__["pagesA/index/index.wxml"] = $gwx0("./pagesA/index/index.wxml"), 
    __wxAppCode__["pagesA/pages/photomarkresult/photomarkresult.json"] = {
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/pages/photomarkresult/photomarkresult.wxml"] = [ $gwx0, "./pagesA/pages/photomarkresult/photomarkresult.wxml" ] : __wxAppCode__["pagesA/pages/photomarkresult/photomarkresult.wxml"] = $gwx0("./pagesA/pages/photomarkresult/photomarkresult.wxml"), 
    __wxAppCode__["pagesA/phone/phone.json"] = {
        navigationBarTitleText: "手机检测",
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/phone/phone.wxml"] = [ $gwx0, "./pagesA/phone/phone.wxml" ] : __wxAppCode__["pagesA/phone/phone.wxml"] = $gwx0("./pagesA/phone/phone.wxml"), 
    __wxAppCode__["pagesA/photomark/photomark.json"] = {
        navigationBarTitleText: "图片加水印",
        usingComponents: {
            "u8-ad": "/components/u8AD/u8AD"
        }
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/photomark/photomark.wxml"] = [ $gwx0, "./pagesA/photomark/photomark.wxml" ] : __wxAppCode__["pagesA/photomark/photomark.wxml"] = $gwx0("./pagesA/photomark/photomark.wxml"), 
    __wxAppCode__["pagesA/photomarkresult/photomarkresult.json"] = {
        navigationBarTitleText: "图片加水印",
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/photomarkresult/photomarkresult.wxml"] = [ $gwx0, "./pagesA/photomarkresult/photomarkresult.wxml" ] : __wxAppCode__["pagesA/photomarkresult/photomarkresult.wxml"] = $gwx0("./pagesA/photomarkresult/photomarkresult.wxml"), 
    __wxAppCode__["pagesA/success/index.json"] = {
        navigationBarTitleText: "截图带壳 Pro",
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/success/index.wxml"] = [ $gwx0, "./pagesA/success/index.wxml" ] : __wxAppCode__["pagesA/success/index.wxml"] = $gwx0("./pagesA/success/index.wxml"), 
    __wxAppCode__["pagesA/waimai/index.json"] = {
        usingComponents: {}
    }, __vd_version_info__.delayedGwx ? __wxAppCode__["pagesA/waimai/index.wxml"] = [ $gwx0, "./pagesA/waimai/index.wxml" ] : __wxAppCode__["pagesA/waimai/index.wxml"] = $gwx0("./pagesA/waimai/index.wxml"), 
    __wxRoute = "pagesA/gongzh/gongzh", __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/gongzh/gongzh.js", 
    define("pagesA/gongzh/gongzh.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        var b, y = e("../../@babel/runtime/helpers/interopRequireDefault")(e("../../@babel/runtime/helpers/defineProperty"));
        Page((b = {
            data: {
                modalHidden: !0
            },
            onLoad: function(e) {},
            showPic: function() {},
            goToApp: function() {
                wx.navigateToMiniProgram({
                    appId: "wx2795ab2f7c07769a",
                    success: function(e) {}
                });
            },
            goToxmApp: function() {
                wx.navigateToMiniProgram({
                    appId: "wx2db9e3f582c44b95",
                    success: function(e) {}
                });
            }
        }, (0, y.default)(b, "showPic", function() {
            wx.previewImage({
                urls: [ "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-f7f3f46c-2ca3-4514-81d9-6144e0e5180f/71684750-83d6-489a-8540-71cb4e7a9173.jpg" ]
            });
        }), (0, y.default)(b, "goBack", function() {
            wx.navigateBack({
                delta: 0
            });
        }), (0, y.default)(b, "showMoneyPic", function() {
            wx.previewImage({
                urls: [ "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-f7f3f46c-2ca3-4514-81d9-6144e0e5180f/d4f90b3f-e7ed-4761-81b3-024657a84028.jpg" ]
            });
        }), b));
    }), require("pagesA/pagesA/gongzh/gongzh.js"), __wxRoute = "pagesA/pages/photomarkresult/photomarkresult", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/pages/photomarkresult/photomarkresult.js", 
    define("pagesA/pages/photomarkresult/photomarkresult.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        Page({
            data: {},
            onLoad: function(e) {},
            onReady: function() {},
            onShow: function() {},
            onHide: function() {},
            onUnload: function() {},
            onPullDownRefresh: function() {},
            onReachBottom: function() {},
            onShareAppMessage: function() {}
        });
    }), require("pagesA/pagesA/pages/photomarkresult/photomarkresult.js"), __wxRoute = "pagesA/waimai/index", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/waimai/index.js", define("pagesA/waimai/index.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        Page({
            data: {
                foodname: "今天吃什么呢？",
                fooditems: [ "呀,今天没饭吃", "花甲粉", "牛杂煲", "煎饼果子", "凉面", "焗饭", "意大利面", "烤鱼", "猪肚鸡", "潮汕牛肉火锅", "重庆火锅", "四川火锅", "麻辣香锅", "酸菜鱼", "兰州拉面", "炒粿条", "奶茶", "煎饺", "肉包", "沙拉", "猪杂粉", "牛腩面", "白切鸡", "螺蛳粉", "麻辣烫", "蛋包饭", "本帮菜", "甜点", "桂林米粉", "粥", "铁板烧", "海鲜", "烤肉", "烤鱼", "沙县酒店", "汉堡王", "肯德基", "麦当劳", "披萨", "菜饭骨头汤", "扬州炒饭", "面包", "家常豆腐", "手撕包菜", "泡椒鸡杂", "红烧回肠", "酸菜魔芋", "干煸土豆丝", "肉夹馍", "涮羊肉", "日料", "水饺", "方便面", "烧烤", "馄饨", "炒面", "抄手", "小炒肉", "猪脚饭", "大盘鸡", "酸菜粉丝汤", "番茄炒蛋", "鱼香茄子", "青椒土豆丝", "蒜苔炒肉", "清蒸鲈鱼", "土豆烧排骨", "蚝油生菜", "爆炒猪肝", "麻婆豆腐", "回锅肉", "麻辣烫", "糖醋排骨", "猪肉炖粉条", "红烧肉", "红烧肉", "盖浇饭", "包子", "馒头", "水煮牛肉", "棒棒鸡" ],
                animate: !1,
                isclick: !1,
                dateMonth: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ][new Date().getMonth()],
                dateDay: new Date().getDate(),
                dateWeek: [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ][new Date().getDay()]
            },
            onLoad: function(e) {},
            onReady: function() {
                this.getTimeFood();
            },
            getTimeFood: function() {
                var e = this, t = new Date().getHours() + ":" + new Date().getMinutes();
                console.log(t), wx.request({
                    url: "https://res.guqule.com/time.json?v=" + new Date().getMinutes(),
                    method: "get",
                    data: {},
                    success: function(o) {
                        console.log(o.data), o.data.map(function(o, n) {
                            (e.equalDate(t, o.start_time) || e.compareDate(o.start_time, t)) && e.compareDate(t, o.end_time) && e.setData({
                                foodname: o.text,
                                fooditems: o.menus
                            });
                        }), "今天吃什么呢？" == e.foodname && (e.foodname = o.data[4].text, e.fooditems = o.data[4].menus);
                    }
                });
            },
            compareDate: function(e, t) {
                var o = new Date(), n = e.split(":"), a = t.split(":");
                return o.setHours(n[0], n[1]) < o.setHours(a[0], a[1]);
            },
            equalDate: function(e, t) {
                var o = new Date(), n = e.split(":"), a = t.split(":");
                return o.setHours(n[0], n[1]) == o.setHours(a[0], a[1]);
            },
            rolledUp: function() {
                console.log(this.data.animate), this.data.animate || (this.setData({
                    isclick: !0,
                    animate: !0
                }), this.bbb(100, 10, 0));
            },
            bbb: function(e, t, o) {
                var n = this;
                n.setData({
                    foodname: n.data.fooditems[Math.floor(Math.random() * n.data.fooditems.length)]
                }), n.timer = setInterval(function() {
                    n.setData({
                        foodname: n.data.fooditems[Math.floor(Math.random() * n.data.fooditems.length)]
                    }), clearInterval(n.timer), n.timer = null, t > o ? n.bbb(e, t, o + 1) : n.setData({
                        animate: !1
                    });
                }, e);
            },
            gotoeleOtherApp1: function() {
                wx.navigateToMiniProgram({
                    appId: "wxece3a9a4c82f58c9",
                    path: "taoke/pages/shopping-guide/index?scene=ZaFsEau"
                });
            },
            gotoPinOtherApp2: function() {
                wx.navigateToMiniProgram({
                    appId: "wxde8ac0a21135c07d",
                    path: "/index/pages/h5/h5?lch=cps:waimai:5:65c5f4b9271221c79eae104d969a48a3:5338jutuike20220301:33:85459&weburl=https%3A%2F%2Fdpurl.cn%2FhubtvlKz&f_userId=1&f_token=1"
                });
            },
            gotoMeiOtherApp3: function() {
                wx.navigateToMiniProgram({
                    appId: "wxece3a9a4c82f58c9",
                    path: "ele-recommend-price/pages/guest/index?inviterId=2ca49072&chInfo=ch_wechat_chsub_Card&_ltracker_f="
                });
            },
            gotomeiOtherApp4: function() {
                wx.navigateToMiniProgram({
                    appId: "wxece3a9a4c82f58c9",
                    path: "pages/sharePid/web/index?scene=s.click.ele.me/HF2yrlu&o2i_sharefrom=wxminiapp"
                });
            },
            gotomeiOtherApp5: function() {
                wx.navigateToMiniProgram({
                    appId: "wxde8ac0a21135c07d",
                    path: "/index/pages/h5/h5?lch=mhqIykekFEV63u81zLSTaQViQ&noshare=1&f_userId=0&f_openId=0&f_token=0&weburl=https%3A%2F%2Fdpurl.cn%2FsYvwIIMz"
                });
            },
            onShow: function() {},
            onHide: function() {},
            onUnload: function() {},
            onPullDownRefresh: function() {},
            onReachBottom: function() {},
            onShareAppMessage: function() {}
        });
    }), require("pagesA/pagesA/waimai/index.js"), __wxRoute = "pagesA/imgparse/index", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/imgparse/index.js", define("pagesA/imgparse/index.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        var b, y, A, O, P, k, L, j, D, M;
        k = (P = e("../../@babel/runtime/helpers/interopRequireDefault"))(e("../../@babel/runtime/helpers/defineProperty")), 
        L = P(e("../../@babel/runtime/regenerator")), j = P(e("../../@babel/runtime/helpers/asyncToGenerator")), 
        D = e("../../request/video.js"), P(e("../../lib/runtime/runtime")), M = e("../../utils/base64"), 
        Page((b = {
            data: {
                url: "",
                imageObj: {}
            },
            reward: function() {
                wx.previewImage({
                    current: "https://www.helloimg.com/images/2022/01/30/G8NO5g.jpg",
                    urls: [ "https://www.helloimg.com/images/2022/01/30/G8NO5g.jpg" ]
                });
            },
            waimai: function(e) {
                wx.navigateTo({
                    url: "/pagesA/waimai/index"
                });
            },
            onLoad: function(e) {},
            onReady: function() {},
            onShow: function() {},
            onHide: function() {},
            onUnload: function() {},
            onPullDownRefresh: function() {},
            onReachBottom: function() {},
            onShareAppMessage: function() {},
            adLoad: function() {
                console.log("小程序视频广告加载成功");
            },
            adError: function(e) {
                console.log("小程序视频广告加载失败", e);
            },
            adClose: function() {
                console.log("小程序视频广告关闭");
            },
            onHelpPage: function() {
                wx.navigateTo({
                    url: "/pagesB/find/xhsimage/help"
                });
            },
            handleUrlInput: function(e) {
                this.setData({
                    url: e.detail.value
                });
            },
            handleCleanUrl: function() {
                this.setData({
                    url: ""
                });
            },
            handleImagePares: (O = (0, j.default)(L.default.mark(function e() {
                var t, o, n;
                return L.default.wrap(function(e) {
                    for (;;) switch (e.prev = e.next) {
                      case 0:
                        if (t = this.data.url) {
                            e.next = 4;
                            break;
                        }
                        return wx.showToast({
                            title: "请输入或粘贴微博链接",
                            icon: "none"
                        }), e.abrupt("return");

                      case 4:
                        return o = {
                            url: t
                        }, e.next = 7, (0, D.request)({
                            url: "/image/weibo",
                            data: o,
                            method: "post"
                        });

                      case 7:
                        if (n = e.sent) {
                            e.next = 11;
                            break;
                        }
                        return wx.showToast({
                            title: "获取失败请重试",
                            icon: "error"
                        }), e.abrupt("return");

                      case 11:
                        this.setData({
                            imageObj: n
                        });

                      case 12:
                      case "end":
                        return e.stop();
                    }
                }, e, this);
            })), function() {
                return O.apply(this, arguments);
            })
        }, (0, k.default)(b, "handleImagePares", (A = (0, j.default)(L.default.mark(function e() {
            var t, o, n;
            return L.default.wrap(function(e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    if (t = this.data.url) {
                        e.next = 4;
                        break;
                    }
                    return wx.showToast({
                        title: "点击输入或粘贴图集链接",
                        icon: "none"
                    }), e.abrupt("return");

                  case 4:
                    return o = {
                        url: t
                    }, e.next = 7, (0, D.request)({
                        url: "/image/v2.parse",
                        data: o,
                        method: "post"
                    });

                  case 7:
                    if (n = e.sent) {
                        e.next = 11;
                        break;
                    }
                    return wx.showToast({
                        title: "获取失败请重试",
                        icon: "error"
                    }), e.abrupt("return");

                  case 11:
                    this.setData({
                        imageObj: n
                    });

                  case 12:
                  case "end":
                    return e.stop();
                }
            }, e, this);
        })), function() {
            return A.apply(this, arguments);
        })), (0, k.default)(b, "handleCopyTitle", function() {
            wx.setClipboardData({
                data: this.data.imageObj.title,
                success: function(e) {
                    wx.showToast({
                        title: "标题复制成功",
                        icon: "success"
                    });
                },
                fail: function() {},
                complete: function() {}
            });
        }), (0, k.default)(b, "handleCopyDesc", function() {
            wx.setClipboardData({
                data: this.data.imageObj.desc,
                success: function(e) {
                    wx.showToast({
                        title: "内容复制成功",
                        icon: "success"
                    });
                },
                fail: function() {},
                complete: function() {}
            });
        }), (0, k.default)(b, "handlePreviewImage", function(e) {
            var t = e.currentTarget.dataset.item;
            wx.previewImage({
                current: t,
                urls: this.data.imageObj.pics,
                success: function(e) {},
                fail: function() {},
                complete: function() {}
            });
        }), (0, k.default)(b, "handleSaveImage", (y = (0, j.default)(L.default.mark(function e() {
            var t;
            return L.default.wrap(function(e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    (t = this).data.imageObj.pics.length > 0 && wx.getSetting({
                        success: function(e) {
                            t.downloadFile();
                        },
                        fail: function(e) {
                            console.log(e), wx.showToast({
                                title: "保存失败",
                                icon: "error",
                                duration: 1500
                            });
                        }
                    });

                  case 2:
                  case "end":
                    return e.stop();
                }
            }, e, this);
        })), function() {
            return y.apply(this, arguments);
        })), (0, k.default)(b, "downloadFile", function() {
            var e = this;
            wx.showLoading({
                title: "保存中...",
                mask: !0
            });
            for (var t = 0, o = 0; o < e.data.imageObj.pics.length; o++) {
                var n = e.data.imageObj.pics[o], a = "https://kejikk.com/image/down?path=" + M.encode(n);
                wx.downloadFile({
                    url: a,
                    success: function(o) {
                        if (o.dataLength > 0) {
                            var n = o.tempFilePath;
                            wx.saveImageToPhotosAlbum({
                                filePath: n,
                                success: function(o) {
                                    ++t == e.data.imageObj.pics.length - 1 && (wx.hideLoading(), wx.showToast({
                                        title: "保存成功",
                                        icon: "success",
                                        duration: 1500
                                    }));
                                },
                                fail: function(o) {
                                    console.log(o), ++t == e.data.imageObj.pics.length - 1 && (wx.hideLoading(), wx.showToast({
                                        title: "保存成功",
                                        icon: "success",
                                        duration: 1500
                                    }));
                                }
                            });
                        } else console.log("保存失败"), wx.hideLoading();
                    },
                    fail: function(e) {
                        console.log(e), wx.hideLoading();
                    }
                });
            }
        }), b));
    }), require("pagesA/pagesA/imgparse/index.js"), __wxRoute = "pagesA/phone/phone", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/phone/phone.js", define("pagesA/phone/phone.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        Page({
            data: {
                mobilepinpai: "",
                mobileModel: "",
                mobilesystem: "",
                screenWidth: "",
                screenHeight: "",
                language: "",
                version: "",
                xinagsubi: ""
            },
            onShareTimeline: function() {
                return {
                    title: "我的手机是正品，你也来鉴定一下吧",
                    imageUrl: "/images/hh.jpg"
                };
            },
            onShareAppMessage: function(e) {
                return "button" === e.from && console.log(e.target), {
                    title: "我的手机是正品，你也来鉴定一下吧",
                    imageUrl: "/images/hh.jpg"
                };
            },
            onLoad: function() {
                var e = this;
                wx.getSystemInfo({
                    success: function(t) {
                        var o = t.model;
                        if (console.log(o), -1 !== o.indexOf("<")) var n = o.indexOf("<"), a = o.substr(0, n); else a = o;
                        if (-1 !== o.indexOf("(") ? (n = o.indexOf("("), a = o.substr(0, n)) : a = o, -1 !== o.indexOf("<") ? (n = o.indexOf("<"), 
                        a = o.substr(0, n)) : a = o, o = a, "zh_HK" == t.language || "zh_TW" == t.language) var r = "繁体中文"; else r = "en" == t.language ? "英文" : "简体中文";
                        var i = parseInt(t.screenWidth * t.pixelRatio), s = parseInt(t.screenHeight * t.pixelRatio);
                        e.setData({
                            mobilepinpai: t.brand,
                            mobileModel: o,
                            mobilesystem: t.system,
                            xiangsubi: t.pixelRatio,
                            screenWidth: i,
                            screenHeight: s,
                            language: r,
                            version: t.version
                        });
                    }
                });
            }
        });
    }), require("pagesA/pagesA/phone/phone.js"), __wxRoute = "pagesA/album/index", __wxRouteBegin = !0, 
    __wxAppCurrentFile__ = "pagesA/album/index.js", define("pagesA/album/index.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        getApp();
        var b = null;
        Page({
            data: {
                msg: "",
                img: ""
            },
            goAlbum: function() {
                var e = this;
                wx.chooseImage({
                    count: 1,
                    sourceType: [ "album" ],
                    sizeType: [ "original", "compressed" ],
                    success: function(t) {
                        wx.cloud.callFunction({
                            name: "ContentCheck",
                            data: {
                                img: e.data.img
                            },
                            success: function(e) {
                                console.log(e.result), 87014 == e.result.imageR.errCode ? wx.showToast({
                                    title: "图片违规"
                                }) : wx.showToast({
                                    title: "图片安全"
                                });
                            }
                        }), wx.getImageInfo({
                            src: t.tempFilePaths[0],
                            success: function(t) {
                                var o = t.height / t.width;
                                e.goIndex(t.width, t.height, o, t.path);
                            }
                        });
                    }
                });
            },
            goIndex: function(e, t, o, n) {
                wx.navigateTo({
                    url: "/pagesA/index/index?pic=" + n + "&k=" + o + "&width=" + e + "&height=" + t
                });
            },
            goAbout: function() {
                wx.navigateTo({
                    url: "/pagesA/about/index"
                });
            },
            paytome: function() {
                wx.previewImage({
                    urls: [ "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-1f712703-6a3e-4be5-afdd-3198babb75c4/8c1da447-d08c-49b0-97b1-df7a83c1ca77.jpg" ],
                    success: function(e) {}
                });
            },
            onShareAppMessage: function() {
                return {
                    path: "/pagesA/album/index",
                    title: "推荐使用[截图带壳 Pro],潮人截图、壁纸分享必备小程序",
                    desc: "让您轻松的把截图套上手机壳，再分享截图、壁纸或者App给好友呢！",
                    imageUrl: "https://cdn.uiunion.cn/screenshot/images/images-share.png"
                };
            },
            onReady: function() {
                wx.createInterstitialAd && ((b = wx.createInterstitialAd({
                    adUnitId: "adunit-b9abf64b40dddf8d"
                })).onLoad(function() {
                    console.log("onLoad event emit");
                }), b.onError(function(e) {
                    console.log("onError event emit", e);
                }), b.onClose(function(e) {
                    console.log("onClose event emit", e);
                }));
            },
            onShow: function() {
                b && b.show().catch(function(e) {
                    console.error(e);
                });
            }
        });
    }), require("pagesA/pagesA/album/index.js"), __wxRoute = "pagesA/index/index", __wxRouteBegin = !0, 
    __wxAppCurrentFile__ = "pagesA/index/index.js", define("pagesA/index/index.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        getApp();
        var b = null;
        Page({
            data: {
                userImage: "",
                userImageWidth: 0,
                userImageHeight: 0,
                userImageK: 0,
                bgMarginLeft: 20,
                bgMarginTop: 20,
                bgSrc: [ {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-silver.png",
                    srcs: [ {
                        name: "银 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-silver.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "深空灰",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-space-gray.png",
                        isCDN: "true"
                    }, {
                        name: "金 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-gold.png",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11 Pro/Max",
                    originBgFrame: [ 0, 0, 509, 1020 ],
                    originPicFrame: [ 37, 27, 436, 944 ],
                    originBgOffset: [ 80, 40 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-desk-2.png",
                    srcs: [ {
                        name: "带刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-desk-2.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-desk-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11 Pro 充电",
                    originBgFrame: [ 0, 0, 943, 1055 ],
                    originPicFrame: [ 487, 323, 310, 670 ],
                    originBgOffset: [ 0, 0 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-in-hand-01.png",
                    srcs: [ {
                        name: "带刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-in-hand-01.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-in-hand-01-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11 Pro 手持",
                    originBgFrame: [ 0, 0, 872, 591 ],
                    originPicFrame: [ 365, 104, 168, 363 ],
                    originBgOffset: [ 0, 0 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-in-hand-02.png",
                    srcs: [ {
                        name: "带刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-in-hand-02.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-in-hand-02-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11 Pro 手持",
                    originBgFrame: [ 0, 0, 713, 980 ],
                    originPicFrame: [ 272, 145, 263, 569 ],
                    originBgOffset: [ 0, 0 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-plants.png",
                    srcs: [ {
                        name: "带刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-plants.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-plants-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11 Pro 多肉",
                    originBgFrame: [ 0, 0, 900, 756 ],
                    originPicFrame: [ 82, 170, 193, 417 ],
                    originBgOffset: [ 0, 0 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-card.png",
                    srcs: [ {
                        name: "带刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-card.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-pro-max-card-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11 Pro 黑卡",
                    originBgFrame: [ 0, 0, 829, 728 ],
                    originPicFrame: [ 129, 124, 187, 403 ],
                    originBgOffset: [ 0, 0 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-white.png",
                    srcs: [ {
                        name: "白 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-white.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "黑 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-black.png",
                        isCDN: "true"
                    }, {
                        name: "红 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-red.png",
                        isCDN: "true"
                    }, {
                        name: "黄 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-yellow.png",
                        isCDN: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-11-black-no.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 11",
                    originBgFrame: [ 0, 0, 497, 993 ],
                    originPicFrame: [ 40, 32, 414, 900 ],
                    originBgOffset: [ 80, 40 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-8-plus-grey.png",
                    srcs: [ {
                        name: "黑 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-8-plus-grey.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "银 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-8-plus-silver.png",
                        isCDN: "true"
                    }, {
                        name: "金 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-8-plus-gold.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone 7/8 Plus",
                    originBgFrame: [ 0, 0, 446, 916 ],
                    originPicFrame: [ 27, 101, 393, 699 ],
                    originBgOffset: [ 80, 40 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-se-space-gray.png",
                    srcs: [ {
                        name: "深空灰",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-se-space-gray.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "玫瑰金",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-se-rose-gold.png",
                        isCDN: "true"
                    }, {
                        name: "金 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-se-gold.png",
                        isCDN: "true"
                    }, {
                        name: "银 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/iphone-se-gold.png",
                        isCDN: "true"
                    } ],
                    title: "iPhone SE/5s/5",
                    originBgFrame: [ 0, 0, 383, 801 ],
                    originPicFrame: [ 26, 111, 329, 585 ],
                    originBgOffset: [ 80, 40 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/huawei/huawei-mate30.png",
                    srcs: [ {
                        name: "带刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/huawei/huawei-mate30.png",
                        isCDN: "true",
                        checked: "true"
                    }, {
                        name: "无刘海",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/huawei/huawei-mate30-no.png",
                        isCDN: "true"
                    } ],
                    title: "Huawei Mate 30",
                    originBgFrame: [ 0, 0, 360, 668 ],
                    originPicFrame: [ 35, 22, 288, 625 ],
                    originBgOffset: [ 40, 24 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-10-pro-sky-blue.png",
                    srcs: [ {
                        name: "星空蓝",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-10-pro-sky-blue.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "无摄像头",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-10-pro-sky-blue-no.png",
                        isCDN: "true"
                    } ],
                    title: "小米10 Pro",
                    originBgFrame: [ 0, 0, 600, 1199 ],
                    originPicFrame: [ 31, 20, 537, 1163 ],
                    originBgOffset: [ 64, 58 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-3-no.png",
                    srcs: [ {
                        name: "无后盖",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-3-no.png",
                        checked: "true",
                        isCDN: "true"
                    } ],
                    title: "小米MIX 3",
                    originBgFrame: [ 0, 0, 468, 880 ],
                    originPicFrame: [ 46, 36, 374, 810 ],
                    originBgOffset: [ 40, 20 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-3-palace.png",
                    srcs: [ {
                        name: "故宫版",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-3-palace.png",
                        checked: "true",
                        isCDN: "true"
                    }, {
                        name: "经典黑",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-3-black.png",
                        isCDN: "true"
                    } ],
                    title: "小米MIX3 带后盖",
                    originBgFrame: [ 0, 0, 782, 880 ],
                    originPicFrame: [ 341, 28, 382, 828 ],
                    originBgOffset: [ 64, 58 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-alpho-2.png",
                    srcs: [ {
                        name: "星空蓝",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/xiaomi/xiaomi-mix-alpho-2.png",
                        checked: "true",
                        isCDN: "true"
                    } ],
                    title: "小米MIX Alrha",
                    originBgFrame: [ 0, 0, 604, 881 ],
                    originPicFrame: [ 198, 204, 264, 572 ],
                    originBgOffset: [ 0, 0 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/oppo/oppo-reno3-pro.png",
                    srcs: [ {
                        name: "日出印象",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/oppo/oppo-reno3-pro.png",
                        isCDN: "true",
                        checked: "true"
                    }, {
                        name: "无摄像头",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/oppo/oppo-reno3-pro-no.png",
                        isCDN: "true"
                    } ],
                    title: "OPPO Reno3 Rro",
                    originBgFrame: [ 0, 0, 600, 1181 ],
                    originPicFrame: [ 52, 40, 496, 1102 ],
                    originBgOffset: [ 30, 20 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/oppo/one-plus7-pro-black.png",
                    srcs: [ {
                        name: "黑 色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/oppo/one-plus7-pro-black.png",
                        isCDN: "true",
                        checked: "true"
                    } ],
                    title: "One Plus 7 Pro",
                    originBgFrame: [ 0, 0, 538, 980 ],
                    originPicFrame: [ 57, 29, 423, 914 ],
                    originBgOffset: [ 0, 40 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/samsung/samsung-galaxy-note-10.png",
                    srcs: [ {
                        name: "星空蓝",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/samsung/samsung-galaxy-note-10.png",
                        isCDN: "true",
                        checked: "true"
                    }, {
                        name: "无摄像头",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/samsung/samsung-galaxy-note-10-no.png",
                        isCDN: "true"
                    } ],
                    title: "Samsung Note 10",
                    originBgFrame: [ 0, 0, 702, 1256 ],
                    originPicFrame: [ 83, 53, 533, 1140 ],
                    originBgOffset: [ 40, 24 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/samsung/samsung-galaxy-s10-silver.png",
                    srcs: [ {
                        name: "黑  色",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/samsung/samsung-galaxy-s10-silver.png",
                        isCDN: "true",
                        checked: "true"
                    }, {
                        name: "无摄像头",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/samsung/samsung-galaxy-s10-silver-no.png",
                        isCDN: "true"
                    } ],
                    title: "Samsung S10",
                    originBgFrame: [ 0, 0, 642, 1228 ],
                    originPicFrame: [ 56, 51, 530, 1117 ],
                    originBgOffset: [ 40, 40 ]
                }, {
                    selectedSrcIndex: 0,
                    selectedSrc: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/apple-watch-42mm-silver.png",
                    srcs: [ {
                        name: "银 蓝",
                        src: "https://7569-uiunion-screenshot-168-1301433344.tcb.qcloud.la/mockups/apple/apple-watch-42mm-silver.png",
                        isCDN: "true",
                        checked: "true"
                    } ],
                    title: "AppleWatch",
                    originBgFrame: [ 0, 0, 512, 950 ],
                    originPicFrame: [ 100, 280, 312, 390 ],
                    originBgOffset: [ 58, 0 ]
                } ],
                selectedBgItem: {},
                canvasWidth: 0,
                canvasHeight: 0,
                screenWidth: 375,
                screenHeight: 667,
                windowHeight: 603,
                pixelRatio: 2,
                bgWidth: 225,
                bgHeight: 449,
                picWidth: 180,
                picHeight: 319,
                marginLeft: 23,
                marginTop: 66
            },
            onLoad: function(e) {
                this.setData({
                    userImage: e.pic,
                    userImageWidth: e.width,
                    userImageHeight: e.height,
                    userImageK: e.k
                });
                var t = this;
                e.k > 2 ? this.setData({
                    selectedBgItem: this.data.bgSrc[3]
                }) : this.setData({
                    selectedBgItem: this.data.bgSrc[0]
                }), wx.getSystemInfo({
                    success: function(e) {
                        t.setData({
                            screenWidth: e.screenWidth,
                            screenHeight: e.screenHeight,
                            windowHeight: e.windowHeight,
                            pixelRatio: e.pixelRatio
                        }), t.findPosition(0);
                    }
                });
            },
            changeBg: function(e) {
                this.findPosition(e.currentTarget.dataset.index);
            },
            redrawCanvas: function() {
                this.setUpCanvas();
                var e = this.data;
                e.showCanvas = !0;
                var t = e.selectedBgItem.originBgFrame, o = e.selectedBgItem.originPicFrame, n = e.selectedBgItem.originBgOffset, a = o[3] / o[2], r = 0, i = 0, s = o[2], p = o[3];
                a > e.userImageK ? (p = e.userImageHeight * o[2] / e.userImageWidth, i = (o[3] - p) / 2) : (s = e.userImageWidth * o[3] / e.userImageHeight, 
                r = (o[2] - s) / 2);
                var c = wx.createCanvasContext("myCanvas");
                c.clearRect(0, 0, t[2] + 2 * n[0], t[3] + 2 * n[1]), c.fillStyle = "#ffffff", c.fillRect(0, 0, t[2] + 2 * n[0], t[3] + 2 * n[1]), 
                c.fillStyle = "#000000", c.fillRect(n[0] + o[0], n[1] + o[1], o[2], o[3]), c.drawImage(e.userImage, n[0] + o[0] + r, n[1] + o[1] + i, s, p), 
                c.drawImage(e.selectedBgItem.selectedSrc, 0, 0, t[2], t[3], n[0], n[1], t[2], t[3]), 
                c.draw(!1, this.saveToAlbum);
            },
            cleanCanvas: function() {
                this.setData({
                    canvasHeight: 0,
                    canvasWidth: 0
                });
            },
            setUpCanvas: function() {
                var e = this.data.selectedBgItem, t = e.originBgFrame[2] + 2 * e.originBgOffset[0], o = e.originBgFrame[3] + 2 * e.originBgOffset[1];
                this.setData({
                    canvasHeight: o,
                    canvasWidth: t
                });
            },
            saveToAlbum: function() {
                var e = this;
                wx.canvasToTempFilePath({
                    quality: 1,
                    canvasId: "myCanvas",
                    success: function(t) {
                        e.cleanCanvas(), wx.authorize({
                            scope: "scope.writePhotosAlbum",
                            success: function() {
                                wx.saveImageToPhotosAlbum({
                                    filePath: t.tempFilePath,
                                    success: function(t) {
                                        e.showCanvas = !1, wx.navigateTo({
                                            url: "/pagesA/success/index"
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            },
            createPic: function() {
                wx.showLoading({
                    title: "制作中"
                });
                var e = this.data.selectedBgItem, t = e.selectedSrcIndex;
                if (e.srcs[t].isCDN) {
                    var o = this;
                    wx.getImageInfo({
                        src: e.srcs[t].src,
                        success: function(t) {
                            wx.saveFile({
                                tempFilePath: t.path,
                                success: function(t) {
                                    e.selectedSrc = t.savedFilePath, o.redrawCanvas();
                                }
                            });
                        }
                    });
                } else this.redrawCanvas();
            },
            findPosition: function(e) {
                var t = this.data.bgSrc[e], o = t.originBgFrame[2], n = t.originBgFrame[3], a = t.originPicFrame[2], r = t.originPicFrame[3], i = t.originPicFrame[0], s = t.originPicFrame[1], p = this.data.windowHeight - 220 / this.data.pixelRatio, c = (.6 * this.data.screenWidth - this.data.bgMarginLeft) / o, u = p / n;
                u < c && (c = u);
                var l = i * c - Math.floor(i * c), d = s * c - Math.floor(s * c), h = Math.round(a * c) - a * c, g = Math.round(r * c) - r * c;
                o = Math.round(o * c), n = Math.round(n * c), a = Math.round(a * c), r = Math.round(r * c), 
                i = Math.floor(i * c), s = Math.floor(s * c), l >= h && a++, d >= g && r++;
                var _ = (p - n) / 2;
                this.setData({
                    selectedBgItem: t,
                    bgWidth: o,
                    bgHeight: n,
                    picWidth: a,
                    picHeight: r,
                    marginLeft: i,
                    marginTop: s,
                    bgMarginTop: _
                });
            },
            radioChange: function(e) {
                var t = e.detail.value;
                if (t != this.data.selectedBgItem.selectedSrcIndex) {
                    for (var o = 0; o < this.data.selectedBgItem.srcs.length; ++o) {
                        var n = this.data.selectedBgItem.srcs[o];
                        o == t ? (n.checked = !0, this.data.selectedBgItem.selectedSrcIndex = t, this.data.selectedBgItem.selectedSrc = n.src) : n.checked = !1;
                    }
                    this.setData({
                        selectedBgItem: this.data.selectedBgItem
                    });
                }
            },
            onShareAppMessage: function() {
                return {
                    path: "/pagesA/album/index",
                    title: "推荐使用[截图带壳 Pro],潮人截图、壁纸分享必备小程序",
                    desc: "让您轻松的把截图套上手机壳，再分享截图、壁纸或者App给好友呢！",
                    imageUrl: "https://cdn.uiunion.cn/screenshot/images/images-share.png"
                };
            },
            onReady: function() {
                wx.createInterstitialAd && ((b = wx.createInterstitialAd({
                    adUnitId: "adunit-b9abf64b40dddf8d"
                })).onLoad(function() {
                    console.log("onLoad event emit");
                }), b.onError(function(e) {
                    console.log("onError event emit", e);
                }), b.onClose(function(e) {
                    console.log("onClose event emit", e);
                }));
            },
            onShow: function() {
                b && b.show().catch(function(e) {
                    console.error(e);
                });
            }
        });
    }), require("pagesA/pagesA/index/index.js"), __wxRoute = "pagesA/success/index", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/success/index.js", define("pagesA/success/index.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        var b = null;
        Page({
            data: {},
            goAlbum: function() {
                wx.navigateBack();
            },
            onLoad: function(e) {
                wx.createInterstitialAd && ((b = wx.createInterstitialAd({
                    adUnitId: "adunit-b9abf64b40dddf8d"
                })).onLoad(function() {
                    console.log("onLoad event emit");
                }), b.onError(function(e) {
                    console.log("onError event emit", e);
                }), b.onClose(function(e) {
                    console.log("onClose event emit", e);
                }));
            },
            onReady: function() {},
            onShow: function() {
                b.show().catch(function(e) {
                    console.error(e);
                });
            },
            onHide: function() {},
            onUnload: function() {},
            onPullDownRefresh: function() {},
            onReachBottom: function() {},
            onShareAppMessage: function() {
                return {
                    path: "/pagesA/album/index",
                    title: "推荐使用[截图带壳 Pro],潮人截图、壁纸分享必备小程序",
                    desc: "让您轻松的把截图套上手机壳，再分享截图、壁纸或者App给好友呢！",
                    imageUrl: "https://cdn.uiunion.cn/screenshot/images/images-share.png"
                };
            }
        });
    }), require("pagesA/pagesA/success/index.js"), __wxRoute = "pagesA/dache/index", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/dache/index.js", define("pagesA/dache/index.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        Page({
            data: {
                foodname: "今天打什么车呢？",
                fooditems: [ "呀,今天走路回去吧", "高德打车", "滴滴打车", "花小猪打车", "喝酒了，找个代驾" ],
                animate: !1,
                isclick: !1,
                dateMonth: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ][new Date().getMonth()],
                dateDay: new Date().getDate(),
                dateWeek: [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ][new Date().getDay()]
            },
            onLoad: function(e) {},
            onReady: function() {
                this.getTimeFood();
            },
            getTimeFood: function() {
                var e = this, t = new Date().getHours() + ":" + new Date().getMinutes();
                console.log(t), wx.request({
                    url: "https://res.guqule.com/time.json?v=" + new Date().getMinutes(),
                    method: "get",
                    data: {},
                    success: function(o) {
                        console.log(o.data), o.data.map(function(o, n) {
                            (e.equalDate(t, o.start_time) || e.compareDate(o.start_time, t)) && e.compareDate(t, o.end_time) && e.setData({
                                foodname: o.text,
                                fooditems: o.menus
                            });
                        }), "今天吃什么呢？" == e.foodname && (e.foodname = o.data[4].text, e.fooditems = o.data[4].menus);
                    }
                });
            },
            compareDate: function(e, t) {
                var o = new Date(), n = e.split(":"), a = t.split(":");
                return o.setHours(n[0], n[1]) < o.setHours(a[0], a[1]);
            },
            equalDate: function(e, t) {
                var o = new Date(), n = e.split(":"), a = t.split(":");
                return o.setHours(n[0], n[1]) == o.setHours(a[0], a[1]);
            },
            rolledUp: function() {
                console.log(this.data.animate), this.data.animate || (this.setData({
                    isclick: !0,
                    animate: !0
                }), this.bbb(100, 10, 0));
            },
            bbb: function(e, t, o) {
                var n = this;
                n.setData({
                    foodname: n.data.fooditems[Math.floor(Math.random() * n.data.fooditems.length)]
                }), n.timer = setInterval(function() {
                    n.setData({
                        foodname: n.data.fooditems[Math.floor(Math.random() * n.data.fooditems.length)]
                    }), clearInterval(n.timer), n.timer = null, t > o ? n.bbb(e, t, o + 1) : n.setData({
                        animate: !1
                    });
                }, e);
            },
            gotoeleOtherApp1: function() {
                wx.navigateToMiniProgram({
                    appId: "wxaf35009675aa0b2a",
                    path: "/pages/index/index?scene=mDg79Kq&source_id=5338jutuike20220301"
                });
            },
            gotoPinOtherApp2: function() {
                wx.navigateToMiniProgram({
                    appId: "wxbc0cf9b963bd3550",
                    path: "shareActivity/basic_activity/page/BasicActivityPop/BasicActivityPop?page_id=4k1Khw5X8wy&gd_from=outside_coupon_&pid=mm_1368340106_1991850209_111340900114&relationId=2816626157"
                });
            },
            gotoMeiOtherApp3: function() {
                wx.navigateToMiniProgram({
                    appId: "wxd98a20e429ce834b",
                    path: "/pages/chitu/index?scene=nRGDkak&source_id=5338jutuike20220301"
                });
            },
            gotoMeiOtherApp4: function() {
                wx.navigateToMiniProgram({
                    appId: "wx0d252f6ed9755862",
                    path: "/pages/router/index?scene=xmmRxdD&source_id=5338jutuike20220301"
                });
            },
            gotoMeiOtherApp5: function() {
                wx.navigateToMiniProgram({
                    appId: "wxaf35009675aa0b2a",
                    path: "/pages/index/index?scene=oWbwz9e&source_id=5338jutuike20220301"
                });
            },
            gotoMeiOtherApp6: function() {
                wx.navigateToMiniProgram({
                    appId: "wxaf35009675aa0b2a",
                    path: "/pages/index/index?scene=dKAYzgO&source_id=5338jutuike20220301"
                });
            },
            onShow: function() {},
            onHide: function() {},
            onUnload: function() {},
            onPullDownRefresh: function() {},
            onReachBottom: function() {},
            onShareAppMessage: function() {}
        });
    }), require("pagesA/pagesA/dache/index.js"), __wxRoute = "pagesA/photomark/photomark", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/photomark/photomark.js", define("pagesA/photomark/photomark.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        var b = null;
        Page({
            data: {
                photoPath: "",
                imageWidth: 0,
                imageHeight: 0,
                text: "此证件只用于办理XX业务，他用无效，阿九源码站",
                colors: [ "gray", "red", "white", "black", "orange", "yellow", "green", "blue", "purple", "darkcyan" ],
                selectedColor: "gray",
                alpha: 1,
                u8ad: {
                    adData: {},
                    ad: {
                        banner: "banner",
                        insert: "insert",
                        fixed: "fixed"
                    }
                }
            },
            onLoad: function(e) {
                wx.createInterstitialAd && ((b = wx.createInterstitialAd({
                    adUnitId: "adunit-b9abf64b40dddf8d"
                })).onLoad(function() {
                    b && b.show().catch(function(e) {
                        console.error(e);
                    });
                }), b.onError(function(e) {}), b.onClose(function() {})), getApp();
            },
            addPhotoTap: function(e) {
                var t = this;
                wx.chooseImage({
                    count: 1,
                    sizeType: [ "original" ],
                    sourceType: [ "album", "camera" ],
                    success: function(e) {
                        t.setData({
                            photoPath: e.tempFilePaths[0]
                        });
                    }
                });
            },
            textChange: function(e) {
                this.setData({
                    text: e.detail.value
                });
            },
            previewTap: function(e) {
                wx.previewImage({
                    current: this.data.photoPath,
                    urls: [ this.data.photoPath ]
                });
            },
            markTap: function(e) {
                this.data.photoPath ? this.data.text ? wx.navigateTo({
                    url: "/pagesA/photomarkresult/photomarkresult?photoPath=" + this.data.photoPath + "&text=" + this.data.text + "&color=" + this.data.selectedColor + "&alpha=" + this.data.alpha
                }) : wx.showToast({
                    title: "请输入水印文字",
                    icon: "error"
                }) : wx.showToast({
                    title: "请先选择图片",
                    icon: "error"
                });
            },
            colorTap: function(e) {
                this.setData({
                    selectedColor: e.currentTarget.dataset.color
                });
            },
            sliderChange: function(e) {
                this.setData({
                    alpha: (100 - e.detail.value) / 100
                });
            },
            onShareAppMessage: function() {
                return {
                    title: "这里可以免费给图片加水印",
                    path: "pagesA/photomark/photomark"
                };
            },
            onShareTimeline: function() {
                return {
                    title: "这里可以免费给图片加水印"
                };
            }
        });
    }), require("pagesA/pagesA/photomark/photomark.js"), __wxRoute = "pagesA/photomarkresult/photomarkresult", 
    __wxRouteBegin = !0, __wxAppCurrentFile__ = "pagesA/photomarkresult/photomarkresult.js", 
    define("pagesA/photomarkresult/photomarkresult.js", function(e, t, o, n, a, r, i, s, p, c, u, l, d, h, g, _, f, w, m, v, x) {
        "use strict";
        var b, y, A = "", O = "", P = "", k = 20, L = null;
        Page({
            data: {
                imageWidth: 0,
                imageHeight: 0
            },
            onLoad: function(e) {
                A = e.photoPath, O = e.text, P = e.color, b = e.alpha, this.mark(), wx.createInterstitialAd && ((L = wx.createInterstitialAd({
                    adUnitId: "adunit-b9abf64b40dddf8d"
                })).onLoad(function() {
                    L && L.show().catch(function(e) {
                        console.error(e);
                    });
                }), L.onError(function(e) {}), L.onClose(function() {}));
            },
            mark: function() {
                k = O.length > 5 ? 20 : 30, y = O.length * k;
                var e = this, t = wx.createCanvasContext("myCanvas");
                wx.getImageInfo({
                    src: A,
                    success: function(o) {
                        var n = o.width, a = o.height;
                        console.log("imgWidth=" + n), console.log("imgHeight=" + a);
                        var r = wx.getSystemInfoSync().windowWidth;
                        console.log("screenWidth=" + r);
                        var i = r, s = a / n * i;
                        console.log("canvasWidth=" + i), console.log("canvasHeight=" + s), e.setData({
                            imageWidth: i,
                            imageHeight: s
                        }), t.drawImage(o.path, 0, 0, i, s), t.rotate(30 * Math.PI / 180);
                        for (var p = 1; p < 30; p++) {
                            t.beginPath(), t.setGlobalAlpha(b), t.setFontSize(18), t.setFillStyle(P), t.fillText(O, 0, 80 * p);
                            for (var c = 1; c < 30; c++) t.beginPath(), t.setGlobalAlpha(b), t.setFontSize(18), 
                            t.setFillStyle(P), t.fillText(O, y * c, 80 * p);
                        }
                        for (var u = 0; u < 30; u++) {
                            t.beginPath(), t.setGlobalAlpha(b), t.setFontSize(18), t.setFillStyle(P), t.fillText(O, 0, -80 * u);
                            for (var l = 1; l < 30; l++) t.beginPath(), t.setGlobalAlpha(b), t.setFontSize(18), 
                            t.setFillStyle(P), t.fillText(O, y * l, -80 * u);
                        }
                        t.draw(!1, function() {
                            wx.canvasToTempFilePath({
                                x: 0,
                                y: 0,
                                width: n,
                                height: a,
                                destWidth: n,
                                destHeight: a,
                                canvasId: "myCanvas",
                                success: function(t) {
                                    wx.hideLoading(), e.addMarkPhotoPath = t.tempFilePath;
                                }
                            });
                        });
                    }
                });
            },
            saveTap: function(e) {
                wx.showLoading({
                    title: "保存中"
                }), wx.saveImageToPhotosAlbum({
                    filePath: this.addMarkPhotoPath,
                    success: function(e) {
                        wx.hideLoading(), wx.showModal({
                            content: "保存成功，请在相册中查看",
                            confirmText: "知道了",
                            showCancel: !1
                        });
                    }
                });
            },
            onShareAppMessage: function() {
                return {
                    title: "这里可以免费给图片加水印",
                    path: "pagesA/photomark/photomark"
                };
            }
        });
    }), require("pagesA/pagesA/photomarkresult/photomarkresult.js");
}();