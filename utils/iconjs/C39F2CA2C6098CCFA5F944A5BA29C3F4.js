var e, r, t, n;

e = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js"), r = e(require("546F4625C6098CCF32092E22607AC3F4.js")), 
t = e(require("1A41E661C6098CCF7C278E663FA9C3F4.js")), (n = require("cloudfunctions/test2/wx-server-sdk.js")).init(), 
exports.main = function() {
    var e = (0, t.default)(r.default.mark(function e(t, u) {
        var i;
        return r.default.wrap(function(e) {
            for (;;) switch (e.prev = e.next) {
              case 0:
                return i = n.getWXContext(), e.abrupt("return", {
                    event: t,
                    openid: i.OPENID,
                    appid: i.APPID,
                    unionid: i.UNIONID
                });

              case 2:
              case "end":
                return e.stop();
            }
        }, e);
    }));
    return function(r, t) {
        return e.apply(this, arguments);
    };
}();