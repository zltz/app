!function() {
    var o = "https://tx.zhuwapidan.com";
    function t(t, n, e, a, s, i, l) {
        e && wx.showToast({
            title: e,
            icon: a,
            mask: !0,
            duration: 3e4
        }), wx.request({
            url: o + t,
            data: n,
            method: s,
            header: {
                "Content-Type": "application/json"
            },
            success: function(o) {
                e && wx.hideToast(), o && (console.log(o), i(o));
            },
            fail: function(o) {
                console.log(o), l(o);
            }
        });
    }
    module.exports = {
        postRequest: function(o, n, e, a, s, i) {
            t(o, n, e, a, "POST", s, i);
        },
        getRequest: function(o, n, e, a, s, i) {
            t(o, n, e, a, "GET", s, i);
        },
        uploadImg: function(t, n, e, a, s, i, l, u) {
            !function(t, n, e, a, s, i, l, u, c) {
                s && wx.showToast({
                    title: s,
                    icon: i,
                    mask: !0,
                    duration: 3e4
                });
                var r = "?";
                for (var f in n) r += f + "=" + n[f] + "&";
                r = r.substr(0, r.length - 1).replace(/#/g, "%23"), console.log(r), wx.uploadFile({
                    url: o + t + r,
                    filePath: a,
                    name: e,
                    header: {
                        "Content-Type": "multipart/form-data"
                    },
                    formData: n,
                    success: function(o) {
                        s && wx.hideToast(), o && (console.log(o), u(o));
                    },
                    fail: function(o) {
                        console.log(o), c(o);
                    }
                });
            }(t, n, e, a, s, i, 0, l, u);
        }
    };
}();