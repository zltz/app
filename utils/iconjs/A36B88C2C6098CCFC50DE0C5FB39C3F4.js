!function() {
    var e = require("05BFE3A3C6098CCF63D98BA434E9C3F4.js")(require("035EDC83C6098CCF6538B484E06AC3F4.js"));
    function r(e) {
        return /(ht|f)tp(s?):\/\/([^ \\\/]*\.)+[^ \\\/]*(:[0-9]+)?\/?/.test(e);
    }
    function t(e) {
        return /data:image\/(\w+);base64,(.*)/.test(e);
    }
    module.exports = {
        isValidUrl: function(e) {
            return r(e) || t(e);
        },
        isOnlineUrl: r,
        isDataUrl: t,
        equal: function r(t, n) {
            if (t === n) return !0;
            if (t && n && "object" == (0, e.default)(t) && "object" == (0, e.default)(n)) {
                var i, u, a, f = Array.isArray(t), o = Array.isArray(n);
                if (f && o) {
                    if ((u = t.length) != n.length) return !1;
                    for (i = u; 0 != i--; ) if (!r(t[i], n[i])) return !1;
                    return !0;
                }
                if (f != o) return !1;
                var s = t instanceof Date, l = n instanceof Date;
                if (s != l) return !1;
                if (s && l) return t.getTime() == n.getTime();
                var c = t instanceof RegExp, g = n instanceof RegExp;
                if (c != g) return !1;
                if (c && g) return t.toString() == n.toString();
                var p = Object.keys(t);
                if ((u = p.length) !== Object.keys(n).length) return !1;
                for (i = u; 0 != i--; ) if (!Object.prototype.hasOwnProperty.call(n, p[i])) return !1;
                for (i = u; 0 != i--; ) if (!r(t[a = p[i]], n[a])) return !1;
                return !0;
            }
            return t != t && n != n;
        }
    };
}();