!function() {
    var t = require("../../utils/iconjs/75170127C6098CCF1371692032F9C3F4.js"), e = require("../../utils/iconjs/05BFE3A3C6098CCF63D98BA434E9C3F4.js"), i = e(require("../../utils/iconjs/210D2926C6098CCF476B4121DF3AC3F4.js")), n = e(require("../../utils/iconjs/546F4625C6098CCF32092E22607AC3F4.js")), s = e(require("../../utils/iconjs/1A41E661C6098CCF7C278E663FA9C3F4.js")), r = e(require("../../utils/iconjs/52A6C852C6098CCF34C0A0558E4AC3F4.js")), o = t(require("../../utils/iconjs/65A29296C6098CCF03C4FA9113CAC3F4.js")), a = e(require("../../utils/iconjs/914B0EE2C6098CCFF72D66E54289C3F4.js")), c = e(require("../../utils/iconjs/02EC8EB6C6098CCF648AE6B156AAC3F4.js"));
    function h(t) {
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
            if (Array.isArray(t) || (t = function(t, e) {
                if (t) {
                    if ("string" == typeof t) return u(t, e);
                    var i = Object.prototype.toString.call(t).slice(8, -1);
                    return "Object" === i && t.constructor && (i = t.constructor.name), "Map" === i || "Set" === i ? Array.from(i) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? u(t, e) : void 0;
                }
            }(t))) {
                var e = 0, i = function() {};
                return {
                    s: i,
                    n: function() {
                        return e >= t.length ? {
                            done: !0
                        } : {
                            done: !1,
                            value: t[e++]
                        };
                    },
                    e: function(t) {
                        throw t;
                    },
                    f: i
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var n, s, r = !0, o = !1;
        return {
            s: function() {
                n = t[Symbol.iterator]();
            },
            n: function() {
                var t = n.next();
                return r = t.done, t;
            },
            e: function(t) {
                o = !0, s = t;
            },
            f: function() {
                try {
                    r || null == n.return || n.return();
                } finally {
                    if (o) throw s;
                }
            }
        };
    }
    function u(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
    }
    var d = require("../../utils/iconjs/A36B88C2C6098CCFC50DE0C5FB39C3F4.js"), l = require("../../utils/iconjs/C8632D54C6098CCFAE0545539549C3F4.js"), p = new a.default();
    function f(t, e) {
        String.prototype.toPx = function(n, s) {
            if ("0" === this) return 0;
            var r = /-?[0-9]+(\.[0-9]+)?(rpx|px|%)/, a = function(i) {
                var n = new RegExp(r).exec(i);
                if (!i || !n) return console.error("The size: ".concat(i, " is illegal")), 0;
                var o = n[2], a = parseFloat(i), c = 0;
                return "rpx" === o ? c = Math.round(a * (t || .5) * (e || 1)) : "px" === o ? c = Math.round(a * (e || 1)) : "%" === o && (c = Math.round(a * s / 100)), 
                c;
            }, c = /^calc\((.+)\)$/.exec(this);
            if (c && c[1]) {
                var h = c[1].replace(/([^\s\(\+\-\*\/]+)\.(left|right|bottom|top|width|height)/g, function(t) {
                    var e = t.split("."), n = (0, i.default)(e, 2), s = n[0], r = n[1];
                    return o.penCache.viewRect[s][r];
                }).replace(new RegExp(r, "g"), a);
                return l(h);
            }
            return a(this);
        };
    }
    Component({
        canvasWidthInPx: 0,
        canvasHeightInPx: 0,
        canvasNode: null,
        paintCount: 0,
        currentPalette: {},
        outterDisabled: !1,
        isDisabled: !1,
        needClear: !1,
        properties: {
            use2D: {
                type: Boolean
            },
            customStyle: {
                type: String
            },
            customActionStyle: {
                type: Object
            },
            palette: {
                type: Object,
                observer: function(t, e) {
                    this.isNeedRefresh(t, e) && (this.paintCount = 0, (0, o.clearPenCache)(), this.startPaint());
                }
            },
            dancePalette: {
                type: Object,
                observer: function(t, e) {
                    this.isEmpty(t) || this.properties.use2D || ((0, o.clearPenCache)(), this.initDancePalette(t));
                }
            },
            scaleRatio: {
                type: Number,
                value: 1
            },
            widthPixels: {
                type: Number,
                value: 0
            },
            dirty: {
                type: Boolean,
                value: !1
            },
            LRU: {
                type: Boolean,
                value: !1
            },
            action: {
                type: Object,
                observer: function(t, e) {
                    !t || this.isEmpty(t) || this.properties.use2D || this.doAction(t, null, !1, !0);
                }
            },
            disableAction: {
                type: Boolean,
                observer: function(t) {
                    this.outterDisabled = t, this.isDisabled = t;
                }
            },
            clearActionBox: {
                type: Boolean,
                observer: function(t) {
                    var e = this;
                    t && !this.needClear && this.frontContext && (setTimeout(function() {
                        e.frontContext.draw();
                    }, 100), this.touchedView = {}, this.prevFindedIndex = this.findedIndex, this.findedIndex = -1), 
                    this.needClear = t;
                }
            }
        },
        data: {
            picURL: "",
            showCanvas: !0,
            painterStyle: ""
        },
        methods: {
            isEmpty: function(t) {
                for (var e in t) return !1;
                return !0;
            },
            isNeedRefresh: function(t, e) {
                return !(!t || this.isEmpty(t) || this.data.dirty && d.equal(t, e));
            },
            getBox: function(t, e) {
                var i = {
                    type: "rect",
                    css: {
                        height: "".concat(t.bottom - t.top, "px"),
                        width: "".concat(t.right - t.left, "px"),
                        left: "".concat(t.left, "px"),
                        top: "".concat(t.top, "px"),
                        borderWidth: "4rpx",
                        borderColor: "#1A7AF8",
                        color: "transparent"
                    }
                };
                return "text" === e && (i.css = Object.assign({}, i.css, {
                    borderStyle: "dashed"
                })), this.properties.customActionStyle && this.properties.customActionStyle.border && (i.css = Object.assign({}, i.css, this.properties.customActionStyle.border)), 
                Object.assign(i, {
                    id: "box"
                }), i;
            },
            getScaleIcon: function(t, e) {
                var i = {}, n = this.properties.customActionStyle;
                return (i = n && n.scale ? {
                    type: "image",
                    url: "text" === e ? n.scale.textIcon : n.scale.imageIcon,
                    css: {
                        height: "".concat(48, "rpx"),
                        width: "".concat(48, "rpx"),
                        borderRadius: "".concat(24, "rpx")
                    }
                } : {
                    type: "rect",
                    css: {
                        height: "".concat(48, "rpx"),
                        width: "".concat(48, "rpx"),
                        borderRadius: "".concat(24, "rpx"),
                        color: "#0000ff"
                    }
                }).css = Object.assign({}, i.css, {
                    align: "center",
                    left: "".concat(t.right + "2rpx".toPx(), "px"),
                    top: "".concat("text" === e ? t.top - "2rpx".toPx() - i.css.height.toPx() / 2 : t.bottom - "2rpx".toPx() - i.css.height.toPx() / 2, "px")
                }), Object.assign(i, {
                    id: "scale"
                }), i;
            },
            getDeleteIcon: function(t) {
                var e = {}, i = this.properties.customActionStyle;
                return (e = i && i.scale ? {
                    type: "image",
                    url: i.delete.icon,
                    css: {
                        height: "".concat(48, "rpx"),
                        width: "".concat(48, "rpx"),
                        borderRadius: "".concat(24, "rpx")
                    }
                } : {
                    type: "rect",
                    css: {
                        height: "".concat(48, "rpx"),
                        width: "".concat(48, "rpx"),
                        borderRadius: "".concat(24, "rpx"),
                        color: "#0000ff"
                    }
                }).css = Object.assign({}, e.css, {
                    align: "center",
                    left: "".concat(t.left - "2rpx".toPx(), "px"),
                    top: "".concat(t.top - "2rpx".toPx() - e.css.height.toPx() / 2, "px")
                }), Object.assign(e, {
                    id: "delete"
                }), e;
            },
            doAction: function(t, e, i, n) {
                var s = this;
                if (!this.properties.use2D) {
                    var o = null;
                    if (t && (o = t.view), o && o.id && this.touchedView.id !== o.id) for (var a = this.currentPalette.views, c = 0; c < a.length; c++) if (a[c].id === o.id) {
                        this.touchedView = a[c], this.findedIndex = c, this.sliceLayers();
                        break;
                    }
                    var h = this.touchedView;
                    h && !this.isEmpty(h) && (o && o.css && (n ? h.css = o.css : Array.isArray(h.css) && Array.isArray(o.css) ? h.css = Object.assign.apply(Object, [ {} ].concat((0, 
                    r.default)(h.css), (0, r.default)(o.css))) : Array.isArray(h.css) ? h.css = Object.assign.apply(Object, [ {} ].concat((0, 
                    r.default)(h.css), [ o.css ])) : Array.isArray(o.css) ? h.css = Object.assign.apply(Object, [ {}, h.css ].concat((0, 
                    r.default)(o.css))) : h.css = Object.assign({}, h.css, o.css)), o && o.rect && (h.rect = o.rect), 
                    o && o.url && h.url && o.url !== h.url ? p.download(o.url, this.properties.LRU).then(function(t) {
                        o.url.startsWith("https") && (h.originUrl = o.url), h.url = t, wx.getImageInfo({
                            src: t,
                            success: function(t) {
                                h.sHeight = t.height, h.sWidth = t.width, s.reDraw(h, e, i);
                            },
                            fail: function() {
                                s.reDraw(h, e, i);
                            }
                        });
                    }).catch(function(t) {
                        console.error(t), s.reDraw(h, e, i);
                    }) : (o && o.text && h.text && o.text !== h.text && (h.text = o.text), o && o.content && h.content && o.content !== h.content && (h.content = o.content), 
                    this.reDraw(h, e, i)));
                }
            },
            reDraw: function(t, e, i) {
                var n = this, s = {
                    width: this.currentPalette.width,
                    height: this.currentPalette.height,
                    views: this.isEmpty(t) ? [] : [ t ]
                };
                new o.default(this.globalContext, s).paint(function(t) {
                    e && e(t), n.triggerEvent("viewUpdate", {
                        view: n.touchedView
                    });
                });
                var r = t.rect, a = t.css, c = t.type;
                this.block = {
                    width: this.currentPalette.width,
                    height: this.currentPalette.height,
                    views: this.isEmpty(t) ? [] : [ this.getBox(r, t.type) ]
                }, a && a.scalable && this.block.views.push(this.getScaleIcon(r, c)), a && a.deletable && this.block.views.push(this.getDeleteIcon(r)), 
                new o.default(this.frontContext, this.block).paint();
            },
            isInView: function(t, e, i) {
                return t > i.left && e > i.top && t < i.right && e < i.bottom;
            },
            isInDelete: function(t, e) {
                var i, n = h(this.block.views);
                try {
                    for (n.s(); !(i = n.n()).done; ) {
                        var s = i.value;
                        if ("delete" === s.id) return t > s.rect.left && e > s.rect.top && t < s.rect.right && e < s.rect.bottom;
                    }
                } catch (t) {
                    n.e(t);
                } finally {
                    n.f();
                }
                return !1;
            },
            isInScale: function(t, e) {
                var i, n = h(this.block.views);
                try {
                    for (n.s(); !(i = n.n()).done; ) {
                        var s = i.value;
                        if ("scale" === s.id) return t > s.rect.left && e > s.rect.top && t < s.rect.right && e < s.rect.bottom;
                    }
                } catch (t) {
                    n.e(t);
                } finally {
                    n.f();
                }
                return !1;
            },
            touchedView: {},
            findedIndex: -1,
            onClick: function() {
                for (var t = this.startX, e = this.startY, i = [], n = !1, s = -1, r = this.currentPalette.views.length - 1; r >= 0; r--) {
                    var o = this.currentPalette.views[r], a = o.rect;
                    if (this.touchedView && this.touchedView.id && this.touchedView.id === o.id && this.isInDelete(t, e, a)) {
                        i.length = 0, s = r, n = !0;
                        break;
                    }
                    this.isInView(t, e, a) && i.push({
                        view: o,
                        index: r
                    });
                }
                if (this.touchedView = {}, 0 === i.length) this.findedIndex = -1; else {
                    var c = 0, h = i.filter(function(t) {
                        return Boolean(t.view.id);
                    });
                    if (0 === h.length) this.findedIndex = i[0].index; else {
                        for (c = 0; c < h.length; c++) if (this.findedIndex === h[c].index) {
                            c++;
                            break;
                        }
                        c === h.length && (c = 0), this.touchedView = h[c].view, this.findedIndex = h[c].index, 
                        this.triggerEvent("viewClicked", {
                            view: this.touchedView
                        });
                    }
                }
                this.findedIndex < 0 || this.touchedView && !this.touchedView.id ? (this.frontContext.draw(), 
                n ? (this.triggerEvent("touchEnd", {
                    view: this.currentPalette.views[s],
                    index: s,
                    type: "delete"
                }), this.doAction()) : this.findedIndex < 0 && this.triggerEvent("viewClicked", {}), 
                this.findedIndex = -1, this.prevFindedIndex = -1) : this.touchedView && this.touchedView.id && this.sliceLayers();
            },
            sliceLayers: function() {
                var t = this.currentPalette.views.slice(0, this.findedIndex), e = this.currentPalette.views.slice(this.findedIndex + 1), i = {
                    width: this.currentPalette.width,
                    height: this.currentPalette.height,
                    background: this.currentPalette.background,
                    views: t
                }, n = {
                    width: this.currentPalette.width,
                    height: this.currentPalette.height,
                    views: e
                };
                this.prevFindedIndex < this.findedIndex ? (new o.default(this.bottomContext, i).paint(), 
                this.doAction(), new o.default(this.topContext, n).paint()) : (new o.default(this.topContext, n).paint(), 
                this.doAction(), new o.default(this.bottomContext, i).paint()), this.prevFindedIndex = this.findedIndex;
            },
            startX: 0,
            startY: 0,
            startH: 0,
            startW: 0,
            isScale: !1,
            startTimeStamp: 0,
            onTouchStart: function(t) {
                if (!this.isDisabled) {
                    var e = t.touches[0], i = e.x, n = e.y;
                    if (this.startX = i, this.startY = n, this.startTimeStamp = new Date().getTime(), 
                    this.touchedView && !this.isEmpty(this.touchedView)) {
                        var s = this.touchedView.rect;
                        this.isInScale(i, n, s) ? (this.isScale = !0, this.startH = s.bottom - s.top, this.startW = s.right - s.left) : this.isScale = !1;
                    } else this.isScale = !1;
                }
            },
            onTouchEnd: function(t) {
                this.isDisabled || (new Date().getTime() - this.startTimeStamp <= 500 && !this.hasMove ? !this.isScale && this.onClick(t) : this.touchedView && !this.isEmpty(this.touchedView) && this.triggerEvent("touchEnd", {
                    view: this.touchedView
                }), this.hasMove = !1);
            },
            onTouchCancel: function(t) {
                this.isDisabled || this.onTouchEnd(t);
            },
            hasMove: !1,
            onTouchMove: function(t) {
                if (!this.isDisabled && (this.hasMove = !0, this.touchedView && (!this.touchedView || this.touchedView.id))) {
                    var e = t.touches[0], i = e.x, n = e.y, s = i - this.startX, r = n - this.startY, a = this.touchedView, c = a.rect, h = a.type, u = {};
                    if (this.isScale) {
                        (0, o.clearPenCache)(this.touchedView.id);
                        var d = this.startW + s > 1 ? this.startW + s : 1;
                        if (this.touchedView.css && this.touchedView.css.minWidth && d < this.touchedView.css.minWidth.toPx()) return;
                        if (this.touchedView.rect && this.touchedView.rect.minWidth && d < this.touchedView.rect.minWidth) return;
                        var l = this.startH + r > 1 ? this.startH + r : 1;
                        u = {
                            width: "".concat(d, "px")
                        }, "text" !== h && (u.height = "".concat("image" === h ? d * this.startH / this.startW : l, "px"));
                    } else this.startX = i, this.startY = n, u = {
                        left: "".concat(c.x + s, "px"),
                        top: "".concat(c.y + r, "px"),
                        right: void 0,
                        bottom: void 0
                    };
                    this.doAction({
                        view: {
                            css: u
                        }
                    }, null, !this.isScale);
                }
            },
            initScreenK: function() {
                if (!(getApp() && getApp().systemInfo && getApp().systemInfo.screenWidth)) try {
                    getApp().systemInfo = wx.getSystemInfoSync();
                } catch (t) {
                    return void console.error("Painter get system info failed, ".concat(JSON.stringify(t)));
                }
                this.screenK = .5, getApp() && getApp().systemInfo && getApp().systemInfo.screenWidth && (this.screenK = getApp().systemInfo.screenWidth / 750), 
                f(this.screenK, this.properties.scaleRatio);
            },
            initDancePalette: function() {
                var t = this;
                this.properties.use2D || (this.isDisabled = !0, this.initScreenK(), this.downloadImages(this.properties.dancePalette).then(function() {
                    var e = (0, s.default)(n.default.mark(function e(i) {
                        var s, r;
                        return n.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                if (t.currentPalette = i, s = i.width, r = i.height, s && r) {
                                    e.next = 5;
                                    break;
                                }
                                return console.error("You should set width and height correctly for painter, width: ".concat(s, ", height: ").concat(r)), 
                                e.abrupt("return");

                              case 5:
                                if (t.setData({
                                    painterStyle: "width:".concat(s.toPx(), "px;height:").concat(r.toPx(), "px;")
                                }), e.t0 = t.frontContext, e.t0) {
                                    e.next = 11;
                                    break;
                                }
                                return e.next = 10, t.getCanvasContext(t.properties.use2D, "front");

                              case 10:
                                t.frontContext = e.sent;

                              case 11:
                                if (e.t1 = t.bottomContext, e.t1) {
                                    e.next = 16;
                                    break;
                                }
                                return e.next = 15, t.getCanvasContext(t.properties.use2D, "bottom");

                              case 15:
                                t.bottomContext = e.sent;

                              case 16:
                                if (e.t2 = t.topContext, e.t2) {
                                    e.next = 21;
                                    break;
                                }
                                return e.next = 20, t.getCanvasContext(t.properties.use2D, "top");

                              case 20:
                                t.topContext = e.sent;

                              case 21:
                                if (e.t3 = t.globalContext, e.t3) {
                                    e.next = 26;
                                    break;
                                }
                                return e.next = 25, t.getCanvasContext(t.properties.use2D, "k-canvas");

                              case 25:
                                t.globalContext = e.sent;

                              case 26:
                                new o.default(t.bottomContext, i, t.properties.use2D).paint(function() {
                                    t.isDisabled = !1, t.isDisabled = t.outterDisabled, t.triggerEvent("didShow");
                                }), t.globalContext.draw(), t.frontContext.draw(), t.topContext.draw();

                              case 30:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }));
                    return function(t) {
                        return e.apply(this, arguments);
                    };
                }()), this.touchedView = {});
            },
            startPaint: function() {
                this.initScreenK();
                var t = this.properties.palette, e = t.width, i = t.height;
                if (e && i) {
                    var r = !1;
                    e.toPx() !== this.canvasWidthInPx && (this.canvasWidthInPx = e.toPx(), r = this.properties.use2D), 
                    this.properties.widthPixels && (f(this.screenK, this.properties.widthPixels / this.canvasWidthInPx), 
                    this.canvasWidthInPx = this.properties.widthPixels), this.canvasHeightInPx !== i.toPx() && (this.canvasHeightInPx = i.toPx(), 
                    r = r || this.properties.use2D), this.setData({
                        photoStyle: "width:".concat(this.canvasWidthInPx, "px;height:").concat(this.canvasHeightInPx, "px;")
                    }, function() {
                        var t = this;
                        this.downloadImages(this.properties.palette).then(function() {
                            var e = (0, s.default)(n.default.mark(function e(i) {
                                var s;
                                return n.default.wrap(function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                      case 0:
                                        if (t.photoContext) {
                                            e.next = 4;
                                            break;
                                        }
                                        return e.next = 3, t.getCanvasContext(t.properties.use2D, "photo");

                                      case 3:
                                        t.photoContext = e.sent;

                                      case 4:
                                        r && (s = getApp().systemInfo.pixelRatio, t.photoContext.width = t.canvasWidthInPx * s, 
                                        t.photoContext.height = t.canvasHeightInPx * s, t.photoContext.scale(s, s)), new o.default(t.photoContext, i).paint(function() {
                                            t.saveImgToLocal();
                                        }), f(t.screenK, t.properties.scaleRatio);

                                      case 7:
                                      case "end":
                                        return e.stop();
                                    }
                                }, e);
                            }));
                            return function(t) {
                                return e.apply(this, arguments);
                            };
                        }());
                    });
                } else console.error("You should set width and height correctly for painter, width: ".concat(e, ", height: ").concat(i));
            },
            downloadImages: function(t) {
                var e = this;
                return new Promise(function(i, n) {
                    var s = 0, r = 0, o = JSON.parse(JSON.stringify(t));
                    if (o.background && (s++, p.download(o.background, e.properties.LRU).then(function(t) {
                        o.background = t, s === ++r && i(o);
                    }, function() {
                        s === ++r && i(o);
                    })), o.views) {
                        var a, c = h(o.views);
                        try {
                            var u = function() {
                                var t = a.value;
                                t && "image" === t.type && t.url && (s++, p.download(t.url, e.properties.LRU).then(function(e) {
                                    t.originUrl = t.url, t.url = e, wx.getImageInfo({
                                        src: e,
                                        success: function(e) {
                                            t.sWidth = e.width, t.sHeight = e.height;
                                        },
                                        fail: function(e) {
                                            console.warn("getImageInfo ".concat(t.originUrl, " failed, ").concat(JSON.stringify(e))), 
                                            t.url = "";
                                        },
                                        complete: function() {
                                            s === ++r && i(o);
                                        }
                                    });
                                }, function() {
                                    s === ++r && i(o);
                                }));
                            };
                            for (c.s(); !(a = c.n()).done; ) u();
                        } catch (t) {
                            c.e(t);
                        } finally {
                            c.f();
                        }
                    }
                    0 === s && i(o);
                });
            },
            saveImgToLocal: function() {
                var t = this, e = this;
                setTimeout(function() {
                    wx.canvasToTempFilePath({
                        canvasId: "photo",
                        canvas: e.properties.use2D ? e.canvasNode : null,
                        destWidth: e.canvasWidthInPx,
                        destHeight: e.canvasHeightInPx,
                        success: function(t) {
                            e.getImageInfo(t.tempFilePath);
                        },
                        fail: function(t) {
                            console.error("canvasToTempFilePath failed, ".concat(JSON.stringify(t))), e.triggerEvent("imgErr", {
                                error: t
                            });
                        }
                    }, t);
                }, 300);
            },
            getCanvasContext: function(t, e) {
                var i = this;
                return new Promise(function(n) {
                    if (t) {
                        var s = wx.createSelectorQuery().in(i), r = "#".concat(e);
                        s.select(r).fields({
                            node: !0,
                            size: !0
                        }).exec(function(t) {
                            i.canvasNode = t[0].node;
                            var s = i.canvasNode.getContext("2d"), r = new c.default("2d", s, e, !0, i.canvasNode);
                            n(r);
                        });
                    } else {
                        var o = wx.createCanvasContext(e, i);
                        n(new c.default("mina", o, e, !0));
                    }
                });
            },
            getImageInfo: function(t) {
                var e = this;
                wx.getImageInfo({
                    src: t,
                    success: function(i) {
                        if (e.paintCount > 5) {
                            var n = "The result is always fault, even we tried ".concat(5, " times");
                            return console.error(n), void e.triggerEvent("imgErr", {
                                error: n
                            });
                        }
                        Math.abs((i.width * e.canvasHeightInPx - e.canvasWidthInPx * i.height) / (i.height * e.canvasHeightInPx)) < .01 ? e.triggerEvent("imgOK", {
                            path: t
                        }) : e.startPaint(), e.paintCount++;
                    },
                    fail: function(t) {
                        console.error("getImageInfo failed, ".concat(JSON.stringify(t))), e.triggerEvent("imgErr", {
                            error: t
                        });
                    }
                });
            }
        }
    });
}();