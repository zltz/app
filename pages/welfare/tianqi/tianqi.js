const app = getApp();
Component({
  data: {
    icon: '',
    name: '1',
    province: '',
    location_city: '',
    // get_time: '',
    cue_word: '',
    tmp: '',
    cond_txt: '',
    wind_dir: '',
    wind_sc: '',
    fl: '',
    tmp_max: '',
    tmp_min: '',
    cond_code_d: '',
    cond_txt_d: '',
    tmp_max_m: '',
    tmp_min_m: '',
    cond_code_d_m: '',
    cond_txt_d_m: '',
    hum: '',
    cloud: '',
    pres: '',
    vis: '',
    wind_deg: '',
    pcpn: '',
    wind_spd: '',
    future: [],
    lifeindex: [],
    showModal: true, // 显示modal弹窗
    single: true// true 只显示一个按钮，如果想显示两个改为false即可
  },
  lifetimes: {
    created: function(){
      var options={};
      this.weiZhi(options);
    }
  },
  onPullDownRefresh: function () {
    var that = this;
    var options = {};
    that.weiZhi(options);
    wx.stopPullDownRefresh();
  },
  methods: {
//获取用户位置（经纬度）
weiZhi: function (options) {
  var truedata = JSON.stringify(options);
  var that = this;
  truedata == '{}';
  if (truedata == '{}') {
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        var latitude = res.latitude
        var longitude = res.longitude
        wx.setStorageSync("latitude", latitude);
        wx.setStorageSync("longitude", longitude);
        that.getUserCity(latitude, longitude);
      },
    })
  } else {
    var latitude = options.lat;
    var longitude = options.lng;
    wx.setStorageSync("latitude", latitude);
    wx.setStorageSync("longitude", longitude);
    that.getUserCity(latitude, longitude);
  }
},

//获取用户地理位置（城市）
getUserCity: function (latitude, longitude) {
  wx.showLoading({
    title: '加载中...',
    mask: true,
  });
  var that = this;
  // var lat = wx.getStorageSync('latitude');
  // var longit = wx.getStorageSync('longitude');
  wx.request({
    method: "POST",
    url: app.globalData.apiUrl + '/GetCityName', 
    data: {
      latitude: latitude,
      longitude: longitude,
    },
    header: {
      'content-type': 'application/json' // 默认值
    },
    success(res) {
      if (res.data.code == 1) {
        that.setData({
          location_city: res.data.data.location_city,
          province: res.data.data.province,
          // get_time: res.data.data.gettime,
          cue_word: res.data.data.cue_word,
        });
        that.getWeatherList(latitude, longitude);
      } else {
        wx.hideLoading();
        wx.showToast({
          title: '请下拉刷新或重新进入小程序',
          icon: 'none',
          duration: 5000
        });
      }
    }
  })
},

//获取用户天气集合预报
getWeatherList: function (latitude, longitude) {
  var that = this;
  // var latitudes = wx.getStorageSync('latitude');
  // var longitudes = wx.getStorageSync('longitude');
  wx.request({
    method: "POST",
    url: app.globalData.apiUrl + '/WeatherGather',
    data: {
      latitude: latitude,
      longitude: longitude,
    },
    header: {
      'content-type': 'application/json' // 默认值
    },
    success(res) {
      // console.log(res.data)
      if (res.data.code == 1) {
        // console.log(1)
        that.setData({
          tmp: res.data.data.tmp,
          cond_txt: res.data.data.cond_txt,
          wind_dir: res.data.data.wind_dir,
          wind_sc: res.data.data.wind_sc,
          hum: res.data.data.hum,
          cloud: res.data.data.cloud,
          pres: res.data.data.pres,
          vis: res.data.data.vis,
          wind_deg: res.data.data.wind_deg,
          pcpn: res.data.data.pcpn,
          wind_spd: res.data.data.wind_spd,
          fl: res.data.data.fl,
          tmp_max: res.data.weather[0].tmp_max,
          tmp_min: res.data.weather[0].tmp_min,
          cond_txt_d: res.data.weather[0].cond_txt_d,
          cond_code_d: res.data.weather[0].cond_code_d,
          tmp_max_m: res.data.weather[1].tmp_max,
          tmp_min_m: res.data.weather[1].tmp_min,
          cond_txt_d_m: res.data.weather[1].cond_txt_d,
          cond_code_d_m: res.data.weather[1].cond_code_d,
          future: res.data.weather,
          lifeindex: res.data.lifeindex,
        });
        wx.hideLoading();
      } else {
        wx.hideLoading();
        wx.showToast({
          title: '请下拉刷新或重新进入小程序',
          icon: 'none',
          duration: 5000
        });
      }
    }
  })
},

// 点击生活指数
click_button: function (e) {
  // console.log(e);
  wx.showModal({
    title: '温馨提示',
    content: e.currentTarget.dataset.name,
    showCancel: false,
    confirmText: '我知道了',
    confirmColor: '#6666FF',
  });
},

// 定位
search: function () {
  wx.navigateTo({
    url: '../../../components/search/search',
  })
},
  }
})