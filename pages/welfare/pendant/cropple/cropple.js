// pages/txgj/cropple/cropple.js
var t, a, e, i, o;
a = require("../../../../utils/iconjs/05BFE3A3C6098CCF63D98BA434E9C3F4.js");
e = a(require("../../../../utils/iconjs/E2BD7757C6098CCF84DB1F50F2D9C3F4.js"));
i = a(require("../../../../utils/iconjs/9D53CC23C6098CCFFB35A4246909C3F4.js"));
o = getApp();
Page({
  data: (t = {
    src: "",
    width: 300,
    height: 300,
    max_width: 1e3,
    max_height: 1e3,
    disable_rotate: !0,
    disable_ratio: !1,
    limit_move: !0,
    show: !1,
    showModalStatus: !1,
    animationData: "",
    info: !1
}, (0, e.default)(t, "animationData", ""), (0, e.default)(t, "showModalDlg", !1), 
(0, e.default)(t, "showModalStatusInfo", !1), (0, e.default)(t, "filtTmpUrlPath", ""), 
(0, e.default)(t, "mengShow", !1), (0, e.default)(t, "export_scale", 1), (0, e.default)(t, "type", ""), 
(0, e.default)(t, "fid", ""), t),
onLoad: function(t) {
    console.log(t), "" != t.type ? (this.data.type = t.type, this.data.fid = t.id, this.cropper = this.selectComponent("#image-cropper")) : wx.reLaunch({
        url: "/pages/home/home"
    });
},
powerDrawer: function() {
    this.setData({
        showModalStatusInfo: !1,
        info: !this.data.info
    });
},
uploadimg: function(t) {
    wx.vibrateShort();
    var a = this;
    wx.chooseImage({
        count: 1,
        sizeType: [ "compressed", "original" ],
        sourceType: [ "album" ],
        success: function(t) {
            console.log(t);
            var e = t.tempFiles[0];
            if (e.size > 3072e3) wx.showToast({
                title: "制作失败，图片超过3M",
                icon: "none",
                duration: 3e3
            }); else {
                console.log(e.path), a.setData({
                    filtTmpUrlPath: e.path
                }), wx.showToast({
                    title: "制作中...",
                    icon: "loading",
                    mask: !0,
                    duration: 3e4
                });
                var o = e.path;
                i.default.uploadImg("/api/Image/imgCheck", {
                    access_token: wx.getStorageSync("access_token")
                }, "file", o, "上传中...", "loading", function(t) {
                    var e = JSON.parse(t.data);
                    console.log(e), 200 == e.status ? (console.log("图片正常" + o), wx.setStorageSync("filePath", o), 
                    a.cropper.imgReset(), a.setData({
                        src: o
                    })) : (e.status, wx.showToast({
                        title: e.info,
                        icon: "none",
                        duration: 3e3
                    }));
                }, function(t) {
                    wx.showToast({
                        title: "请求失败",
                        icon: "none",
                        duration: 3e3
                    });
                });
            }
        },
        fail: function(t) {
            console.error(t);
        }
    });
},
handleTouchStart: function(t) {
    var a = t.currentTarget.dataset.statu;
    this.util(a), this.setData({
        info: !this.data.info
    });
},
showInfo: function(t) {
    wx.vibrateShort();
    var a = t.currentTarget.dataset.statu;
    this.util(a), this.setData({
        info: !this.data.info
    });
},
handleTouchEnd: function(t) {
    var a = t.currentTarget.dataset.statu;
    this.util(a), this.setData({
        info: !this.data.info
    });
},
util: function(t) {
    var a = wx.createAnimation({
        duration: 300,
        timingFunction: "linear",
        delay: 0
    });
    this.animation = a, a.opacity(0).rotateY(100).step(), this.setData({
        animationData: a.export()
    }), setTimeout(function() {
        a.opacity(1).rotateY(0).step(), this.setData({
            animationData: a
        }), "close" == t && this.setData({
            showModalStatusInfo: !1
        });
    }.bind(this), 200), "open" == t && this.setData({
        showModalStatusInfo: !0
    });
},
showPopup: function() {
    this.setData({
        show: !0
    });
},
onClose: function() {
    this.setData({
        show: !1
    });
},
showModal: function() {
    var t = wx.createAnimation({
        duration: 200,
        timingFunction: "ease-in-out",
        delay: 0
    });
    this.animation = t, t.translateY(500).step(), this.setData({
        animationData: t.export(),
        showModalStatus: !0
    }), setTimeout(function() {
        t.translateY(0).step(), this.setData({
            animationData: t.export()
        });
    }.bind(this), 200);
},
hideModal: function() {
    this.setData({
        showModalStatus: !1
    });
},
onShow: function() {},
cropperload: function(t) {
    console.log("cropper加载完成");
},
loadimage: function(t) {
    wx.hideLoading(), console.log("图片"), this.cropper.imgReset();
},
clickcut: function(t) {
    console.log(t.detail), wx.previewImage({
        current: t.detail.url,
        urls: [ t.detail.url ]
    });
},
upload: function() {
    var t = this;
    wx.chooseImage({
        count: 1,
        sizeType: [ "original", "compressed" ],
        sourceType: [ "album" ],
        success: function(a) {
            wx.showLoading({
                title: "加载中"
            });
            var e = a.tempFilePaths[0];
            t.cropper.imgReset(), t.setData({
                src: e
            });
        }
    });
},
setWidth: function(t) {
    this.setData({
        width: t.detail.value < 10 ? 10 : t.detail.value
    }), this.setData({
        cut_left: this.cropper.data.cut_left
    });
},
setHeight: function(t) {
    this.setData({
        height: t.detail.value < 10 ? 10 : t.detail.value
    }), this.setData({
        cut_top: this.cropper.data.cut_top
    });
},
switchChangeDisableRatio: function(t) {
    this.setData({
        disable_ratio: t.detail.value
    });
},
setCutTop: function(t) {
    this.setData({
        cut_top: t.detail.value
    }), this.setData({
        cut_top: this.cropper.data.cut_top
    });
},
setCutLeft: function(t) {
    this.setData({
        cut_left: t.detail.value
    }), this.setData({
        cut_left: this.cropper.data.cut_left
    });
},
switchChangeDisableRotate: function(t) {
    t.detail.value ? this.setData({
        disable_rotate: t.detail.value
    }) : this.setData({
        limit_move: !1,
        disable_rotate: t.detail.value
    });
},
switchChangeLimitMove: function(t) {
    t.detail.value && this.setData({
        disable_rotate: !0
    }), this.cropper.setLimitMove(t.detail.value);
},
switchChangeDisableWidth: function(t) {
    this.setData({
        disable_width: t.detail.value
    });
},
switchChangeDisableHeight: function(t) {
    this.setData({
        disable_height: t.detail.value
    });
},
submit: function() {
    var t = this;
    this.data.src ? this.cropper.getImg(function(a) {
        console.log(a.url), o.globalData.imgSrc = a.url, wx.setStorageSync("avatarUrlHD", a.url), 
        wx.setStorageSync("avatarUrlHDTmp", a.url);
        var e = "";
        1 == t.data.type && (e = "../detail/detail"), 2 == t.data.type && (e = "/pages/gua/gua"), 
        wx.navigateTo({
            url: e + "?id=" + t.data.fid
        });
    }) : wx.showToast({
        title: "请点击上传照片",
        icon: "none",
        mask: !0,
        duration: 500
    });
},
rotate: function() {
    this.cropper.setAngle(this.cropper.data.angle += 90);
},
top: function() {
    var t = this;
    this.data.top = setInterval(function() {
        t.cropper.setTransform({
            y: -3
        });
    }, 1e3 / 60);
},
bottom: function() {
    var t = this;
    this.data.bottom = setInterval(function() {
        t.cropper.setTransform({
            y: 3
        });
    }, 1e3 / 60);
},
left: function() {
    var t = this;
    this.data.left = setInterval(function() {
        t.cropper.setTransform({
            x: -3
        });
    }, 1e3 / 60);
},
right: function() {
    var t = this;
    this.data.right = setInterval(function() {
        t.cropper.setTransform({
            x: 3
        });
    }, 1e3 / 60);
},
narrow: function() {
    var t = this;
    this.data.narrow = setInterval(function() {
        t.cropper.setTransform({
            scale: -.02
        });
    }, 1e3 / 60);
},
enlarge: function() {
    var t = this;
    this.data.enlarge = setInterval(function() {
        t.cropper.setTransform({
            scale: .02
        });
    }, 1e3 / 60);
},
end: function(t) {
    clearInterval(this.data[t.currentTarget.dataset.type]);
}
});