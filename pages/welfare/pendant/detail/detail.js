var t, a;
t = require("../../../../utils/iconjs/05BFE3A3C6098CCF63D98BA434E9C3F4.js")(require("../../../../utils/iconjs/9D53CC23C6098CCFFB35A4246909C3F4.js"));
const request = require('../../../../utils/request.js');
const app = getApp();
Page({
    data: {
        statusBarHeight: 0,
        uploadImg: "https://file.tlzcf.vip/moren.png",
        isIphoneX: !1,
        cate: "hot",
        ColorList: app.globalData.ColorList,
        CustomBar: "",
        scrollHeight: "",
        imgData: [],
        isSucaiStatus: !0,
        SucaiImgTmp: "../../images/tm.jpg",
        selSucaiIndex: -1,
        isLogin: !1,
        typeArr: [],
        imgArrList: [],
        TabCur: 0,
        scrollLeft: 0,
        modalName: !1,
        SucaiImg: "",
        uploadImgUrl: "",
        page: 1,
        loadingDH: !0,
        TabCurItem: "",
        fid: ""
    },
    onLoad: function(t) {
        t.id || wx.reLaunch({
            url: "/pages/home/home"
        }), this.data.fid = t.id, wx.setStorageSync("lock1", ""), this.computeScrollViewHeight();
    },
    onShow: function() {
        wx.getStorageSync("userInfo") ? this.setData({
            isLogin: !0,
            uploadImg: wx.getStorageSync("avatarUrlHDTmp"),
            uploadImgUrl: wx.getStorageSync("avatarUrlHD")
        }) : this.setData({
            isLogin: !1,
            uploadImg: wx.getStorageSync("avatarUrlHDTmp") ? wx.getStorageSync("avatarUrlHDTmp") : "https://file.tlzcf.vip/moren.png",
            uploadImgUrl: wx.getStorageSync("avatarUrlHD") ? wx.getStorageSync("avatarUrlHD") : "https://file.tlzcf.vip/moren.png"
        }), this.getImgData();
    },
    upimg: function() {
        wx.navigateTo({
            url: "../cropple/cropple?type=1&id=" + this.data.fid
        });
    },
    chongzhi: function() {
        wx.showToast({
            title: "重置中...",
            icon: "loading",
            duration: 3e3,
            mask: !0
        }), wx.setStorageSync("lock1", ""), this.setData({
            SucaiImgTmp: "../../images/tm.jpg",
            SucaiImg: "",
            selSucaiIndex: -1
        }, function() {
            wx.hideToast({
                success: function(t) {}
            });
        });
    },
    tabSelect: function(t) {
        var a = t.currentTarget.dataset.id, e = t.currentTarget.dataset.item;
        this.data.TabCurItem = e, this.setData({
            imgData: [],
            page: 1,
            TabCur: a,
            scrollLeft: 80 * (a - 1)
        }), wx.setStorageSync("type", a), this.getImgData();
    },
    scrollToLower: function(t) {
        wx.showToast({
            title: "加载中...",
            icon: "loading",
            mask: !0,
            duration: 3e3
        }), this.setData({
            page: this.data.page + 1
        }), wx.hideToast();
    },
    getImgData: function(a) {
        var e = this;
        var urlContent = app.globalData.url + "toolapi/tPendantDetail/list"
        request.requestPostApi(urlContent,{
          fid: e.data.fid
        },this,function(res){
            if(res.status==200){
              e.setData({
                loadingDH: !1,
                imgData: e.data.imgData.concat(res.data)
            }) 
            }
        })
      
    },
    getIndexData_del: function() {
        var a = this;
        t.default.postRequest("/api/Index/frame", {}, "加载中...", "loading", function(t) {
            a.setData({
                loadingDH: !1
            }), 200 == t.data.status ? a.setData({
                TabCurItem: t.data.data.frame[0].id,
                typeArr: t.data.data.frame,
                imgData: t.data.data.frameImg
            }) : wx.showToast({
                title: "请求失败",
                icon: "none",
                mask: !0,
                duration: 1e3
            });
        }, function(t) {
            wx.showToast({
                title: "请求失败",
                icon: "none",
                duration: 3e3
            });
        });
    },
    chooseImg: function(a) {
        var e = this, o = a.currentTarget.dataset.id, n = a.currentTarget.dataset.url;
        e.setData({
            selSucaiIndex: a.currentTarget.dataset.index,
            SucaiImg: n
        }), e.data.isSucaiStatus = !1, "lock" == a.currentTarget.dataset.item ? wx.setStorageSync("lock1", "lock") : wx.setStorageSync("lock1", ""), 
        wx.getImageInfo({
            src: n,
            success: function(a) {
                e.data.SucaiImgTmp = a.path; 
                e.data.isSucaiStatus = !0;
            },
            fail: function(t) {}
        });
    },
    computeScrollViewHeight: function() {
        var t = this;
        wx.createSelectorQuery().in(this).select(".btns").boundingClientRect(function(e) {
            var o = e.height, n = wx.getSystemInfoSync().windowHeight - o - 0;
            t.setData({
                scrollHeight: n - app.globalData.CustomBar - 15 - 30
            });
        }).exec();
    },
    isIphoneX: function() {
        var t = wx.getSystemInfoSync();
        return !!/iPhone X/i.test(t.model);
    },
    drawAvatar: function() {
        var t = wx.createCanvasContext("myAvatar", this);
        t.clearRect(0, 0, 320, 320), t.draw(!1), t.lineJoin = "round", t.lineWidth = 20, 
        t.clearRect(0, 0, 320, 320), t.drawImage(this.data.uploadImg, 0, 0, 320, 320), t.draw(!0), 
        t.save(), t.drawImage(this.data.SucaiImgTmp, 0, 0, 320, 320), t.draw(!0), t.save();
    },
    onUnload: function() {},
    suo: function(t) {
        wx.showToast({
            title: "我是高级挂件哦~",
            icon: "none",
            duration: 800
        });
    },
    handleSaveImg: function() {
        if (wx.vibrateShort(), wx.showToast({
            title: "生成中...",
            icon: "loading",
            mask: !0,
            duration: 3e4
        }), this.data.isSucaiStatus) {
            var t = wx.createCanvasContext("myAvatar", this);
            t.clearRect(0, 0, 320, 320), t.draw(!1), t.lineJoin = "round", t.lineWidth = 20, 
            t.clearRect(0, 0, 320, 320), t.drawImage(this.data.uploadImg, 0, 0, 320, 320), t.draw(!0), 
            t.save(), t.drawImage(this.data.SucaiImgTmp, 0, 0, 320, 320), t.draw(!0, setTimeout(function() {
                t.save(), wx.canvasToTempFilePath({
                    canvasId: "myAvatar",
                    success: function(t) {
                        wx.setStorageSync("img_kuang", t.tempFilePath), setTimeout(function() {
                            wx.hideToast(), wx.navigateTo({
                                url: "../save/save?type=1"
                            });
                        }, 200);
                    }
                });
            }, 300));
        } else wx.showToast({
            title: "请稍候...",
            icon: "loading",
            mask: !0,
            duration: 1e3
        });
    },
    handleAvatar: function(t) {
        var a = this;
        this.setData({
            chooseImg: t.target.dataset.avatar
        }, function() {
            a.drawAvatar();
        });
    },
    handleLogin: function(t) {
        var a = this;
        wx.getUserProfile({
            desc: "用于完善会员资料",
            success: function(t) {
                wx.setStorageSync("userInfo", t.userInfo);
                var e = a.headimgHD(t.userInfo.avatarUrl);
                wx.setStorageSync("avatarUrlHD", e), wx.getImageInfo({
                    src: e,
                    success: function(t) {
                        wx.showToast({
                            title: "授权成功",
                            icon: "none",
                            duration: 800
                        }), wx.setStorageSync("avatarUrlHDTmp", t.path), a.setData({
                            uploadImg: t.path,
                            uploadImgUrl: e,
                            isLogin: !0,
                            modalName: !1
                        });
                    }
                });
            }
        });
    },
    headimgHD: function(t) {
        return (t = t.split("/"))[t.length - 1] && 0 != t[t.length - 1] && (t[t.length - 1] = 0), 
        t.join("/");
    },
    onShareAppMessage: function() {
        return {
            title: "➜使用打开视界，点击开始制作",
            imageUrl: "https://file.tlzcf.vip/moren.png",
            path: "/pages/index/index",
            success: function(t) {
            }
        };
    },
    onShareTimeline: function() {
        return {
            title: "➜使用打开视界，点击开始制作",
            imageUrl: "https://file.tlzcf.vip/moren.png",
            path: "/pages/index/index",
            success: function(t) {
            }
        };
    },
    cloudCheck: function(t, a) {
        wx.compressImage({
            src: t,
            quality: 10,
            success: function(t) {
                wx.getFileSystemManager().readFile({
                    filePath: t.tempFilePath,
                    success: function(t) {
                        wx.cloud.callFunction({
                            name: "checkImg",
                            data: {
                                img: t.data
                            }
                        }).then(function(t) {
                            t.result && 87014 == t.result.errCode ? a(!1) : a(!0);
                        });
                    }
                });
            }
        });
    }
});