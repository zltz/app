// pages/pendant/pendant.js
const app = getApp();
const request = require('../../../utils/request.js');
Component({
  data: {
    loadingDH: !1,
    keywords: [],
    list: [],
    kindex: 0
  },
  lifetimes: {
    created: function(){
     this.getIndex();
    },
    onPullDownRefresh: function () {
      this.getIndex();
    }
  },
  methods: {

    getIndex: function() {
      var thas = this
      var urlContent = app.globalData.url + "toolapi/tPendant/list"
      request.requestPostApi(urlContent, {}, this, function(res) {
          var a = ["状态", "可爱", "节日", "少女心", "角标", "游戏", "小红旗", "潮流","酷炫","头饰","萌物"];
          a.unshift("全部", "热门", "最新", "名称");
          if(res.status==200){
            thas.setData({
              keywords: a,
              list: res.data
          });
          }
      }, function(res) {
          wx.showToast({
              title: "请求失败",
              icon: "none",
              duration: 3e3
          });
      });
    },
    
    selKey: function(e) {
      var a = this;
      var n = e.currentTarget.dataset.index;
      a.setData({
          kindex: n
      });
      var urlContent = app.globalData.url + "toolapi/tPendant/frameKeywords"
      request.requestPostApi(urlContent, {keywords: a.data.keywords[n]}, this, function(res) {
        if(res.status==200){
          a.setData({
            list: res.data
        })
        }
      }, function(res) {
          wx.showToast({
              title: "请求失败",
              icon: "none",
              duration: 3e3
          });
      });
    },
    detail: function(t) {
      var e = t.currentTarget.dataset.id;
      var a = t.currentTarget.dataset.item;
      wx.navigateTo({
          url:"../pendant/detail/detail?id=" + e
      });
    }
},
pageLifetimes: {
  onShow: function() {
  },
  },
  onShareAppMessage: function() {
    return {
        title: "➜使用打开视界，点击开始制作",
        imageUrl: "https://file.tlzcf.vip/default.jpg",
        path: "/pages/index/index",
        success: function(t) {
            console.log(t);
        }
    };
  }
})
