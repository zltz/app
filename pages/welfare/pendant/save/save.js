// pages/txgj/save/save.js
!function() {
  getApp();
  var t = null;
  Page({
      data: {
          imgpath: "",
          mengShow: !1,
          imgDraw: [],
          poster_path: "",
          poster_show: !1,
          qr_url: "https://file.tlzcf.vip/moren.png",
          img_url: "https://file.tlzcf.vip/moren.png",
          head_url: "https://file.tlzcf.vip/moren.png",
          okimg: "",
          CustomBar: "",
          currentTab: 0,
          modalName: !1,
          type: ""
      },
      onLoad: function(e) {
          var o = this;
          if (e) {
              if (1 == e.type) {
                  this.data.type = e.type;
                  var n = wx.getStorageSync("img_kuang");
              }
              if (2 == e.type && (this.data.type = e.type, n = wx.getStorageSync("img_zhizuo")), 
              n) {
                  this.setData({
                      okimg: n
                  });
                  var a = "";
                  1 == this.data.type && (a = wx.getStorageSync("lock1")), 2 == this.data.type && (a = wx.getStorageSync("lock2")), 
                  console.log(a), wx.createRewardedVideoAd && "lock" == a && (console.log("激励视频"), 
                  (t = wx.createRewardedVideoAd({
                      adUnitId: ""
                  })).onError(function(t) {}), t.onClose(function(e) {
                      e && e.isEnded ? (console.log("播放完成"), wx.showToast({
                          title: "恭喜！解锁成功",
                          icon: "success",
                          duration: 3e3
                      }), wx.saveImageToPhotosAlbum({
                          filePath: o.data.okimg,
                          success: function(t) {
                              wx.hideToast({
                                  success: function(t) {
                                      wx.showModal({
                                          title: "已保存到相册",
                                          content: "制作成功！赶紧换上新头像吧！",
                                          showCancel: !1,
                                          confirmText: "确定"
                                      });
                                  }
                              });
                          },
                          fail: function(t) {
                              console.log(t), "saveImageToPhotosAlbum:fail auth deny" !== t.errMsg && "saveImageToPhotosAlbum:fail:auth denied" !== t.errMsg || (console.log("当初用户拒绝，再次发起授权"), 
                              wx.showModal({
                                  title: "提示",
                                  content: "需要您授权保存相册",
                                  showCancel: !1,
                                  success: function(t) {
                                      wx.openSetting({
                                          success: function(t) {
                                              console.log("settingdata", t), t.authSetting["scope.writePhotosAlbum"] ? wx.showModal({
                                                  title: "提示",
                                                  content: "获取权限成功，再次点击保存按钮即可保存",
                                                  showCancel: !1
                                              }) : wx.showModal({
                                                  title: "提示",
                                                  content: "获取权限失败，将无法保存到相册哦~",
                                                  showCancel: !1
                                              });
                                          },
                                          fail: function(t) {
                                              console.log("failData", t);
                                          },
                                          complete: function(t) {
                                              console.log("finishData", t);
                                          }
                                      });
                                  }
                              }));
                          }
                      })) : (console.log("中途退出"), wx.showModal({
                          title: "温馨提示",
                          content: "亲~，需要将视频观看完毕，才可以保存哦^_^，是否重新播放？",
                          success: function(e) {
                              e.confirm && (console.log("确定"), t && t.show().catch(function(e) {
                                  t.load().then(function() {
                                      return t.show();
                                  });
                              }));
                          }
                      }));
                  }));
              } else wx.reLaunch({
                  url: "/pages/index/index"
              });
          } else wx.reLaunch({
              url: "/pages/index/index"
          });
      },
      onReady: function() {},
      savePic: function() {
          wx.vibrateShort();
          var e = "";
          1 == this.data.type && (e = wx.getStorageSync("lock1")), 2 == this.data.type && (e = wx.getStorageSync("lock2")), 
          "lock" ==  (wx.showToast({
              title: "保存中",
              icon: "loading",
              duration: 3e3
          }), wx.saveImageToPhotosAlbum({
              filePath: this.data.okimg,
              success: function(t) {
                  wx.hideToast({
                      success: function(t) {
                          wx.showModal({
                              title: "已保存到相册",
                              content: "制作成功！赶紧换上新头像吧！",
                              showCancel: !1,
                              confirmText: "好的"
                          });
                      }
                  });
              },
              fail: function(t) {
                  console.log(t), "saveImageToPhotosAlbum:fail auth deny" !== t.errMsg && "saveImageToPhotosAlbum:fail:auth denied" !== t.errMsg || (console.log("当初用户拒绝，再次发起授权"), 
                  wx.showModal({
                      title: "提示",
                      content: "需要您授权保存相册",
                      showCancel: !1,
                      success: function(t) {
                          wx.openSetting({
                              success: function(t) {
                                  console.log("settingdata", t), t.authSetting["scope.writePhotosAlbum"] ? wx.showModal({
                                      title: "提示",
                                      content: "获取权限成功，再次点击保存按钮即可保存",
                                      showCancel: !1
                                  }) : wx.showModal({
                                      title: "提示",
                                      content: "获取权限失败，将无法保存到相册哦~",
                                      showCancel: !1
                                  });
                              },
                              fail: function(t) {
                                  console.log("failData", t);
                              },
                              complete: function(t) {
                                  console.log("finishData", t);
                              }
                          });
                      }
                  }));
              }
          }));
      },
      onShareAppMessage: function() {
          return wx.vibrateShort(), {
              title: "➜使用打开视界，点击开始制作",
              imageUrl: "https://file.tlzcf.vip/default.jpg",
              path: "/pages/index/index"
          };
      },
      onShareTimeline: function() {
          return {
              title: "➜使用打开视界，点击开始制作",
              query: "pages/index/index",
              imageUrl: "https://file.tlzcf.vip/default.jpg"
          };
      },
      headimgHD: function(t) {
          return (t = t.split("/"))[t.length - 1] && 0 != t[t.length - 1] && (t[t.length - 1] = 0), 
          t.join("/");
      },
      hideModal: function(t) {
          this.setData({
              modalName: !1
          });
      },
      beifen: function() {
          var t = wx.getStorageSync("userInfo"), e = t ? this.headimgHD(t.avatarUrl) : "https://yuan.dahongxx.cn/file/default.jpeg";
          wx.downloadFile({
              url: e,
              success: function(t) {
                  var e = t.tempFilePath;
                  wx.saveImageToPhotosAlbum({
                      filePath: e,
                      success: function(t) {
                          wx.showToast({
                              title: "保存成功",
                              icon: "success",
                              duration: 2e3
                          });
                      },
                      fail: function(t) {
                          t.errMsg && wx.showModal({
                              title: "提示",
                              content: "您好！请先授权，再保存此图像",
                              showCancel: !1,
                              success: function(t) {
                                  t.confirm && wx.openSetting({
                                      success: function(t) {
                                          console.log(t), t.authSetting["scope.writePhotosAlbum"] ? wx.saveImageToPhotosAlbum({
                                              filePath: e,
                                              success: function(t) {
                                                  wx.showToast({
                                                      title: "保存成功",
                                                      icon: "success",
                                                      duration: 2e3
                                                  });
                                              }
                                          }) : wx.showModal({
                                              title: "温馨提示",
                                              content: "授权失败，请稍后重新获取",
                                              showCancel: !1
                                          });
                                      }
                                  });
                              }
                          });
                      }
                  });
              }
          });
      },
      copy: function() {
          wx.setClipboardData({
              data: "weijianabcdefg",
              success: function(t) {
                  wx.showToast({
                      title: "已复制微信号"
                  });
              }
          });
      },
      _getPoster: function() {
          this.data.qr_url ? (this.setData({
              mengShow: !0
          }), wx.showLoading({
              title: "生成中...",
              icon: "loading"
          }), this.setData({
              imgDraw: {
                  width: "1080rpx",
                  height: "1832rpx",
                  background: "#fff",
                  views: [ {
                      type: "image",
                      url: this.data.qr_url,
                      css: {
                          top: "1536rpx",
                          left: "63rpx",
                          width: "226rpx",
                          height: "226rpx"
                      }
                  }, {
                      type: "image",
                      url: this.data.img_url,
                      css: {
                          top: "310rpx",
                          left: "58rpx",
                          right: "58rpx",
                          width: "966rpx",
                          height: "923rpx",
                          borderRadius: "23rpx"
                      }
                  }, {
                      type: "image",
                      url: this.data.head_url,
                      css: {
                          top: "128rpx",
                          left: "58rpx",
                          width: "126rpx",
                          height: "126rpx",
                          borderWidth: "6rpx",
                          borderColor: "#FFF",
                          borderRadius: "126rpx"
                      }
                  }, {
                      type: "text",
                      text: "各种头像制作神器",
                      css: {
                          top: "139rpx",
                          fontSize: "33rpx",
                          left: "222rpx",
                          align: "left",
                          color: "#1e262b",
                          fontWeight: "bold"
                      }
                  }, {
                      type: "text",
                      text: "在线制作各种挂件头像，快来一起制作",
                      css: {
                          top: "199rpx",
                          left: "222rpx",
                          fontSize: "33rpx",
                          color: "#a1a4a4"
                      }
                  }, {
                      type: "text",
                      text: "『头像制作神器』",
                      css: {
                          top: "1622rpx",
                          left: "347rpx",
                          fontSize: "34rpx",
                          color: "#1e262b",
                          fontWeight: "bold"
                      }
                  }, {
                      type: "text",
                      text: "保存图片，扫码制作~",
                      css: {
                          top: "1685rpx",
                          left: "362rpx",
                          fontSize: "33rpx",
                          color: "#a1a4a4"
                      }
                  }, {
                      type: "text",
                      text: "头像制作神器",
                      css: {
                          width: "880rpx",
                          top: "1280rpx",
                          left: "104rpx",
                          lineHeight: "60rpx",
                          maxLines: 4,
                          align: "left",
                          fontSize: "45rpx",
                          color: "#0081ff"
                      }
                  }, {
                      type: "text",
                      text: "在线免费制作各种挂件头像，节日游戏动漫情侣头像，支持1000+种挂件，快来试试吧~",
                      css: {
                          width: "880rpx",
                          top: "1360rpx",
                          left: "104rpx",
                          lineHeight: "60rpx",
                          maxLines: 4,
                          align: "left",
                          fontSize: "48rpx",
                          color: "#262f3e"
                      }
                  } ]
              }
          })) : wx.showToast({
              title: "请稍候...",
              icon: "loading",
              mask: !0,
              duration: 3e3
          });
      },
      _onImgOK: function(t) {
          wx.hideLoading({});
          var e = t.detail.path;
          this.data.poster_path = e, this.setData({
              poster_path: e,
              poster_show: !0,
              mengShow: !1
          }), wx.showToast({
              title: "点击图片，长按分享",
              icon: "none",
              duration: 3e3
          });
      },
      _closeModal: function(t) {
          this.setData({
              poster_show: !1
          });
      },
      _saveAlbum: function(t) {
          this.data.poster_path && wx.saveImageToPhotosAlbum({
              filePath: this.data.poster_path,
              success: function(t) {
                  wx.showToast({
                      title: "保存成功!",
                      icon: "success",
                      duration: 1e3
                  });
              },
              fail: function(t) {
                  wx.showModal({
                      title: "提示",
                      content: "请先授权，再保存海报图片",
                      showCancel: !1,
                      success: function(t) {
                          t.confirm && wx.openSetting({
                              success: function(t) {
                                  t.authSetting["scope.writePhotosAlbum"] ? wx.saveImageToPhotosAlbum({
                                      filePath: e.data.poster_path,
                                      success: function(t) {
                                          wx.showToast({
                                              title: "保存成功!",
                                              icon: "success",
                                              duration: 1e3
                                          });
                                      }
                                  }) : wx.showModal({
                                      title: "温馨提示",
                                      content: "授权失败，请稍后重新获取!",
                                      showCancel: !1
                                  });
                              }
                          });
                      }
                  });
              }
          });
      },
      _previewPoster: function() {
          wx.previewImage({
              urls: [ this.data.poster_path ],
              current: this.data.poster_path
          });
      }
  });
}();