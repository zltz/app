// pages/welfare/yizhan/yizhan.js
const app = getApp();
const request = require('../../../utils/request.js');
Component({
  data: {
    lists: [],
    thissss:true,
    page:1
  },
  methods:{
    maskSwitch:function(e){
         wx.showToast({
          title: e.currentTarget.dataset['index'],
          content:e.currentTarget.dataset['index']
        })
    },
    //监听是否滑到底部
    nextPage: function () {
      let page = this.data.page;
      let startindex = page * 10;
      let bujin=10;
      console.log("第" + page + "页滑到底部了,请求第" + (page + 1) + "页");
      page += 1;
      var thas = this
      var params={
        page:startindex,
        limit:bujin
      }
      var urlContent = app.globalData.url + "toolapi/tOneStop/list"
      request.requestPostApi(urlContent,params,this,function(res){
        if(res.status==200){
          thas.setData({ lists: thas.data.lists.concat(res.data), page: page })
        }
      })
    }
  },
  lifetimes: {
    created: function(){
      var thas = this
      var params={
        page:0,
        limit:10
      }
      var urlContent = app.globalData.url + "toolapi/tOneStop/list"
      request.requestPostApi(urlContent,params,this,function(res){
        if(res.status==200){
          thas.setData({
            lists: res.data
        })
        }
      })
    }
  },
})