const app = getApp();
const request = require('../../utils/request.js');
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    lists: [],
    page:1
  },
  lifetimes: {
    created: function(){
      var thas = this
      var params={
        page:0,
        limit:15
      }
      var urlContent = app.globalData.url + "reptile/tJournalism/list"
      request.requestPostApi(urlContent,params,this,function(res){
        if(res.status==200){
          thas.setData({
            lists: res.data
        })
        var url = app.globalData.url + "api/wx/login"
        request.requestPostApi(url,{},this,function(e){
          if(res.status==200){
            console.log("今日最新获取信息！！")
          }
        })
        }
      })
    }
  },
  methods: {
    gotoDetail: function(e){
      let query = e.currentTarget.dataset['index'];
      wx.setStorageSync("id",query)
    },
    //监听是否滑到底部
    nextPage: function () {
      let page = this.data.page;
      let startindex = page * 15;
      let bujin=15;
      console.log("第" + page + "页滑到底部了,请求第" + (page + 1) + "页");
      page += 1;
      var thas = this
      var params={
        page:startindex,
        limit:bujin
      }
      var urlContent = app.globalData.url + "reptile/tJournalism/list"
      request.requestPostApi(urlContent,params,this,function(res){
        if(res.status==200){
          thas.setData({ lists: thas.data.lists.concat(res.data), page: page })
        }
      })
    }
  }
});