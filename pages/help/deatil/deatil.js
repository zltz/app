// pages/help/deatil.js
const app = getApp();
const request = require('../../../utils/request.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tJournalismDetail: {}
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.showShareMenu({
      withShareTicket: true
    })
    var thas = this
    var id=wx.getStorageSync("id")
    var params={
     id:id
    }
    var urlContent = app.globalData.url + "reptile/tJournalismDetail/queryDetailById"
    request.requestPostApi(urlContent,params,this,function(res){
      if(res.status==200){
        let newContent=  res.data.content.replace(/<img[^>]*>/gi,function(match,capture){
          match = match.replace(/style="[^"]+"/gi, '').replace(/style='[^']+'/gi, '');
          match = match.replace(/width="[^"]+"/gi, '').replace(/width='[^']+'/gi, '');
          match = match.replace(/height="[^"]+"/gi, '').replace(/height='[^']+'/gi, '');
          return match;
      });
      newContent = newContent.replace(/style="[^"]+"/gi,function(match,capture){
          match = match.replace(/width:[^;]+;/gi, 'max-width:100%;').replace(/width:[^;]+;/gi, 'max-width:100%;');
          return match;
      });
      newContent = newContent.replace(/<br[^>]*\/>/gi, '');
      newContent = newContent.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:10px 0;"');
      res.data.content=newContent
        thas.setData({
          tJournalismDetail: res.data
      })
      }
    })
  }
})