const request = require('../../utils/request.js');
const app = getApp();
Page({
  data: {
    PageCur: 'help'
  },
  NavChange(e) {
    this.setData({
      PageCur: e.currentTarget.dataset.cur
    })
  },
  onShareAppMessage() {
    return {
      title: '打开视界',
      path: '/pages/index/index'
    }
  },
})