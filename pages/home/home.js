const request = require('../../utils/request.js');
const app = getApp();
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    starCount: 0,
    forksCount: 0,
    visitTotal: 0,
    success:false,
    error:false,
    text:"                                                            ",
    marqueePace: 1,//滚动速度
    marqueeDistance: 0,//初始滚动距离
    marquee_margin: 30,
    size:14,
    interval: 20 // 时间间隔
  },
  attached() {
    let that = this;
    numDH();
    function numDH() {
    var length = that.data.text.length * that.data.size;//文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth;// 屏幕宽度
      var urlContent = app.globalData.url + "api/wx/login"
      request.requestPostApi(urlContent, {}, this, function (res) {
        if (res.status == 200) {
          that.setData({
            starCount: that.coutNum(res.data.signInSum),
            visitTotal: that.coutNum(res.data.videoNumber),
            text:res.data.count,
            length: length,
            windowWidth: windowWidth,
            userInfo: app.globalData.userInfo
          })
        } else {
          console.error(res)
        }
        that.scrolltxt()// 第一个字消失后立即从右边出现
      }, function (res) {
        console.error(res)
      });
    }
  },
  methods: {
    scrolltxt: function () {
      var that = this;
      var length = that.data.length;//滚动文字的宽度
      var windowWidth = that.data.windowWidth;//屏幕宽度
      if (length > windowWidth){
        var interval = setInterval(function () {
          var maxscrollwidth = length + that.data.marquee_margin;//滚动的最大宽度，文字宽度+间距，如果需要一行文字滚完后再显示第二行可以修改marquee_margin值等于windowWidth即可
          var crentleft = that.data.marqueeDistance;
          if (crentleft < maxscrollwidth) {//判断是否滚动到最大宽度
            that.setData({
              marqueeDistance: crentleft + that.data.marqueePace
            })
          }
          else {
            //console.log("替换");
            that.setData({
              marqueeDistance: 0 // 直接重新滚动
            });
            clearInterval(interval);
            that.scrolltxt();
          }
        }, that.data.interval);
      }
      else{
        that.setData({ marquee_margin:"1000"});//只显示一条不滚动右边间距加大，防止重复显示
      } 
    },  
    coutNum(e) {
      if (e > 1000 && e < 10000) {
        e = (e / 1000).toFixed(2) + 'k'
      }
      if (e > 10000) {
        e = (e / 10000).toFixed(2) + 'W'
      }
      return e
    },
    CopyLink(e) {
      wx.setClipboardData({
        data: e.currentTarget.dataset.link,
        success: res => {
          wx.showToast({
            title: '已复制',
            duration: 1000,
          })
        }
      })
    },
    showModal(e) {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    },
    hideModal(e) {
      this.setData({
        modalName: null
      })
    },
    showQrcode() {
      wx.previewImage({
        urls: ['http://r9y4fq47a.hb-bkt.clouddn.com/175056-14986434564d62.jpg'],
        current: 'http://r9y4fq47a.hb-bkt.clouddn.com/175056-14986434564d62.jpg' // 当前显示图片的http链接      
      })
    },
    sign() {
      let that = this;
      var urlContent = app.globalData.url + "api/wx/signIn"
      request.requestPostApi(urlContent, {}, this, function (res) {
        if (res.status == 200) {
          that.setData({
            starCount: that.coutNum(res.data.signInSum),
            visitTotal: that.coutNum(res.data.videoNumber),
            success:true
          })
        } else {
          that.setData({
            error:true
          })
        }
      }, function (res) {
        console.error(res)
      });
    },
    qinggan:function(){
      wx.navigateTo({
        url: '../home/qinggan/qinggan',
      })
    },
    hideModal(){
      this.setData({
        error:false,
        success:false
      })
    },
    login() {
      var that = this;
      wx.getUserProfile({
        desc: '用于展示用户信息',
        success(res) {
          var user = res.userInfo
          app.globalData.userInfo = user
          that.setData({
            userInfo: user
          }),
          that.updateUser(user.nickName)
        }
      })
    },
    loginOut() {
      app.globalData.userInfo = null
      this.setData({
        userInfo: null
      })
    },
    updateUser(e){
      var urlContent = app.globalData.url + "api/wx/update"
      var params = {
        name: e
      };
      request.requestPostApi(urlContent, params, this, function (res) {
        if (res.status == 200) {
        }
      })
    }
  }
})