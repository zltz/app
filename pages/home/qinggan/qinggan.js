const app = getApp();
const request = require('../../../utils/request.js');
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    lists: []
  },
  lifetimes: {
    created: function(){
      var thas = this
      var params={
        page:0,
        limit:120
      }
      var urlContent = app.globalData.url + "reptile/tJournalism/list"
      request.requestPostApi(urlContent,params,this,function(res){
        if(res.status==200){
          thas.setData({
            lists: res.data
        })
        }
      })
    }
  },
  methods: {
    gotoDetail: function(e){
      let query = e.currentTarget.dataset['index'];
      wx.setStorageSync("id",query)
    }
  }
});